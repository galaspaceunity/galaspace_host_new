<?php

session_start();

require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/../app/settings.php';

$app = new \Slim\App($config);

require __DIR__ . '/../app/dependencies.php';

require __DIR__ . '/../app/routes.php';
