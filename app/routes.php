<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;

$app->get('/', 'HomeController:index')->setName('home');
$app->get('/community-feed', 'HomeController:communityFeed')->setName('community-feed');
$app->get('/request-space', 'HomeController:requestSpace')->setName('request-space');
$app->get('/about-us', 'HomeController:aboutUs')->setName('about-us');
$app->get('/press', 'HomeController:press')->setName('press');
$app->get('/careers', 'HomeController:careers')->setName('Careers');
$app->get('/faq', 'HomeController:faq')->setName('FAQ');
$app->get('/get-started-space-owner', 'HomeController:getStartedSpaceOwner')->setName('Get Started Space Owner');
$app->get('/get-started-organizer', 'HomeController:getStartedOrganizer')->setName('Get Started Event Organizer');
$app->get('/get-started-service-provider', 'HomeController:getStartedServiceProvider')->setName('Get Started Event Service Provider');
$app->get('/contact', 'HomeController:getContact')->setName('Contact Us');
$app->post('/contact', 'HomeController:postContact');
$app->get('/terms', 'HomeController:tos')->setName('Terms Of Service');
$app->get('/privacy-policy', 'HomeController:privacyPolicy')->setName('Privacy Policy');
$app->get('/cancellation-policy', 'HomeController:cancellationPolicy')->setName('Cancellation Policy');
$app->get('/event-spaces', 'HomeController:eventSpaces')->setName('Event Spaces');

//space overview
$app->get('/space-overview/{space_id}', 'DashboardController:getSpaceOverview')->setName('space-overview');
$app->post('/space-overview/{space_id}', 'DashboardController:postSpaceOverview');

//space-booking
$app->post('/space-booking', 'BookingController:SpaceBooking');

//service booking
$app->get('/service-booking/{spbook_id}', 'BookingController:getServiceBooking');
$app->post('/service-booking', 'BookingController:postServiceBooking');

//plan an event
$app->get('/plan-an-event', 'HomeController:getPlanEvent')->setName('Plan an Event');
$app->post('/plan-an-event', 'HomeController:postPlanEvent');

$app->get('/getSpaceBookingAvailability/{spaceid}/{dt}','BookingController:getEventAvailability');

$app->group('', function() {
	//signup routes
	$this->get('/signup', 'UserRegistrationController:getSignUp')->setName('user_registration.signup');
	$this->post('/signup', 'UserRegistrationController:postSignUp');

    //company description
    $this->get('/company-description/{user_name}', 'UserRegistrationController:getCompanyDescription')->setName('company-description');
    $this->post('/company-description/{user_name}', 'UserRegistrationController:postCompanyDescription');

    //company location
    $this->get('/company-location/{user_name}', 'UserRegistrationController:getCompanyLocation')->setName('company-location');
    $this->post('/company-location/{user_name}', 'UserRegistrationController:postCompanyLocation');

	//signin routes
//	$this->get('/signin', 'AuthController:getSignIn')->setName('auth.signin');
	$this->post('/signin', 'AuthController:postSignIn');

	//reset password routes
	$this->get('/reset-password', 'AuthController:getResetPassword')->setName('auth.reset-password');
	$this->post('/reset-password', 'AuthController:postResetPassword');

	$this->get('/confirm-reset', 'AuthController:getConfirmReset')->setName('confirm-reset');

	$this->get('/verify-reset/{user_token}', 'AuthController:getVerifyReset')->setName('verify-reset');
	$this->post('/verify-reset/{user_token}', 'AuthController:postVerifyReset');

	//confirmation token
	$this->get('/confirm-token', 'AuthController:getConfirmToken')->setName('confirm-token');
	$this->get('/verify-token/{user_token}', 'AuthController:getVerifyToken')->setName('verify-token');

})->add(new GuestMiddleware($container));

$app->group('', function() {

	//signout routes
	$this->get('/signout', 'AuthController:getSignOut')->setName('auth.signout');

	//space type routes
	$this->get('/space-type/{user_id}', 'SpaceRegistrationController:getSpaceType')->setName('space-type');
	$this->post('/space-type/{user_id}', 'SpaceRegistrationController:postSpaceType');

	//space details routes
	$this->get('/space-details/{space_id}', 'SpaceRegistrationController:getSpaceDetails')->setName('space-details');
	$this->post('/space-details/{space_id}', 'SpaceRegistrationController:postSpaceDetails');

	//space address routes
	$this->get('/space-address/{space_id}', 'SpaceRegistrationController:getSpaceAddress')->setName('space-address');
	$this->post('/space-address/{space_id}', 'SpaceRegistrationController:postSpaceAddress');

	//space amenities
	$this->get('/space-amenities/{space_id}', 'SpaceRegistrationController:getSpaceAmenities')->setName('space-amenities');
	$this->post('/space-amenities/{space_id}', 'SpaceRegistrationController:postSpaceAmenities');

    //event description
    $this->get('/space-description/{space_id}', 'SpaceRegistrationController:getSpaceDescription')->setName('space-description');
    $this->post('/space-description/{space_id}', 'SpaceRegistrationController:postSpaceDescription');

    //event rules
    $this->get('/space-rules/{space_id}', 'SpaceRegistrationController:getSpaceRules')->setName('space-rules');
    $this->post('/space-rules/{space_id}', 'SpaceRegistrationController:postSpaceRules');

    //event name
    $this->get('/space-name/{space_id}', 'SpaceRegistrationController:getSpaceName')->setName('space-name');
    $this->post('/space-name/{space_id}', 'SpaceRegistrationController:postSpaceName');

    //space availability
    $this->get('/space-availability/{space_id}', 'SpaceRegistrationController:getSpaceAvailability')->setName('space-availability');
    $this->post('/space-availability/{space_id}', 'SpaceRegistrationController:postSpaceAvailability');

    //space cancellation policy
    $this->get('/space-cpolicy/{space_id}', 'SpaceRegistrationController:getSpaceCPolicy')->setName('space-cpolicy');
    $this->post('/space-cpolicy/{space_id}', 'SpaceRegistrationController:postSpaceCPolicy');

    //space pricing
    $this->get('/space-pricing/{space_id}', 'SpaceRegistrationController:getSpacePricing')->setName('space-pricing');
    $this->post('/space-pricing/{space_id}', 'SpaceRegistrationController:postSpacePricing');

    //space image
    $this->get('/space-image/{space_id}', 'SpaceRegistrationController:getSpaceImage')->setName('space-image');
    $this->post('/space-image/{space_id}', 'SpaceRegistrationController:postSpaceImage');

    //space law
    $this->get('/space-law/{space_id}', 'SpaceRegistrationController:getSpaceLaw')->setName('space-law');
    $this->post('/space-law/{space_id}', 'SpaceRegistrationController:postSpaceLaw');

    //service type
    $this->get('/service-type/{user_id}', 'ServiceRegistrationController:getServiceType')->setName('service-type');
    $this->post('/service-type/{user_id}', 'ServiceRegistrationController:postServiceType');

    //service details
    $this->get('/service-details/{service_id}', 'ServiceRegistrationController:getServiceDetails')->setName('service-details');
    $this->post('/service-details/{service_id}', 'ServiceRegistrationController:postServiceDetails');

    //service name
    $this->get('/service-name/{service_id}', 'ServiceRegistrationController:getServiceName')->setName('service-name');
    $this->post('/service-name/{service_id}', 'ServiceRegistrationController:postServiceName');

    //service law
    $this->get('/service-law/{service_id}', 'ServiceRegistrationController:getServiceLaw')->setName('service-law');
    $this->post('/service-law/{service_id}', 'ServiceRegistrationController:postServiceLaw');

	//service cuisine type
    $this->get('/service-cuisine-type/{service_id}', 'ServiceRegistrationController:getServiceCuisineType')->setName('service-cuisine-type');
    $this->post('/service-cuisine-type/{service_id}', 'ServiceRegistrationController:postServiceCuisineType');

	//service menu
	$this->get('/service-menu/{catering_id}', 'ServiceRegistrationController:getServiceMenu')->setName('service-menu');
	$this->post('/service-menu/{catering_id}', 'ServiceRegistrationController:postServiceMenu');

    //service overview
    $this->get('/service-overview/{service_id}', 'DashboardController:getServiceOverview')->setName('service-overview');
    $this->post('/service-overview/{service_id}', 'DashboardController:postServiceOverview');

    //service menu overview
//    $this->get('/service-overview/{menu_id}', 'DashboardController:getServiceOverview')->setName('service-overview');
//    $this->post('/service-overview/{menu_id}', 'DashboardController:postServiceOverview');

	//host dashboard
	$this->get('/host-dashboard/{user_id}', 'DashboardController:getHostDashboard')->setName('host-dashboard');
	$this->post('/host-dashboard/{user_id}', 'DashboardController:postHostDashboard');

    $this->post('/chat_history', 'DashboardController:getUserMessages');
    $this->post('/send_message', 'DashboardController:postUserMessage');
    $this->post('/seen', 'DashboardController:postSeenMessages');

	$this->map(['GET', 'POST'],'/ajax-test', 'DashboardController:getAjaxTest')->setName('ajax-test');

	$this->get('/delete-svBooking/{svbid}/{spbid}', 'BookingController:deleteServiceBooking');
    $this->get('/booking-cancel', 'BookingController:cancelBooking');

    $this->get('/booking-overview/{bookid}', 'BookingController:getBookingOverview');
    $this->post('/booking-overview/{bookid}', 'BookingController:postBookingOverview');

    $this->get('/booking-summary/{bookid}', 'ReportController:bookingSummary');
    $this->get('/download-summary/{bookid}', 'ReportController:downloadPDF');

    //booking-spaces
    $this->get('/booking-space/{book_id}', 'BookingController:getBookingSpace')->setName('booking-space');
    $this->post('/booking-space/{book_id}', 'BookingController:postBookingSpace');

    //booking-service
    $this->get('/booking-service/{book_id}', 'BookingController:getBookingService')->setName('booking-service');
    $this->post('/booking-service/{book_id}', 'BookingController:postBookingService');

})->add(new AuthMiddleware($container));

if(isset($_SESSION["user_admin"]) && $_SESSION["user_admin"] == "true") {
    $app->group('/admin', function() {

        //admin-dashboard
        $this->get('/dashboard/{user_id}', 'AdministratorController:getAdminDashboard')->setName('admin-dashboard');
        $this->post('/dashboard/{user_id}', 'AdministratorController:postAdminDashboard');

        //user-details
        $this->get('/user-details/{user_id}', 'AdministratorController:getUserDetails')->setName('User Details');
        $this->post('/user-details/{user_id}', 'AdministratorController:postUserDetails');

        //space-details
        $this->get('/space-details/{user_id}', 'AdministratorController:getSpaceDetails')->setName('Space Details');
        $this->post('/space-details/{user_id}', 'AdministratorController:postSpaceDetails');

        //service-details
        $this->get('/service-details/{user_id}', 'AdministratorController:getServiceDetails')->setName('Service Details');
        $this->post('/service-details/{user_id}', 'AdministratorController:postServiceDetails');

        //book-details
        $this->get('/book-details/{user_id}', 'AdministratorController:getBookDetails')->setName('Book Details');
        $this->post('/book-details/{user_id}', 'AdministratorController:postBookDetails');

    })->add(new AuthMiddleware($container));
};
