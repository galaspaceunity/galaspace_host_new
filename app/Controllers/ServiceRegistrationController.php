<?php

namespace App\Controllers;

use App\Models\CateringMenu;
use App\Models\CateringMenuImage;
use App\Models\Service;
use App\Models\ServiceCatering;
use App\Models\ServiceDetail;
use App\Models\SpaceAvailability;
use Slim\Views\Twig as View;
use App\Controllers\Controller;
use Slim\Http\UploadedFile;
use App\Models\User;
use App\Models\UserFunction;
use App\Models\Space;
use Respect\Validation\Validator as v;

class ServiceRegistrationController extends Controller
{
    //Service Type
    public function getServiceType($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'title' => 'Service Type',
        ];
        return $this->view->render($response, '/service_registration/service-type.twig', $data);
    }

    public function postServiceType($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'service_type' => v::notEmpty(),
            'service_name' => v::notEmpty(),
            'service_location' => v::notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($request->getUri()->getBasePath().'/service-type/'.$_SESSION['user_id']);
        }
        //end of validation

        $user = User::where('user_id', $_SESSION['user_id'])->first();
        if ($user != NULL) {
            $_SESSION['service_id'] = "SV".$_SESSION['user_id'].date('mdYHis');
            UserFunction::create([
                'user_id' => $_SESSION['user_id'],
                'service_id' => $_SESSION['service_id'],
            ]);
            $service_type = ucwords($request->getParam('service_type'));
            $location = $request->getParam('service_location');
            $currency = "";
            if($location == "Singapore"){
                $currency = "SGD$";
            }elseif ($location == "Sarawak"){
                $currency = "MYR";
            }
            Service::create([
                'service_id' => $_SESSION['service_id'],
                'service_name' => ucwords($request->getParam('service_name')),
                'service_type' => $service_type,
                'service_status' => "INCOMPLETE",
                'service_location' => $location,
                'service_currency' => $currency,
                'service_by' => $_SESSION['user_id'],
            ]);

            $request->getUploadedFiles();
            //handle single input with single file upload
            $targetDir = "Users/" . $user->user_name . "/servicePic/";
            if (!file_exists($targetDir)) {
                mkdir($targetDir, 0777, true);
            }
            $targetFile = $targetDir . basename($_FILES["service_photo"]["name"]);
            $check = getimagesize($_FILES["service_photo"]["tmp_name"]);
            $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
            $uploadOK = 1;
            // Check file size
            if ($_FILES["service_photo"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOK = 0;
            }
            //file format condition
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOK = 0;
            }
            if ($check !== false && $uploadOK != 0) {
                if ($c = move_uploaded_file($_FILES["service_photo"]["tmp_name"], $targetFile)) {
                    //for database dir purpose
                    $svc = Service::where('service_id',$_SESSION["service_id"])->first();
                    $svc->service_main_image = "Users/" . $user->user_name . "/servicePic/" . basename($_FILES["service_photo"]["name"]);
                    $svc->save();
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            } else {
                echo "Your image is not uploaded.";
            }
            if($service_type == "Catering") {
                return $response->withRedirect($request->getUri()->getBasePath() . '/service-cuisine-type/' . $_SESSION['service_id']);
            }else{
                return $response->withRedirect($request->getUri()->getBasePath() . '/service-details/' . $_SESSION['service_id']);
            }
        } else {
            return $this->view->render($response, '/404.twig');
        }
    }
    //End of Service Type

    //Service Cuisine Type
    public function getServiceCuisineType($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Service Cuisine Type',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'service_id' => $_SESSION['service_id']
        ];
        return $this->view->render($response, '/service_registration/service-cuisine-type.twig', $data);
    }

    public function postServiceCuisineType($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'service_cuisine_type' => v::notEmpty(),
            'service_halal' => v::notEmpty()
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($request->getUri()->getBasePath().'/service-cuisine-type/'.$_SESSION['service_id']);
        }
        //end of validation
        $catID = "SC".$_SESSION["user_id"].date('mdYHis');
        $_SESSION['catering_id'] = $catID;
        ServiceCatering::create([
            'catering_id' => $catID,
            'service_id' => $_SESSION['service_id'],
            'catering_description' => $request->getParam('catering_description'),
            'catering_type' => $request->getParam('service_cuisine_type'),
            'catering_halal' => $request->getParam('service_halal')
        ]);

        $Cid = Service::where('service_id',$_SESSION['service_id'])->first();
        $Cid->service_catering = $catID;
        $Cid->save();

        return $response->withRedirect($request->getUri()->getBasePath().'/service-menu/'.$_SESSION['catering_id']);
    }
    //End of Service Cuisine Type

    public function getServiceMenu($request, $response)
    {
        $menu = CateringMenu::where('service_catering.catering_id',$_SESSION["catering_id"])
            ->leftJoin('service_catering','catering_menu.catering_id','=','service_catering.catering_id')
            ->leftJoin('services','services.service_id','=','service_catering.service_id')->get();

        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Service Menu',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'service_id' => $_SESSION['service_id'],
            'catering_id' => $_SESSION['catering_id'],
            'catering_menu' => $menu
        ];

        return $this->view->render($response, '/service_registration/service-menu.twig', $data);
    }

    public function postServiceMenu($request, $response)
    {
        if ($request->getParam('menu_hidden_input') === "menulist") {
            $menuID = "M".$_SESSION["catering_id"].date('mdYHis');
            $catering = CateringMenu::create([
                'menu_id' => $menuID,
                'catering_id' => $_SESSION["catering_id"],
                'menu_name' => ucwords($request->getParam('menu_name')),
                'menu_price' => $request->getParam('menu_price'),
                'menu_description' => $request->getParam('menu_description'),
            ]);
            $user = User::where('user_id', $_SESSION["user_id"])->first();
            $uploadedImage = $request->getUploadedFiles();
            $uFcount = count($uploadedImage);
            $feV = "Users/" . $user->user_name . "/menuPic/";
            if (!file_exists($feV)) {
                mkdir($feV, 0777, true);
            }
            for ($xC = 0; $xC < $uFcount; $xC++) {
                $cfp = "Users/" . $user->user_name . "/menuPic/".$menuID;
                if (!file_exists($cfp)) {
                    mkdir($cfp, 0777, true);
                }
                $targetDir = "Users/" . $user->user_name . "/menuPic/".$menuID;
                $targetFile = $targetDir . "/" . basename($_FILES["menu_photo_".($xC+1)]["name"]);
                $check = getimagesize($_FILES["menu_photo_".($xC+1)]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["menu_photo_".($xC+1)]["size"] > 100000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["menu_photo_".($xC+1)]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //for database dir purpose
                            $menu = CateringMenu::where('menu_id',$menuID)->first();
                            $ac = $xC+1;
                            if($ac == 1){
                                $menu->menu_image_1 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }elseif ($ac == 2){
                                $menu->menu_image_2 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }elseif ($ac == 3){
                                $menu->menu_image_3 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }elseif($ac == 4){
                                $menu->menu_image_4 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }
                            $menu->save();
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                } else {

                }
            }
            return $response->withRedirect($request->getUri()->getBasePath().'/service-menu/'.$_SESSION['catering_id']);

        }elseif ($request->getParam('menu_hidden_input') === "menusubmit"){
            return $response->withRedirect($request->getUri()->getBasePath().'/service-law/'.$_SESSION['service_id']);
        }

    }

    //Service Details
    public function getServiceDetails($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Service Details',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'service_id' => $_SESSION['service_id'],
        ];
        return $this->view->render($response, '/service_registration/service-details.twig', $data);
    }

    public function postServiceDetails($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'service_detail' => v::notEmpty(),
            'service_pricing' => v::notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($request->getUri()->getBasePath().'/service-details/'.$_SESSION['service_id']);
        }
        //end of validation

        $user = User::where('user_id', $_SESSION["user_id"])->first();
            $dID = "SD".$_SESSION["user_id"].date('mdYHis');
            ServiceDetail::create([
                'detail_id' => $dID,
                'service_id' => $_SESSION["service_id"],
                'detail_description' =>  ucwords($request->getParam('service_detail')),
                'detail_pricing' => $request->getParam('service_pricing'),
            ]);
        $uploadedImage = $request->getUploadedFiles();
        $tf = "Users/" . $user->user_name . "/servicePic";
        if (!file_exists($tf)) {
            mkdir($tf, 0777, true);
        }
        $uFcount = count($uploadedImage["service_photo"]);
        for ($xC = 0; $xC < $uFcount; $xC++) {
            $targetDir = "Users/" . $user->user_name . "/detailPic/".$dID;
            if (!file_exists($targetDir)) {
                mkdir($targetDir, 0777, true);
            }
            $targetFile = $targetDir . "/" . basename($_FILES["service_photo"]["name"][$xC]);
            $check = getimagesize($_FILES["service_photo"]["tmp_name"][$xC]); //some img cannot and have to further investigate
            $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
            $uploadOK = 1;
            // Check file size *the size of file that declared is on byte
            if ($_FILES["service_photo"]["size"][$xC] > 10000000) {
                echo "Sorry, your picture is too large.";
                $uploadOK = 0;
            }
            //file format condition
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOK = 0;
            }
            if ($check != false) {
                if ($uploadOK != 0) {
                    $upDir = "Users/" . $user->user_name . "/detailPic/".$dID."/". basename($_FILES["service_photo"]["name"][$xC]);
                    $a = chmod($upDir, 0777);
                    $b = move_uploaded_file($_FILES["service_photo"]["tmp_name"][$xC], $upDir);
                    if ($b == true) {
                        //for database dir purpose
                        $di = ServiceDetail::where('detail_id',$dID)->first();
                        $ac = $xC + 1;
                        $acd = "detail_image_" . $ac;
                        $di->$acd = $upDir;
                        $di->save();
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                } else {
                    echo "Your image is not uploaded.";
                }
            } else {

            }
        }
        $svc = Service::where('service_id',$_SESSION["service_id"])->first();
        $svc->service_detail = $dID;
        $svc->save();
        return $response->withRedirect($request->getUri()->getBasePath().'/service-law/'.$_SESSION['service_id']);
    }
    //End of Service Details

    //Service Name
//    public function getServiceName($request, $response)
//    {
//        $data = [
//            'title' => 'Service Details',
//            'user_id' => $_SESSION['user_id'],
//            'service_id' => $_SESSION['service_id'],
//        ];
//        return $this->view->render($response, '/service_registration/service-name.twig', $data);
//    }
//
//    public function postServiceName($request, $response)
//    {
//        //validation
//        $validation = $this->validator->validate($request, [
//            'service_name' => v::notEmpty(),
//        ]);
//
//        if ($validation->failed()) {
//            return $response->withRedirect($request->getUri()->getBasePath().'/service-name/'.$_SESSION['service_id']);
//        }
//        //end of validation
//
//        $details = Service::where('service_id',$_SESSION['service_id'])->first();
//        $details->service_name = ucwords($request->getParam('service_name'));
//        $details->save();
//        return $response->withRedirect($request->getUri()->getBasePath().'/service-law/'.$_SESSION['service_id']);
//    }
    //End of Service Name

    //Service Law
    public function getServiceLaw($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Service Submit Review',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'service_id' => $_SESSION['service_id'],
        ];
        return $this->view->render($response, '/service_registration/service-law.twig', $data);
    }

    public function postServiceLaw($request, $response)
    {
        $details = Service::where('service_id',$_SESSION['service_id'])->first();
        $details->service_status = "PENDING";
        $details->save();
        return $response->withRedirect($request->getUri()->getBasePath().'/host-dashboard/'.$_SESSION['user_id']);
    }
    //End of Service Law

}
