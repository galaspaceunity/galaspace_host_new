<?php

namespace App\Controllers;

use App\Models\CateringBooking;
use App\Models\CateringMenu;
use App\Models\Payment;
use App\Models\Service;
use App\Models\ServiceBooking;
use App\Models\Space;
use App\Models\SpaceAvailability;
use App\Models\SpaceBooking;
use App\Models\User;
use App\Models\UserBooking;
use App\Models\UserFunction;
use Illuminate\Support\Facades\Request;
use Respect\Validation\Validator as v;

class BookingController extends Controller
{
    public function SpaceBooking($request,$response){

        $spaceID = $request->getParam('space_id');

        //syntax validation
        $validation = $this->validator->validate($request, [
            'event_type' => v::notEmpty(),
            'event_date' => v::notEmpty(),
            'event_time_start' => v::notEmpty(),
            'event_time_end' => v::notEmpty(),
            'event_guest' => v::notEmpty(),
        ]);

        if ($validation->failed())
        {
            return $response->withJson("Validation Failed");
        }
//		//end of syntax validation

        $event_type = $request->getParam('event_type');
        $event_date = $request->getParam('event_date');
        $eTS = $request->getParam('event_time_start');
        $eTE = $request->getParam('event_time_end');
        $event_guest = $request->getParam('event_guest');
        $event_description = $request->getParam('event_description');
        $jsonReturn = array();

        //Convert time picker to db
        $event_time_start = explode(":",date("H:i", strtotime($eTS)));
        $event_time_end = explode(":",date("H:i", strtotime($eTE)));

        //logic
            //involved db for checking
            $space = Space::where('space_id',$spaceID)->first();
            $spBooking = SpaceBooking::where('space_id',$spaceID)->first();
            $spAvailability = SpaceAvailability::where('space_id',$spaceID)->first();

            //get minimum booking hour(s) (in minutes)
            $spaceMinBook  = abs($space->space_min_usage * 60);
                //block if min hr not fulfilled
                $timeDiff = (($event_time_end[0] * 60.0 + $event_time_end[1] * 1.0) - ($event_time_start[0] * 60.0 + $event_time_start[1] * 1.0));
                if($spaceMinBook > $timeDiff){
                    $jsonReturn[] = "Your booking doesn't match with requirement hour(s). Required minimum booking hour for this space is ".($spaceMinBook / 60)." hour(s)";
                    return $response->withJson($jsonReturn);
                }else{
                    true;
                }
            //get day from proposed date
            $getDate = \DateTime::createFromFormat('d-m-Y',$event_date);
            $newFormatDate = "";
            $getDay = "";
            if($getDate == true) {
                $getDay = $getDate->format('l');
                $newFormatDate = $getDate->format('Y-m-d');
            }
            //check either the day proposed is available
            $space_start = "";
            $space_end = "";
            $dayAv = "";

            if($getDay === "Monday"){
               if($spAvailability->mon_available == 1){
                   $dayAv = "true";
                   $space_start = $spAvailability->mon_start;
                   $space_end = $spAvailability->mon_end;
               }else{
                   $dayAv = "false";
               }
            }elseif($getDay === "Tuesday"){
                if($spAvailability->tue_available == 1){
                    $dayAv = "true";
                    $space_start = $spAvailability->tue_start;
                    $space_end = $spAvailability->tue_end;
                }else{
                    $dayAv = "false";
                }
            }elseif($getDay === "Wednesday"){
                if($spAvailability->wed_available == 1){
                    $dayAv = "true";
                    $space_start = $spAvailability->wed_start;
                    $space_end = $spAvailability->wed_end;
                }else{
                    $dayAv = "false";
                }
            }elseif($getDay === "Thursday"){
                if($spAvailability->thu_available == 1){
                    $dayAv = "true";
                    $space_start = $spAvailability->thu_start;
                    $space_end = $spAvailability->thu_end;
                }else{
                    $dayAv = "false";
                }
            }elseif($getDay === "Friday"){
                if($spAvailability->fri_available == 1){
                    $dayAv = "true";
                    $space_start = $spAvailability->fri_start;
                    $space_end = $spAvailability->fri_end;
                }else{
                    $dayAv = "false";
                }
            }elseif($getDay === "Saturday"){
                if($spAvailability->sat_available == 1){
                    $dayAv = "true";
                    $space_start = $spAvailability->sat_start;
                    $space_end = $spAvailability->sat_end;
                }else{
                    $dayAv = "false";
                }
            }elseif($getDay === "Sunday"){
                if($spAvailability->sun_available == 1){
                    $dayAv = "true";
                    $space_start = $spAvailability->sun_start;
                    $space_end = $spAvailability->sun_end;
                }else{
                    $dayAv = "false";
                }
            }

            //check the date either it's available or booked
            $bookDt = SpaceBooking::getSpaceBookingDate($spaceID,$event_date);
            if($dayAv == "true") {
//                if ($bookDt != null) {
//                    if ($bookDt == true) {
////                        $event_start = $spBooking->spbook_time_from;
////                        $event_end = $spBooking->spbook_time_to;
////
////                        //check event time availability for user to book
////                        $c = (date("H:i:s", strtotime($eTS) >= $event_start));
////                        $d = (date("H:i:s", strtotime($eTE) >= $event_end));
////
////                        if ($c == f) {
////                            $jsonReturn[] = "The event start time are unavailable due to others usage.";
////                        } else {
////                            true;
////                        }
////
////                        if ($d == false) {
////                            $jsonReturn[] = "The event end time unavailable due to others usage.";
////                        } else {
////                            true;
////                        }
//                    }
//                }

                //check space time availability for user to book
                    $a = ($space_start <= date("H:i:s", strtotime($eTS)));
                    $b = (date("H:i:s", strtotime($eTE)) <= $space_end);

                    if ($a == false) {
                        $jsonReturn[] = "The event start time unavailable due to the space out of operational time.";
                    } else {
                        true;
                    }

                    if ($b == false) {
                        $jsonReturn[] = "The event end time unavailable due to the space out of operational time.";
                    } else {
                        true;
                    }
                } else{
                    $jsonReturn[] = "The date that you chosen is unavailable. ";
                }

                //check space running time availability for user to book. Ensure that nobody has booked that event space

            if($jsonReturn != null){
                return $response->withJson($jsonReturn);
            }
        //end of logic validation

        //create space booking id & user book id
        $ubook_id = "BOOK".$_SESSION['user_id'].date('mdYHis');
        $spbook_id = "SPBOOK".$_SESSION['user_id'].date('mdYHis');

        $user_booking = UserBooking::create([
            'book_id' => $ubook_id,
            'user_id' => $_SESSION['user_id'],
            'book_status' => 'INCOMPLETE',
        ]);

        $spbooking = SpaceBooking::create([
            'spbook_id' => $spbook_id,
            'space_id' => $spaceID,
            'book_id' => $ubook_id,
            'user_id' => $_SESSION['user_id'],
            'spbook_event_type' => $event_type,
            'spbook_date' =>  $newFormatDate,
            'spbook_time_from' => date("H:i:s", strtotime($eTS)),
            'spbook_time_to' => date("H:i:s", strtotime($eTE)),
            'spbook_guest' => $event_guest,
            'spbook_desc_msg' => $event_description,
            'spbook_status' => 'IN PROGRESS',
        ]);

        if($spbooking == true && $user_booking == true){
            $dtJson = ["success",$ubook_id];
            return $response->withJson($dtJson);
        }
    }

    public function getServiceBooking($request,$response){

        if(isset($_SESSION['spbook_id'])){
            unset($_SESSION['spbook_id']);
        }
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $bID = $url_d[4];
        $_SESSION['book_id'] = $bID;

        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
        }
        //Database involved
        $spaceBooking = SpaceBooking::where('book_id','=',$bID)->first();
        $spaces = Space::where('space_id','=',$spaceBooking->space_id)->first();
        $spaceCatering = $spaces->space_catering;
        if($spaceCatering == '"In House Catering"'){
            $services = Service::where('service_status','=','APPROVED')->leftJoin('user_function','services.service_id','=','user_function.service_id')
                ->leftJoin('service_detail','service_detail.service_id','=','services.service_id')->where('user_function.user_id','!=',$user_id)
                ->where('services.service_type','!=','Catering')->where('services.service_location','=',$spaces->space_area)->get();
            $serviceCatering = Service::where('service_status','=','APPROVED')->leftJoin('user_function','services.service_id','=','user_function.service_id')
                ->rightJoin('service_detail','service_detail.service_id','=','services.service_id')->where('user_function.user_id','!=',$user_id)->
                where('services.service_type','=','Catering')->where('services.service_location','=',$spaces->space_area)->where('services.service_by','=',$spaces->space_by)
                ->select('services.*','service_detail.*')->get();
        }else{
            $services = Service::where('service_status','=','APPROVED')->leftJoin('user_function','services.service_id','=','user_function.service_id')
                ->leftJoin('service_detail','service_detail.service_id','=','services.service_id')->where('services.service_location','=',$spaces->space_area)
                ->where('user_function.user_id','!=',$user_id)->select('services.*','service_detail.*')->get();
            $serviceCatering = Service::where('service_status','=','APPROVED')->leftJoin('user_function','services.service_id','=','user_function.service_id')
                ->rightJoin('service_detail','service_detail.service_id','=','services.service_id')->where('user_function.user_id','!=',$user_id)->
                where('services.service_type','=','Catering')->where('services.service_location','=',$spaces->space_area)
                ->select('services.*','service_detail.*')->get();
        }
        $svBookings = UserBooking::where('user_bookings.book_id',$bID)
                      ->rightJoin('service_booking','user_bookings.book_id','=','service_booking.book_id')
                      ->rightJoin('services','service_booking.service_id','=','services.service_id')
                      ->leftJoin('service_detail','service_detail.service_id','=','services.service_id')
                      ->rightJoin('users','users.user_id','=','service_booking.service_by')->get();
        $catMenu = CateringBooking::rightJoin('catering_menu','catering_booking.menu_id','=','catering_menu.menu_id')->get();

        $data = [
            'title' => 'Service Booking',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'services' => $services,
            'book_id' => $_SESSION['book_id'],
            'service_bookings' => $svBookings,
            'service_catering' => $serviceCatering,
            'catMenu' => $catMenu,
        ];

        return $this->view->render($response, 'services/service-booking.twig', $data);
    }

    public function postServiceBooking($request,$response){

        $user_id = $_SESSION['user_id'];
        $svcID = $request->getParam('service_id');
        $spaceID = $request->getParam('booking_space'); //get spaceID
        $svPrice = $request->getParam('booking_price');
        $bookID = $_SESSION['book_id'];

        //Database involved
//        $spBooking = SpaceBooking::where('user_id',$user_id)->where('book_id',$bookID)->first();
        $svBy = UserFunction::where('service_id',$svcID)->first();
        $svBooking = ServiceBooking::where('service_id',$svcID)->get();
        $spBooking = SpaceBooking::where('book_id',$bookID)->first();

        //logic
            //get event date and time
            $spbook_dt = $spBooking->spbook_date;
            $spbook_time_from = $spBooking->spbook_time_from;
            $spbook_time_to = $spBooking->spbook_time_to;

            //check either the service is available
            $evDt = "";
            $svBa = "";
            $svBb = "";
            foreach($svBooking as $svB_loop){
                $a = $svB_loop->svbook_date;
                if($a == $spbook_dt){
                    $evDt = true;
                    $svBa = $svB_loop->svbook_time_from;
                    $svBb = $svB_loop->svbook_time_to;
                }
            }

            if( $evDt == true ){
                //check event time availability for user to book
                $c = ($svBa >= $spbook_time_from);
                $d = ($svBb >= $spbook_time_to);

                if ($c == false) {
                    $jsonReturn[] = "The service start time are unavailable due to others usage.";
                } else {
                    true;
                }

                if ($d == false) {
                    $jsonReturn[] = "The service end time unavailable due to others usage.";
                } else {
                    true;
                }
            }

            //check either this service has been booked
            $svBChk = ServiceBooking::where('service_id',$svcID)->where('book_id',$bookID)->where('space_id',$spaceID)->first();
            if($svBChk != null){
                $dtJson = ["","You have been booked this. If you wish to cancel, please cancel at the service booking summary"];
                return $response->withJson($dtJson);
            }

        if($request->getParam('cateringBooking') === 'catering') {
            $menu_checked = $request->getParam('menu_checked');
            $menu_pax = $request->getParam('menu_pax');
            if ($menu_checked == "" && $menu_pax == "") {
                $dtJson = ["Please choose the menu that you wish to purchase and the amount of it"];
                return $response->withJson($dtJson);
            }
        }

        //end of logic

        $svbook_id = "SVBOOK".$_SESSION['user_id'].date('mdYHis');
        $sbdb = ServiceBooking::create([
            'svbook_id' => $svbook_id,
            'service_id' => $svcID,
            'book_id' => $bookID,
            'space_id' => $spaceID,
            'user_id' => $user_id,
            'svbook_date' => $spbook_dt,
            'svbook_time_from' => $spbook_time_from,
            'svbook_time_to' => $spbook_time_to,
            'svbook_description' => $request->getParam('detail_description'),
            'svbook_status' => "IN PROGRESS",
//            'svbook_pricing' =>
            'service_by' => $svBy->user_id,
        ]);

        if($request->getParam('cateringBooking') === 'catering'){
            $menu_checked = $request->getParam('menu_checked');
            foreach($menu_checked as $k=>$i){
                $menu_pax = $request->getParam('menu_pax'.($k+1));
                CateringBooking::create([
                    'svbook_id' => $svbook_id,
                    'menu_id' => $i,
                    'catBk_pax' => $menu_pax,
                    'catering_id' => $request->getParam('cateringID'),
                ]);
            }
        }

//        $ubook_dt = UserBooking::where('book_id',$_SESSION['book_id'])->first();
//        $ubook = new UserBooking();
//        $ubook->book_id = $_SESSION['book_id'];
//        $ubook->book_space = $ubook_dt->book_space;
//        $ubook->book_service = $svbook_id;
//        $ubook->user_id = $user_id;
//        $ubook->book_status = "INCOMPLETE";
//        $ubook->save();

        if($sbdb == true){
            $dtJson = ["success",$bookID];
            return $response->withJson($dtJson);
        }
    }

    public function deleteServiceBooking($request,$response){
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $svcID = $url_d[4];
        $book_id = $url_d[5];

        $a = ServiceBooking::where('svbook_id',$svcID)->delete();

        if($a){
            return $response->withRedirect($request->getUri()->getBasePath().'/service-booking/'.$book_id);
        }
    }

    public function getBookingOverview($request,$response){

        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
        }

        if(isset($_SESSION['cancel_booking'])){
            $cBooking = $_SESSION['cancel_booking'];
            unset($_SESSION['cancel_booking']);
        }else{
            $cBooking = "n.a.";
        };

        $url = $request->getUri();
        $url_d = explode("/", $url);
        $bookID = $url_d[4];

        //Database involved
        $bookings = UserBooking::where('book_id',$bookID)->first();
        $spBooking = SpaceBooking::where('book_id', $bookID)->rightJoin('spaces','space_booking.space_id','=','spaces.space_id')->get();
        $svBooking = ServiceBooking::where('service_booking.book_id', $bookID)->leftJoin('services','service_booking.service_id','=','services.service_id')
                     ->where('services.service_type','!=','Catering')->leftJoin('service_detail','service_detail.service_id','services.service_id')
                     ->get();
        $catList = ServiceBooking::where('service_booking.book_id', $bookID)->rightJoin('services','service_booking.service_id','=','services.service_id')
                    ->leftJoin('catering_booking','catering_booking.svbook_id','=','service_booking.svbook_id')
                    ->leftJoin('catering_menu','catering_menu.menu_id','=','catering_booking.menu_id')
                    ->where('services.service_type','=','Catering')->get();
        $catListSvcName = ServiceBooking::where('service_booking.book_id', $bookID)->rightJoin('services','service_booking.service_id','=','services.service_id')
            ->leftJoin('catering_booking','catering_booking.svbook_id','=','service_booking.svbook_id')
            ->leftJoin('catering_menu','catering_menu.menu_id','=','catering_booking.menu_id')
            ->where('services.service_type','=','Catering')->first();
        $eventSpace2 = "";
        $eventSpace5 = "";
        $eventSpace8 = "";
        $eventSpace11 = "";
        $otherService2 = array();
        $spaceCurrency = "";
        $spacePricing = "";
        $addRate = 0;
        $tmp = 0;
        $tmp2 = 0;
        $eventSpace = "<table class='table table-bordered'>
                        <tr>
                            <td style='text-align: center;vertical-align: middle;width: 22%;'><h4>Name of Event Space</h4></td>";
                            foreach ($spBooking as $i) {
                                $eventSpace2 = "<td style='vertical-align: middle;'><h4>$i->space_name</h4></td>";
                            };
        $eventSpace3 = "</tr>";
        $eventSpace4 = "<tr>
                            <td style='text-align: center;vertical-align: middle;'><h4>Address</h4></td>";
                            foreach ($spBooking as $i) {
                                $eventSpace5 = "<td style='vertical-align: middle;'><h4>$i->map_location</h4></td>";
                            };
        $eventSpace6 = "</tr>";
        $eventSpace7 = "<tr>
                            <td style='text-align: center;vertical-align: middle;'><h4>Pricing</h4></td>";
                            foreach ($spBooking as $i) {
                                $spacePricing = $i->space_pricing;
                                $spaceCurrency = $i->space_currency;
                                $eventSpace8 = "<td style='vertical-align: middle;'><h4>$spaceCurrency$spacePricing</h4></td>";
                            };
        $eventSpace9 = "</tr>";
        $eventSpace10 = "<tr>
                            <td style='text-align: center;vertical-align: middle;'><h4>Additional Pricing</h4></td>";
                            foreach ($spBooking as $i) {
                                $addRate = $i->space_add_rate;
                                $eventSpace11 = "<td style='vertical-align: middle;'><h4>$i->space_currency$addRate</h4></td>";
                            };
        $eventSpace12 = "</tr></table>";
        $cateringList = "";
        $cateringList2 = "";
        $cateringList3 = "";
        if($catListSvcName != null) {
            $cateringList2 = array();
            $cateringList = "<table class='table table-bordered'>
                        <tr>
                            <td style='text-align: center;vertical-align: middle;'><h4>Caterer</h4></td>
                            <td style='vertical-align: middle;' colspan='4'><h4>$catListSvcName->service_name</h4></td>
                        </tr>";
            foreach ($catList as $key => $j) {
                $inc = $key + 1;
                $tmp = ($j->menu_price * $j->catBk_pax);
                $cateringList2[] = "<tr><td style='text-align: center;vertical-align: middle;width: 22%;'><h4>Menu $inc Name</h4></td>
                          <td style='vertical-align: middle;'><h4>$j->menu_name</h4></td></tr>
                          <tr><td style='text-align: center;vertical-align: middle;'><h4>Menu $inc Pricing</h4></td>
                          <td style='vertical-align: middle;'><h4>$j->service_currency$tmp ($j->service_currency$j->menu_price x $j->catBk_pax pax)</h4></td></tr>";
            };
            $cateringList3 = "</table>";
        }

        $otherService = "<table class='table table-bordered'>";
        foreach ($svBooking as $ky => $k){
            $inc2 = $ky+1;
            $ts = strtotime($k->svbook_time_from);
            $te = strtotime($k->svbook_time_to);
            $hrDiff = round(abs($te-$ts)/3600,2);
            $tmp2 = ($k->detail_pricing * $hrDiff);
            $otherService2[] = "<tr><td style='text-align: center;vertical-align: middle;width: 22%;'><h4>Performer $inc2</h4></td>
                          <td style='vertical-align: middle;'><h4>$k->service_name</h4></td></tr>
                          <tr><td style='text-align: center;vertical-align: middle;'><h4>Performer $inc2 Pricing</h4></td>
                          <td style='vertical-align: middle;'><h4>$k->service_currency$tmp2 ($k->service_currency$k->detail_pricing x $hrDiff hour(s))</h4></td></tr>";
        };
        $otherService3="</table>";

        $totPricing = $spacePricing+$addRate+$tmp+$tmp2;
        $svcFee = number_format($totPricing*0.03,2);
        $totAmtToPay = $svcFee+$totPricing;
        $rTotAmtToPay = number_format($totAmtToPay,2);
        $bookSummary = "<h4>Subtotal : $spaceCurrency$totPricing</h4>
                        <h4>Service Fee : $spaceCurrency$svcFee</h4>
                        <h4><b>Total Amount to Pay : $spaceCurrency$rTotAmtToPay</b></h4>";

        //payment database
//        $payment_id = "PY".$_SESSION['user_id'].date('mdHis');
//        Payment::create([
//            'payment_id' => $payment_id,
//            'user_id' => $user_id,
//            'book_id' => $bookID,
//            'payment_generated' => date('Y-m-d'),
//            'payment_amount' => $totAmtToPay,
//            'payment_currency' => $spaceCurrency,
//            'payment_due' => date('Y-m-d',strtotime("+4 days")),
//            'payment_status' => 'UNPAID',
//        ]);

        $cL = "";
        if($cateringList2 != null){
            $cL = $cateringList.join("",$cateringList2).$cateringList3;
        }else{
            $cL = "<h3>&nbsp;&nbsp;&nbsp;You have not book any of services.</h3>";
        }
        $data = [
            'title' => 'Service Booking',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'bookings' => $bookings,
            'booking_summary' => $spBooking,
            'space_booking' => $eventSpace.$eventSpace2.$eventSpace3.$eventSpace4.$eventSpace5.$eventSpace6.$eventSpace7.$eventSpace8.$eventSpace9.$eventSpace10.$eventSpace11.$eventSpace12,
            'service_booking' => $svBooking,
            'serviceBooking' => $otherService.join("",$otherService2).$otherService3,
            'catList' => $cL,
            'bookSummary' => $bookSummary,
            'cBooking' => $cBooking,
        ];

        return $this->view->render($response, 'bookings/booking-overview.twig', $data);
    }

    public function postBookingOverview($request,$response){
        $bookID = $request->getParam("book_id");
        $eName = $request->getParam("event_name");
        $ePax = $request->getParam("pax_no");
        $eDate = $request->getParam("event_date");
        $eType = $request->getParam("event_type");

        $uBook = UserBooking::where('book_id',$bookID)->first();
        $spBook = SpaceBooking::where('book_id',$bookID)->first();
        $uBook->book_name = $eName;
        $spBook->spbook_guest = $ePax;
        $spBook->spbook_date = $eDate;
        $spBook->spbook_event_type = $eType;

        if($uBook->save() && $spBook->save()){
            $dtJson = ["success",$bookID];
            return $response->withJson($dtJson);
//            return $response->withRedirect($request->getUri()->getBasePath().'/booking-overview/'.$bookID);
        }
    }

    public function cancelBooking($request,$response){

        $book_id = $request->getParam('book_id');
        $upswd = $request->getParam('password');
        $userDt = User::where('user_id',$_SESSION['user_id'])->first();
        $pv = password_verify($upswd,$userDt->user_password);
        if($pv == true) {
            $a = UserBooking::where('book_id',$book_id)->delete();
            $b = SpaceBooking::where('book_id',$book_id)->delete();
            $c = ServiceBooking::where('book_id',$book_id)->first();
            $d = CateringBooking::where('svbook_id',$c->svbook_id)->delete();
            $e = ServiceBooking::where('book_id',$book_id)->delete();
            if($a){
                $_SESSION["cancel_booking"] = "true";
                return $response->withRedirect($request->getUri()->getBasePath().'/host-dashboard/'.$_SESSION["user_id"]);
            }
        }else{
            $_SESSION["cancel_booking"] = "false";
            return $response->withRedirect($request->getUri()->getBasePath().'/booking-overview/'.$book_id);
        }
    }

    public function getBookingSpace($request,$response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $url = $request->getUri();
        $url_d = explode("/", $url);
        $bid = $url_d[4]; //might be consider session sols

        //special code to avoid SQL injection
        $spaces = UserBooking::where('user_bookings.book_id',$bid)->rightJoin('space_booking','space_booking.book_id','=','user_bookings.book_id')
                  ->rightJoin('user_function','user_function.space_id','=','space_booking.space_id')->where('user_function.user_id',$_SESSION['user_id'])->first();
        if($spaces->book_id != $bid){
            return redirect()->route('/');
        }

        $book = UserBooking::where('user_bookings.book_id',$bid)->rightJoin('users','users.user_id','=','user_bookings.user_id')
            ->rightJoin('space_booking','space_booking.book_id','=','user_bookings.book_id')
            ->rightJoin('spaces','spaces.space_id','=','space_booking.space_id')->first();

        $data = [
            'title' => 'Admin - Booking Space',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'book' => $book,
        ];

        return $this->view->render($response, '/bookings/booking-space.twig', $data);
    }

    public function postBookingSpace($request,$response)
    {
        $book_id = $request->getParam('book_id');
        $spBook = SpaceBooking::where('book_id',$book_id)->first();
        $spBook->spbook_status = $request->getParam('book_status');
        $spBook->spbook_vendor_msg = $request->getParam('vendormsg');
        if($spBook->save()){
            //might be put email notification
            return $response->withRedirect($request->getUri()->getBasePath().'/booking-space/'.$book_id);
        }
    }

    public function getBookingService($request,$response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $url = $request->getUri();
        $url_d = explode("/", $url);
        $bid = $url_d[4]; //might be consider session sols

        //special code to avoid SQL injection
        $spaces = UserBooking::where('user_bookings.book_id',$bid)->rightJoin('service_booking','service_booking.book_id','=','user_bookings.book_id')
            ->rightJoin('user_function','user_function.service_id','=','service_booking.service_id')->where('user_function.user_id',$_SESSION['user_id'])->first();
        if($spaces->book_id != $bid){
            return redirect()->route('/');
        }

        $book = UserBooking::where('user_bookings.book_id',$bid)->rightJoin('users','users.user_id','=','user_bookings.user_id')
            ->rightJoin('service_booking','service_booking.book_id','=','user_bookings.book_id')
            ->rightJoin('services','services.service_id','=','service_booking.service_id')->first();
        $catBooking = CateringBooking::where('svbook_id',$book->svbook_id)->rightJoin('service_catering','service_catering.catering_id','=','catering_booking.catering_id')->get();
        $catMenu = CateringBooking::where('catering_booking.svbook_id',$book->svbook_id)->join('catering_menu','catering_booking.menu_id','=','catering_menu.menu_id')->get();
        $data = [
            'title' => 'Admin - Booking Space',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'book' => $book,
            'catering_booking' => $catBooking,
            'catMenu' => $catMenu,
        ];

        return $this->view->render($response, '/bookings/booking-service.twig', $data);
    }

    public function postBookingService($request,$response)
    {
        $book_id = $request->getParam('book_id');
        $spBook = SpaceBooking::where('book_id',$book_id)->first();
        $spBook->spbook_status = $request->getParam('book_status');
        $spBook->spbook_vendor_msg = $request->getParam('vendormsg');
        if($spBook->save()){
            //might be put email notification
            return $response->withRedirect($request->getUri()->getBasePath().'/booking-service/'.$book_id);
        }
    }

    public function getEventAvailability($request, $response)
    {
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $spaceId = $url_d[4]; //might be consider session sols
        $date = date("Y-m-d", strtotime($url_d[5]));

        $checkTime = SpaceBooking::where('space_id',$spaceId)->where('spbook_date',date($date))->get();
        $timeDt = [];
        $arrayMerge = [];
        foreach($checkTime as $key=>$time){
            $timeDt[$key] = array($time->spbook_time_from, $time->spbook_time_to);
            $arrayMerge = array_merge($timeDt);
        }
        return $response->withJson($arrayMerge);
    }

}