<?php

namespace App\Controllers;

use App\Models\SpaceAvailability;
use App\Models\SpaceImage;
use App\Models\UserFunction;
use Slim\Views\Twig as View;
use App\Controllers\Controller;
use Slim\Http\UploadedFile;
use App\Models\User;
use App\Models\Space;
use Respect\Validation\Validator as v;

class SpaceRegistrationController extends Controller
{
	//Space Type
	public function getSpaceType($request, $response)
	{
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

		$data = [
			'title' => 'Space Type',
			'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
		];
		return $this->view->render($response, '/space_registration/space-type.twig', $data);
	}

	public function postSpaceType($request, $response)
	{
		//validation
		$validation = $this->validator->validate($request, [
			'space_type' => v::notEmpty()->alnum(),
			'space_category' => v::noWhitespace()->notEmpty()->alnum(),
		]);

		if ($validation->failed()) {
			return $response->withRedirect($request->getUri()->getBasePath().'/space-type/'.$_SESSION['user_id']);
		}
		//end of validation

        $user = User::where('user_id', $_SESSION['user_id'])->first();
		if ($user != NULL) {
			$_SESSION['space_id'] = "SP".$_SESSION['user_id'].date('mdYHis');
			$user_function = UserFunction::create([
			    'user_id' => $_SESSION['user_id'],
			    'space_id' => $_SESSION['space_id'],
                'service_id' => '',
            ]);
			$space = Space::create([
				'space_id' => $_SESSION['space_id'],
				'space_type' => ucwords($request->getParam('space_type')),
				'space_category' => ucwords($request->getParam('space_category')),
                'space_status' => "INCOMPLETE",
                'space_by' => $_SESSION['user_id'],
			]);

            $uploadedFiles = $request->getUploadedFiles();
            //handle single input with single file upload
            $targetDir = "Users/" . $user->user_name . "/eventPic/".$_SESSION["space_id"];
            if (!file_exists($targetDir)) {
                mkdir($targetDir, 0777, true);
            }
            $targetFile = $targetDir ."/C". basename($_FILES["space_cover_photo"]["name"]);
            $check = getimagesize($_FILES["space_cover_photo"]["tmp_name"]);
            $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
            $uploadOK = 1;
            // Check file size
            if ($_FILES["space_cover_photo"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOK = 0;
            }
            //file format condition
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOK = 0;
            }
            if ($check !== false && $uploadOK != 0) {
                if ($c = move_uploaded_file($_FILES["space_cover_photo"]["tmp_name"], $targetFile)) {
                    //for database dir purpose
                    $space = Space::where('space_id',$_SESSION["space_id"])->first();
                    $space->space_main_image = $targetFile;
                    $space->save();
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            } else {
                echo "Your image is not uploaded.";
            }

            return $response->withRedirect($request->getUri()->getBasePath().'/space-details/'.$_SESSION['space_id']);
		} else {
            return $this->view->render($response, '/404.twig');
		}
	}

	//End of Space Type

	//Space Details
	public function getSpaceDetails($request, $response)
	{
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

		$data = [
			'title' => 'Space Details',
			'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
		];

		return $this->view->render($response, '/space_registration/space-details.twig', $data);
	}

	public function postSpaceDetails($request, $response)
	{
		//validation
		$validation = $this->validator->validate($request, [
			'space_guest' => v::notEmpty()->alnum(),
			'space_size' => v::notEmpty()->alnum(),
			'space_restroom' => v::notEmpty()->alnum(),
			'space_access' => v::notEmpty()->alnum()
		]);

		if ($validation->failed())
		{
			return $response->withRedirect($request->getUri()->getBasePath().'/space-details/'.$_SESSION['space_id']);
		}
//		//end of validation

        $spaces = Space::where('space_id', $_SESSION['space_id'])->first();

		if ($spaces != NULL) {
			$spaces->space_guest = $request->getParam('space_guest');
			$spaces->space_size = $request->getParam('space_size');
			$spaces->space_restroom = $request->getParam('space_restroom');
			$spaces->space_access = $request->getParam('space_access');
            $spaces->save();
			return $response->withRedirect($request->getUri()->getBasePath().'/space-address/'.$_SESSION['space_id']);
		} else {
			return $response->withRedirect($response, '/404.twig');
		}
	}
	//End of Space Details

	//Space Address
	public function getSpaceAddress($request, $response)
	{
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

		$data = [
			'title' => 'Space Address',
			'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
		];

		return $this->view->render($response, '/space_registration/space-address.twig', $data);
	}

	public function postSpaceAddress($request, $response)
	{
		//validation
		$validation = $this->validator->validate($request, [
			'space_address' => v::notEmpty(),
			'space_floor' => v::notEmpty()->alnum(),
			'space_latitude' => v::notEmpty()->floatVal(),
			'space_longitude' => v::notEmpty()->floatVal(),
		]);

		if ($validation->failed()) {
			return $response->withRedirect($request->getUri()->getBasePath().'/space-address/'.$_SESSION['space_id']);
		}
		//end of validation

		$spaces = Space::where('space_id', $_SESSION['space_id'])->first();
		// if no records then create new, else update the existing one
		if ($spaces != NULL) {
            $spaces->space_address = ucwords($request->getParam('space_address'));
            $spaces->space_floor = $request->getParam('space_floor');
            $spaces->space_latitude = $request->getParam('space_latitude');
            $spaces->space_longitude = $request->getParam('space_longitude');
            $spaces->space_place_id = $request->getParam('space_place_id');
            $spaces->map_location = ucwords($request->getParam('space_location'));
            $spaces->space_area = $request->getParam('space_area');
            if(strpos($request->getParam('space_location'),'Singapore')){
                $spaces->space_currency = "SGD$";
            }elseif (strpos($request->getParam('space_location'),'Malaysia')){
                $spaces->space_currency = "MYR";
            }
            $spaces->save();

			return $response->withRedirect($request->getUri()->getBasePath().'/space-amenities/'.$_SESSION['space_id']);
		} else {
			return $response->withRedirect($response, '/404.twig');
		}
	}
	//End of Space Address

	//Space Amenities
	public function getSpaceAmenities($request, $response)
	{
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

		$data = [
			'title' => 'Space Amenities',
			'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
		];
		return $this->view->render($response, '/space_registration/space-amenities.twig', $data);
	}

	public function postSpaceAmenities($request, $response)
	{
		if (!empty($request->getParam('space_amenities'))) {
			$amenities_list = implode(',', $request->getParam('space_amenities'));
			$amenities = json_encode($amenities_list);

            $spaces = Space::where('space_id', $_SESSION['space_id'])->first();
			if ($spaces != NULL) {
                $spaces->space_amenities = $amenities;
                $spaces->save();
                return $response->withRedirect($request->getUri()->getBasePath().'/space-description/'.$_SESSION['space_id']);
			} else {
                return $response->withRedirect($response, '/404.twig');
			}
		}
		else {
			return $response->withRedirect($request->getUri()->getBasePath().'/space-amenities/'.$_SESSION['space_id']);
		}
	}
	//End of Space Amenities

    //Space Description
    public function getSpaceDescription($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Description',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,

        ];
        return $this->view->render($response, '/space_registration/space-description.twig', $data);
    }


    public function postSpaceDescription($request, $response)
    {
		//validation
		$validation = $this->validator->validate($request, [
			'space_description' => v::notEmpty(),
		]);

		if ($validation->failed()) {
			return $response->withRedirect($request->getUri()->getBasePath().'/space-description/'.$_SESSION['space_id']);
		}
		//end of validation

        $description = Space::where('space_id', $_SESSION['space_id'])->first();
        $description->space_description = ucwords($request->getParam('space_description'));
        $description->space_known = ucwords($request->getParam('space_known'));
        $description->save();
        return $response->withRedirect($request->getUri()->getBasePath().'/space-rules/'.$_SESSION['space_id']);
    }
    //End of Space Description

    //Space Rules
    public function getSpaceRules($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Rules',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
        ];
        return $this->view->render($response, '/space_registration/space-rules.twig', $data);
    }

    public function postSpaceRules($request, $response)
    {
        $counter = 0;

        if (!empty($request->getParam('space_rules'))) {
            $rules_list = implode(',', $request->getParam('space_rules'));
            $rules = json_encode($rules_list);

            $space_detail = Space::where('space_id', $_SESSION['space_id'])->first();
            $space_detail->space_rule = $rules;
            $space_detail->save();
            $counter += 1;

            if (!empty($request->getParam('catering'))) {
                $catering_list = implode(',', $request->getParam('catering'));
                $catering = json_encode($catering_list);

                $space_detail = Space::where('space_id', $_SESSION['space_id'])->first();
                $space_detail->space_catering = $catering;
                $space_detail->save();
                $counter += 1;

                if ($counter == 2) {
                    return $response->withRedirect($request->getUri()->getBasePath().'/space-name/'.$_SESSION['space_id']);
                } else {
                    return $response->withRedirect($response, '/404.twig');
                }
            }
        } else {
            return $response->withRedirect($request->getUri()->getBasePath().'/space-rules/'.$_SESSION['space_id']);
        }
    }
    //End of Space Description

    //Space Rules
    public function getSpaceName($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Name',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
        ];
        return $this->view->render($response, '/space_registration/space-name.twig', $data);
    }

    public function postSpaceName($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'space_name' => v::notEmpty()->alnum(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($request->getUri()->getBasePath().'/space-name/'.$_SESSION['space_id']);
        }
        //end of validation

        $name = Space::where('space_id', $_SESSION['space_id'])->first();
        $name->space_name = ucwords($request->getParam('space_name'));
        $name->save();
        return $response->withRedirect($request->getUri()->getBasePath().'/space-availability/'.$_SESSION['space_id']);
    }
    //End of Space Name

    //Space Availability
    public function getSpaceAvailability($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Availability',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
        ];
        return $this->view->render($response, '/space_registration/space-availability.twig', $data);
    }

    public function postSpaceAvailability($request, $response)
    {
        $nAvailability = new SpaceAvailability;
        $nAvailability->space_id = $_SESSION['space_id'];

//        monday logic begin
        if ($request->getParam('monday_close') == "Monday Closed") {
            $monday_available = 0;
        } else {
            $monday_available = 1;
            $nAvailability->mon_start = $request->getParam('monday_from');
            $nAvailability->mon_end = $request->getParam('monday_to');
        }
        $nAvailability->mon_available = $monday_available;
//        monday logic ends

//        tuesday logic begin
        if ($request->getParam('tuesday_close') == "Tuesday Closed") {
            $tuesday_available = 0;
        } else {
            $tuesday_available = 1;
            $nAvailability->tue_start = $request->getParam('tuesday_from');
            $nAvailability->tue_end = $request->getParam('tuesday_to');
        }
        $nAvailability->tue_available = $tuesday_available;
//        tuesday logic ends

//        wednesday logic begin
        if ($request->getParam('wednesday_close') == "Wednesday Closed") {
            $wednesday_available = 0;
        } else {
            $wednesday_available = 1;
            $nAvailability->wed_start = $request->getParam('wednesday_from');
            $nAvailability->wed_end = $request->getParam('wednesday_to');
        }
        $nAvailability->wed_available = $wednesday_available;
//        wednesday logic ends

//        thursday logic begin
        if ($request->getParam('thursday_close') == "Thursday Closed") {
            $thursday_available = 0;
        } else {
            $thursday_available = 1;
            $nAvailability->thu_start = $request->getParam('thursday_from');
            $nAvailability->thu_end = $request->getParam('thursday_to');
        }
        $nAvailability->thu_available = $thursday_available;
//        thursday logic ends

//        friday logic begin
        if ($request->getParam('friday_close') == "Friday Closed") {
            $friday_available = 0;
        } else{
            $friday_available = 1;
            $nAvailability->fri_start = $request->getParam('friday_from');
            $nAvailability->fri_end = $request->getParam('friday_to');
        }
        $nAvailability->fri_available = $friday_available;
//        friday logic ends

//        saturday logic begin
        if ($request->getParam('saturday_close') == "Saturday Closed") {
            $saturday_available = 0;
        } else {
            $saturday_available = 1;
            $nAvailability->sat_start = $request->getParam('saturday_from');
            $nAvailability->sat_end = $request->getParam('saturday_to');
        }
        $nAvailability->sat_available = $saturday_available;
//        saturday logic ends

//        sunday logic begin
        if ($request->getParam('sunday_close') == "Sunday Closed") {
            $sunday_available = 0;
        } else {
            $sunday_available = 1;
            $nAvailability->sun_start = $request->getParam('sunday_from');
            $nAvailability->sun_end = $request->getParam('sunday_to');
        }
        $nAvailability->sun_available = $sunday_available;
//        sunday logic ends

        $nAvailability->save();
        return $response->withRedirect($request->getUri()->getBasePath().'/space-cpolicy/'.$_SESSION['space_id']);
    }
    //End of Space Availability

    //Space Cancellation Policy
    public function getSpaceCPolicy($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Cancellation Policy',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
        ];
        return $this->view->render($response, '/space_registration/space-cpolicy.twig', $data);
    }

    public function postSpaceCPolicy($request, $response)
    {
        $cancel = Space::where('space_id', $_SESSION['space_id'])->first();
        $cancel->space_cancellation = ucwords($request->getParam('cancel_type'));
         $cancel->save();
        return $response->withRedirect($request->getUri()->getBasePath().'/space-pricing/'.$_SESSION['space_id']);
    }
    //End of Cancellation Policy

    //Space Pricing
    public function getSpacePricing($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Pricing',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
        ];
        return $this->view->render($response, '/space_registration/space-pricing.twig', $data);
    }

    public function postSpacePricing($request, $response)
    {
        //validation
            $validation = $this->validator->validate($request, [
                'space_pricing' => v::notEmpty()->numeric(),
                'space_minimum_hour' => v::notEmpty()->numeric(),
                'space_full_day_rate' => v::notEmpty()->numeric()
            ]);
        if ($validation->failed()) {
            return $response->withRedirect($request->getUri()->getBasePath().'/space-pricing/'.$_SESSION['space_id']);
        }
        //end of validation
        $space = Space::where('space_id', $_SESSION['space_id'])->first();
        $space->space_pricing = $request->getParam('space_pricing');
        $space->space_min_usage = $request->getParam('space_minimum_hour');
        $space->space_full_rate = $request->getParam('space_full_day_rate');
        $space->space_add_rate = $request->getParam('space_additional_rate');
        $space->save();

        return $response->withRedirect($request->getUri()->getBasePath() . '/space-image/' . $_SESSION['space_id']);
    }
    //End of Space Pricing

    //Space Image
    public function getSpaceImage($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Image',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
        ];
        return $this->view->render($response, '/space_registration/space-image.twig', $data);
    }

    public function postSpaceImage($request, $response)
    {
        $user = User::where('user_id', $_SESSION['user_id'])->first();

        $uploadedFiles = $request->getUploadedFiles();

            $uFcount = count($uploadedFiles["space_photo"]);
            $tf = "Users/" . $user->user_name . "/eventPic";
            if (!file_exists($tf)) {
                mkdir($tf, 0777, true);
            }
            for($xC=0;$xC<$uFcount;$xC++) {
                $targetDir = "Users/" . $user->user_name . "/eventPic/".$_SESSION["space_id"];
                if (!file_exists($targetDir)) {
                    mkdir($targetDir, 0777, true);
                }
                $targetFile = $targetDir . "/" . basename($_FILES["space_photo"]["name"][$xC]);
                $check = getimagesize($_FILES["space_photo"]["tmp_name"][$xC]);
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["space_photo"]["size"][$xC] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if($uploadOK != 0){
                        $upDir = "Users/" . $user->user_name . "/eventPic/".$_SESSION["space_id"]."/" . basename($_FILES["space_photo"]["name"][$xC]);
                        $c = chmod($upDir, 0755);
                        boolval($c);
                        if (move_uploaded_file($_FILES["space_photo"]["tmp_name"][$xC], $upDir)) {
                            //for database dir purpose
                            $spaceImg = new SpaceImage();
                            $spaceImg->user_id = $_SESSION["user_id"];
                            $spaceImg->space_id = $_SESSION["space_id"];
                            $spaceImg->space_index = $xC+1;
                            $spaceImg->space_image_path = $upDir;
                            $spaceImg->save();
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    }else{
                         echo "Your image is not uploaded.";
                    }
                }else {

                }
            }
        return $response->withRedirect($request->getUri()->getBasePath().'/space-law/'.$_SESSION['space_id']);
    }
    //End of Space Image

    //Space Law
    public function getSpaceLaw($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_full_name = "";
            $user_avatar = "";
        }

        $data = [
            'title' => 'Space Law',
            'space_id' => $_SESSION['space_id'],
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
        ];
        return $this->view->render($response, '/space_registration/space-law.twig', $data);
    }

    public function postSpaceLaw($request, $response)
    {
        $user = User::where('user_id',$_SESSION['user_id'])->first();
        $application = Space::where('space_id', $_SESSION['space_id'])->first();
        $application->space_status = "PENDING";
        $application->save();

        //this part where we have to produce the modal and stated that the application has been submitted and we'll review it
        return $response->withRedirect($request->getUri()->getBasePath() . '/host-dashboard/' . $_SESSION['user_id']);
    }
    //End of Space Law

}
