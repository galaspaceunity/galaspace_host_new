<?php

namespace App\Controllers;

use App\Models\ServiceBooking;
use App\Models\SpaceBooking;
use App\Models\UserBooking;
use Illuminate\Http\Request;
use Mpdf\Mpdf;

class ReportController extends Controller
{
    public function bookingSummary($request,$response)
    {
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $bookID = $url_d[4]; //might be consider session sols

        //Database involved
        $bookings = UserBooking::where('book_id',$bookID)->rightJoin('users','users.user_id','=','user_bookings.user_id')->first();
        $spBooking = SpaceBooking::where('book_id', $bookID)->join('spaces','space_booking.space_id','=','spaces.space_id')->get();
        $svBooking = ServiceBooking::where('service_booking.book_id', $bookID)->rightJoin('services','service_booking.service_id','=','services.service_id')
        ->leftJoin('catering_booking','catering_booking.svbook_id','=','service_booking.svbook_id')
        ->leftJoin('catering_menu','catering_menu.menu_id','=','catering_booking.menu_id')->get();

            $currency = "";
            $rate=0;
            $subtotal=0;
            $total_cost=0;
            $splist="";
            $svlist="";
            foreach($spBooking as $i) {
                if($i->space_area == "Singapore"){
                    $currency = "SGD$";
                }elseif ($i->space_area == "Sarawak"){
                    $currency = "RM";
                }
                $ts = strtotime($i->spbook_time_from);
                $te = strtotime($i->spbook_time_to);
                $rate = $i->space_pricing;
                $hrDiff = round(abs($te-$ts)/3600,2);
                $subtotal = ($hrDiff * $rate);
                $total_cost += $subtotal;
                $splist = "<tr class='item'>
                            <td>".$i->space_name."</td>
                            <td style='text-align: center;'>".$rate."</td>
                            <td style='text-align: center;'>".$hrDiff."</td>
                            <td style='text-align: center;'>".$subtotal."</td>
                        </tr>";
            };
            foreach($svBooking as $j) {
                $ts = strtotime($j->svbook_time_from);
                $te = strtotime($j->svbook_time_to);
                $hrDiff = round(abs($te-$ts)/3600,2);
                $rate = $j->service_pricing;
                $subtotal = ($hrDiff * $rate);
                $total_cost += $subtotal;
                $svlist = "<tr class='item'>
                                <td>".$i->space_name."</td>
                                <td style='text-align: center;'>".$rate."</td>
                                <td style='text-align: center;'>".$hrDiff."</td>
                                <td style='text-align: center;'>".$subtotal."</td>
                            </tr>";
            };
            $svFee = ($total_cost * 0.03);
            $disc = ($svFee / 0.02);
            $gt = (($total_cost + $svFee) - $disc);
            $total = "<tr class='total'>
                        <td colspan='3'>Total Due:<br>".$currency.$gt."<br>Due Date: <br><br> Payment Methods: 
                        <br>Paypal: billings@galaspace.com <br>
                        Accepted Card Payment : Visa & Mastercard
                        </td>
                        <td style='padding:10px;'>
                            Subtotal : ".$currency.$total_cost."<br>
                            Service Fee (3%) : ".$svFee."<br>
                            Discount(2%) : ".$disc."<br><br>
                            <span style='background-color:#00a99d; padding:15px;color:#fff;'>
                            Grand Total : ".$currency.$gt."
                            </span>
                        </td>
                      </tr>";
        $data = [
            'title' => 'Booking Invoice',
            'date_now' => date('d-m-Y'),
            'bookings' => $bookings,
            'space_bookings' => $spBooking,
            'splist' => $splist,
            'svlist' => $svlist,
            'total' => $total,
        ];

        return $this->view->render($response, 'report/invoice.twig',$data);

    }
    public function downloadPDF($request){
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $bookID = $url_d[4]; //might be consider session sols

        $grabzIt = new \GrabzIt\GrabzItClient("NmRlZTM0YWFkNWM1NDU1MTgzNzY3YThmNDdkZjU1MTQ=", "P2Y/Pz9fPz8/VxM/Bz9TP0ANDD9sPxF0Pz9SPy85CxM=");

        $options = new \GrabzIt\GrabzItPDFOptions();
        $options->setCustomId(123456);

        $grabzIt->URLToPDF("https://www.galaspace.com/booking-summary/".$bookID, $options);

        $grabzIt->Save("test.pdf");
    }
}
