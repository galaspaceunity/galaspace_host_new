<?php

namespace App\Controllers;

use App\Mail\SwiftMailer;
use Illuminate\Support\Facades\App;
use Slim\Views\Twig as View;
use App\Models\User;
use App\Models\UserCompany;
use Respect\Validation\Validator as v;


class UserRegistrationController extends Controller
{
    //User Registration
    public function getSignUp($request, $response)
    {
        $data = [
            'title' => 'Signup'
        ];

        return $this->view->render($response, '/user_registration/signup.twig', $data);
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function postSignUp($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'username' => v::noWhitespace()->notEmpty()->length(5)->alnum()->usernameTaken(),
            'title_name' => v::notEmpty(),
            'full_name' => v::notEmpty()->alpha(),
            'email_address' => v::noWhitespace()->notEmpty()->email()->emailTaken(),
            'password' => v::noWhitespace()->notEmpty()->length(8)->alnum(),
            'confirm_password' => v::confirmPassword($request->getParam('password'))->noWhitespace()->notEmpty(),
//            'user_type' => v::notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor('user_registration.signup'));
        }
        //end of validation

        //insertion
        $_SESSION['user_name'] = $request->getParam('username');
        $user = User::create([
            'user_name' => $_SESSION['user_name'],
            'user_title' => ucwords($request->getParam('title_name')),
            'user_full_name' => ucwords($request->getParam('full_name')),
            'user_email' => $request->getParam('email_address'),
            'user_password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT, ['cost' => 12]),
//            'user_type' => $request->getParam('user_type'),
            'user_type' => 'host_subhost',
            'user_token' => (md5(uniqid(rand(), true))),
        ]);

        if ($request->getParam('title_name') == "Company") {
            return $response->withRedirect($request->getUri()->getBasePath() . '/company-description/' . $_SESSION['user_name']);
        } else {
            //email confirmation link
//        $this->mailer->send('confirm-token.twig', ['token' => $user->user_token, 'full_name' => $user->user_full_name] , function($message) use ($user){
//          $message->to($user->user_email);
//          $message->subject('Token Confirmation Title');
//          $message->from('confirmation@galaspace.com');
//          $message->fromName('Galaspace UserCompany');

//            $msg = "<h1>Galaspace Account Confirmation</h1>
//                    <p>Hey ".$user->user_full_name.", you're almost ready to enter the world of Galaspace. Simply click the link below to verify your account.</p>
//                    <a href=\"https://www.galaspace.com/verify-token/$user->user_token\">Click Here</a>";
//            $sm = new SwiftMailer();
//            $sm->sendEmail($user->user_email,'Galaspace Confirmation',$msg);
        }
        //end of email confirmation link

        $this->container->flash->addMessage('info', "Please login. Thank you!");
        return $response->withRedirect($request->getUri()->getBasePath() . '/');
    }
	//End of User Registration

	//Profile photo
	public function getProfilePhoto($request, $response)
	{
		$data = [
			'title' => 'Profile Photo',
			'id' => $_SESSION['user_id']
		];
		return $this->view->render($response, '/user_registration/profile-photo.twig', $data);
	}

	public function postProfilePhoto($request, $response)
	{

	}
	//End of Profile photo

	//Company Description
	public function getCompanyDescription($request, $response)
	{
        $user = User::where('user_name',$_SESSION['user_name'])->get();
        foreach($user as $users){
            $type =  $users->user_type;
        }
        if($type == 'subhost'){
            $a = 1;
        }else if ($type == 'host_subhost'){
            $a = 1;
        }else{
            $a = 0;
        }

		$data = [
			'title' => 'Company Description',
			'user_name' => $_SESSION['user_name'],
            'user_type' => $a
		];
		return $this->view->render($response, '/user_registration/company-description.twig', $data);
	}

	public function postCompanyDescription($request, $response)
	{
        //validation
        $validation = $this->validator->validate($request, [
            'company_name' => v::notEmpty(),
            'company_description' => v::notEmpty()
        ]);

        if ($validation->failed())
        {
            return $response->withRedirect($request->getUri()->getBasePath().'/company-description/'.$_SESSION['user_name']);
        }
        //end of validation

        //insertion
        $company = UserCompany::create([
            'company_name' => ucwords($request->getParam('company_name')),
            'company_description' => $request->getParam('company_description'),
        ]);
            $a = ucwords($request->getParam('company_name'));
            $b = $request->getParam('company_description');

        //update user table
        $company = UserCompany::where('company_name',$a)->where('company_description',$b)->first();
        $_SESSION['company_id'] = $company->company_id;
        $user = User::where('user_name', $_SESSION['user_name'])->update(['company_id' =>  $_SESSION['company_id']]);

        return $response->withRedirect($request->getUri()->getBasePath().'/company-location/'.$_SESSION['user_name']);
	}
	//End of Company Description

	//Company Location
	public function getCompanyLocation($request, $response)
	{
		$data = [
			'title' => 'Company Location',
			'user_name' => $_SESSION['user_name'],
            'company_id' => $_SESSION['company_id']
		];
		return $this->view->render($response, '/user_registration/company-location.twig', $data);
	}

	public function postCompanyLocation($request, $response)
	{
        //validation
        $validation = $this->validator->validate($request, [
            'company_location' => v::notEmpty(),
//            'company_radius' => v::notEmpty()
        ]);

        if ($validation->failed())
        {
            return $response->withRedirect($request->getUri()->getBasePath().'/company-location/'.$_SESSION['user_name']);
        }
        //end of validation

        $location = UserCompany::where('company_id',$_SESSION['company_id'])->first();

        $location->company_location = ucwords($request->getParam('company_location'));
//        $location->company_radius = $request->getParam('company_radius');
        $location->save();

        //db query for user token
        $user = User::where('user_name', $_SESSION['user_name'])->first();
//        foreach ($user as $users){
//            $full_name = $users->user_full_name;
//            $token = $users->user_token;
//        }
        //db query ends

        //email confirmation link
//        $this->mailer->send('confirm-token.twig', ['token' => $token, 'full_name' => $full_name] , function($message) use ($users){
//            $message->to($users->user_email);
//            $message->subject('Token Confirmation Title');
//            $message->from('confirmation@galaspace.com');
//            $message->fromName('Galaspace Company');
//        });

//        $msg = "<h1>Galaspace Account Confirmation</h1>
//                <p>Hey ".$user->user_full_name.", you're almost ready to enter the world of Galaspace. Simply click the link below to verify your account.</p>
//                <a href=\"https://www.galaspace.com/verify-token/$user->user_token\">Click Here</a>";
//        $sm = new SwiftMailer();
//        $sm->sendEmail($user->user_email,'Galaspace Confirmation',$msg);
        //end of email confirmation link

        $this->container->flash->addMessage('info', "Please login. Thank you!");
        return $response->withRedirect($request->getUri()->getBasePath() . '/');
    }
	//End of UserCompany Location
}
