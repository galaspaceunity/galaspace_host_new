<?php

namespace App\Controllers;

use App\Models\CateringBooking;
use App\Models\Contact;
use App\Models\Service;
use App\Models\ServiceBooking;
use App\Models\Space;
use App\Models\SpaceBooking;
use App\Models\User;
use App\Models\UserBooking;
use App\Models\UserCompany;

class AdministratorController extends Controller
{
    public function getAdminDashboard($request,$response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $users = User::get();
        $spaces = Space::get();
        $service = Service::get();
        $bookings = UserBooking::get();
        $webC = Contact::get();

        $data = [
            'title' => 'Admin Dashboard',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'users_dt' => $users,
            'space_dt' => $spaces,
            'service_dt' => $service,
            'booking_dt' => $bookings,
            'contact' => $webC,
        ];

        return $this->view->render($response, '/administrator/admin-dashboard.twig', $data);
    }

    public function getUserDetails($request,$response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $url = $request->getUri();
        $url_d = explode("/", $url);
        $uid = $url_d[5]; //might be consider session sols

        $user = User::where('user_id',$uid)->first();
        $company = UserCompany::where('company_id',$user->company_id)->first();

        $data = [
            'title' => 'Admin - User Details',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'user' => $user,
            'company' => $company,
        ];

        return $this->view->render($response, '/administrator/user-details.twig', $data);
    }

    public function getSpaceDetails($request,$response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $url = $request->getUri();
        $url_d = explode("/", $url);
        $sid = $url_d[5]; //might be consider session sols

        $space = Space::where('space_id',$sid)->first();
        $space_bookings = SpaceBooking::where('space_id',$sid)->rightJoin('users','users.user_id','=','space_booking.user_id')->get();

        $data = [
            'title' => 'Admin - User Details',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'space' => $space,
            'spbook' => $space_bookings,
        ];

        return $this->view->render($response, '/administrator/space-details.twig', $data);
    }

    public function postSpaceDetails($request,$response)
    {
        $sid = $request->getParam('spaceid');
        $sp = Space::where('space_id',$sid)->first();
        $sp->space_status = $request->getParam('status');
        if($sp->save()){
            echo "<script type='text/javascript'> 
                    alert('Successfully saved the data');
                  </script>";
            return $response->withRedirect($request->getUri()->getBasePath().'/admin/space-details/'.$sid);
        }
    }

    public function getServiceDetails($request,$response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $url = $request->getUri();
        $url_d = explode("/", $url);
        $sid = $url_d[5]; //might be consider session sols

        $service = Service::where('service_id',$sid)->first();
        $svbook = ServiceBooking::where('service_id',$sid)->rightJoin('users','users.user_id','=','service_booking.user_id')->get();

        $data = [
            'title' => 'Admin - User Details',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'service' => $service,
            'svbook' => $svbook,
        ];

        return $this->view->render($response, '/administrator/service-details.twig', $data);
    }

    public function postServiceDetails($request,$response)
    {
        $sid = $request->getParam('serviceid');
        $sv = Service::where('service_id',$sid)->first();
        $sv->service_status = $request->getParam('status');
        if($sv->save()){
            echo "<script type='text/javascript'> 
                    alert('Successfully saved the data');
                  </script>";
            return $response->withRedirect($request->getUri()->getBasePath().'/admin/service-details/'.$sid);
        }
    }

    public function getBookDetails($request,$response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $url = $request->getUri();
        $url_d = explode("/", $url);
        $bid = $url_d[5]; //might be consider session sols

        $book = UserBooking::where('book_id',$bid)->rightJoin('users','users.user_id','=','user_bookings.user_id')->first();
        $spbook = SpaceBooking::where('book_id', $book->book_id)->first();
        $svbook = UserBooking::where('user_bookings.book_id',$book->book_id)
            ->rightJoin('service_booking','user_bookings.book_id','=','service_booking.book_id')
            ->rightJoin('services','service_booking.service_id','=','services.service_id')
            ->rightJoin('users','users.user_id','=','service_booking.service_by')->get();
        $catMenu = CateringBooking::leftJoin('catering_menu','catering_booking.menu_id','=','catering_menu.menu_id')->get();

        $data = [
            'title' => 'Admin - Book Details',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'book' => $book,
            'spbook' => $spbook,
            'svbookings' => $svbook,
            'catMenu' => $catMenu,
        ];

        return $this->view->render($response, '/administrator/book-details.twig', $data);
    }

}
