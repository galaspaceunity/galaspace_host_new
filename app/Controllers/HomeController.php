<?php

namespace App\Controllers;

use App\Models\Contact;
use App\Models\PlanEvent;
use App\Models\Space;
use Illuminate\Support\Facades\Request;
use ReCaptcha\ReCaptcha;
use ReCaptcha\RequestMethod\Curl;
use ReCaptcha\RequestMethod\CurlPost;
//use ReCaptcha\Response;
use Respect\Validation\Validator as v;



class HomeController extends Controller
{
    public function index($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Galaspace Homepage',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'home.twig', $data);
    }

    public function aboutUs($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'About Us',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'about-us.twig', $data);
    }

    public function requestSpace($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Request Space',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'request-space.twig', $data);
    }

    public function communityFeed($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Request Space',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'community-feed.twig', $data);
    }

    public function press($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Press',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'press.twig', $data);
    }

    public function careers($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Careers',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'careers.twig', $data);
    }

    public function faq($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'FAQ',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'faq.twig', $data);
    }

    public function getStartedSpaceOwner($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Get Started Space Owner',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'get-started-space-owner.twig', $data);
    }

    public function getStartedOrganizer($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Get Started Event Organizer',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'get-started-organizer.twig', $data);
    }

    public function getStartedServiceProvider($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Get Started Event Service Provider',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'get-started-service-provider.twig', $data);
    }

    public function getContact($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        if(isset($_SESSION["contact_success"])){
            $contSuccess = $_SESSION["contact_success"];
            unset($_SESSION["contact_success"]);
        }else{
            $contSuccess = "n.a.";
        }

        $data = [
            'title' => 'Contact Us',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'contact_success' => $contSuccess,
        ];

        return $this->view->render($response, 'contact.twig', $data);
    }

    public function postContact($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'full_name' => v::notEmpty(),
            'email_address' => v::noWhitespace()->notEmpty()->email(),
            'user_info' => v::notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor('Contact Us'));
        }
        $secret = "6Leac2QUAAAAAK9hSvuzW259tyPImL4xpZFKXvEN";
        $recaptcha = new ReCaptcha($secret, new CurlPost());
        $verify = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        //end of validation

        if($verify->isSuccess()){
            $aa = Contact::create([
                'contact_full_name' => ucwords($request->getParam('full_name')),
                'contact_email' => $request->getParam('email_address'),
                'contact_message' => $request->getParam('user_info'),
            ]);
            if($aa == true){
                $_SESSION["contact_success"] = "true";
            }else{
                $_SESSION["contact_success"] = "false";
            }
            return $response->withRedirect($request->getUri()->getBasePath().'/contact');
        }else{
            $_SESSION["contact_success"] = "falseVerify";
            return $response->withRedirect($request->getUri()->getBasePath().'/contact');
        }

    }

    public function tos($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Terms Of Service',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'terms.twig', $data);
    }

    public function privacyPolicy($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Privacy Policy',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'privacy-policy.twig', $data);
    }

    public function cancellationPolicy($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        $data = [
            'title' => 'Cancellation Policy',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
        ];

        return $this->view->render($response, 'cancellation-policy.twig', $data);
    }

    public function eventSpaces($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        //request from AJAX src
        if($this->request->isXhr() == true){
            //get filter data
            $eType = $request->getParam('eType');
            $eLocation = $request->getParam('eLocation');

            $eSpace = Space::where('space_status','APPROVED')->leftJoin('user_function','spaces.space_id','=','user_function.space_id')
                ->where('user_function.user_id','!=',$user_id);

            if($eType != ""){
                $eSpace->where('space_type',$eType);
            }
            if($eLocation != ""){
                if($eLocation[0] == "Sarawak") {
                    $eSpace->where('space_area', 'Sarawak');
                    $eSpace->orWhere('space_area', 'Kuching');
                    $eSpace->orWhere('space_area', 'Malaysia');
                }else{
                    $eSpace->where('space_area', $eLocation);
                }
            }
            $ss = $eSpace->get();
            return $response->withJson($ss);
        }

        //if select from home options
        $eType = $request->getParam('eType');
        $eLocation = $request->getParam('eLocation');

        $eSpaceH = Space::where('space_status','=','APPROVED')->leftJoin('user_function','spaces.space_id','=','user_function.space_id')
            ->where('user_function.user_id','!=',$user_id);
        if($eType != ""){
            if($eType != "allTypes"){
                $eSpaceH->where('space_type',$eType);
            }
        }
        if($eLocation != ""){
            if($eLocation == "Sarawak") {
                $eSpaceH->where('space_area', 'Sarawak');
                $eSpaceH->orWhere('space_area', 'Kuching');
                $eSpaceH->orWhere('space_area', 'Malaysia');
            }else{
                $eSpaceH->where('space_area', $eLocation);
            }
        }
        $ss = $eSpaceH->get();

        $data = [
            'title' => 'Event Spaces',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'spaces' => $ss,
            'eType' => $eType,
            'eLocation' => $eLocation
        ];

        return $this->view->render($response, 'event-spaces.twig', $data);

    }

//    public function postEventSpaces($request, $response) : Response
//    {
//        if (isset($_SESSION['user_id'])) {
//            $user_id = $_SESSION['user_id'];
//        } else {
//            $user_id = "";
//        }
//
//        //query event spaces
//        $eSpace = Space::where('space_status','APPROVED')->get();
//        $dte = $eSpace->toArray();
//
//        return $this->view->render($response, 'event-spaces.twig');
//    }

    public function getPlanEvent($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
            $user_admin = $_SESSION['user_admin'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
            $user_admin = "";
        }

        if(isset($_SESSION["event_enquire"])){
            $eventEnquire = $_SESSION["event_enquire"];
            unset($_SESSION["event_enquire"]);
        }else{
            $eventEnquire = "n.a.";
        }

        $data = [
            'title' => 'Plan an Event',
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_admin' => $user_admin,
            'eventEnquire' => $eventEnquire,
        ];

        return $this->view->render($response, 'plan-an-event.twig', $data);
    }

    public function postPlanEvent($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'event_plan' => v::notEmpty(),
            'event_budget' => v::numeric(),
            'event_location' => v::notEmpty(),
            'event_dt' => v::notEmpty(),
            'event_guest' => v::numeric(),
            'event_services' => v::notEmpty(),
            'event_request' => v::notEmpty(),
            'full_name' => v::notEmpty(),
            'email_address' => v::noWhitespace()->notEmpty()->email(),
            'contact_number' => v::noWhitespace()->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($request->getUri()->getBasePath().'/plan-an-event/');
        }
        //end of validation

        //event service
        $evSvc = "";
        if (!empty($request->getParam('event_services'))) {
            $event_services = implode(',', $request->getParam('event_services'));
            $evSvc = json_encode($event_services);
        }
        $ev = PlanEvent::create([
            'event_plan' => $request->getParam('event_plan'),
            'event_budget' => $request->getParam('event_budget'),
            'event_location' => $request->getParam('event_location'),
            'event_dt' => $request->getParam('event_dt'),
            'event_guest' => $request->getParam('event_guest'),
            'event_services' => $evSvc,
            'event_request' => $request->getParam('event_request'),
            'event_name' => $request->getParam('full_name'),
            'event_email' => $request->getParam('email_address'),
            'event_contact' => $request->getParam('contact_number'),
        ]);

        if($ev == true){
            $_SESSION["event_enquire"] = "true";
        }else{
            $_SESSION["event_enquire"] = "false";
        }
        return $response->withRedirect($request->getUri()->getBasePath().'/plan-an-event');
    }
}
