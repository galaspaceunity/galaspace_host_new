<?php

namespace App\Controllers;

use App\Mail\SwiftMailer;
use Slim\Views\Twig as View;
use App\Controllers\Controller;
use App\Models\User;
use Respect\Validation\Validator as v;

class AuthController extends Controller
{
	//Logout
	public function getSignOut($request, $response)
	{
		$this->auth->logout();

		return $response->withRedirect($this->router->pathFor('home'));
	}

	//Registration Token
	public function getConfirmToken($request, $response)
	{
		$data = [
			'title' => 'Confirm Token',
		];

		return $this->view->render($response, 'confirm-token.twig', $data);
	}

	public function getVerifyToken($request, $response)
	{
		$data = [
			'title' => 'Verify Token',
		];

		$user = User::where('user_token', '=' ,$request->getAttribute('user_token'))->first();
		if ($user == true) {
		    $user = User::find($user->user_id);
			$user->user_confirm = 1;
			$user->user_token = '0';
			$user->save();

			//create file for users where it keeps their data
            mkdir("Users/$user->user_name/profile_picture",0777,true);
            mkdir("Users/$user->user_name/documents",0777,true);
            mkdir("Users/$user->user_name/event_picture",0777,true);
            mkdir("Users/$user->user_name/miscellaneous",0777,true);
            $this->container->flash->addMessage('success', "Your account has been successfully confirmed! Enjoy Galaspace.");
		} else {
			$this->container->flash->addMessage('error', 'Sorry, the token was already expired.');
		}
        return $response->withRedirect($this->router->pathFor('auth.signin'));
//        return $this->view->render($response, 'verify-token.twig', $data);
	}
	//End of Registration Token

	//User Login
	public function getSignIn($request, $response)
	{
		$data = [
			'title' => 'Signin',
		];

		return $this->view->render($response, 'auth/signin.twig', $data);
	}

    public function postSignIn($request, $response)
    {
        //validation
        $validation = $this->validator->validate($request, [
            'username' => v::noWhitespace()->notEmpty()->alnum(),
            'password' => v::noWhitespace()->notEmpty()->alnum(),
        ]);

        if ($validation->failed())
        {
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }
        //end of validation

//        if ($user->user_confirm === 1) {
            $auth = $this->auth->attempt(
                $request->getParam('username'),
                $request->getParam('password')
            );

            if (!$auth) {
//                $this->container->flash->addMessage('error', "The username or password is incorrect.");
                return $response->withRedirect($this->router->pathFor('auth.signin'));
            }
//            if($_SESSION["user_admin"] == "true"){
//                return $response->withRedirect($request->getUri()->getBasePath().'/admin-dashboard/'.$_SESSION['user_id']);
//            }else{
//                return $response->withRedirect($request->getUri()->getBasePath().'/host-dashboard/'.$_SESSION['user_id']);
//            }

//        } else {
//            $this->container->flash->addMessage('error', "Please check the email confirmation first.");
//            return $response->withRedirect($this->router->pathFor('auth.signin'));
//        }

    }
    //End of User Login

	//User Reset Password
	public function getResetPassword($request, $response)
	{
		$data = [
			'title' => 'Reset Password'
		];

		return $this->view->render($response, 'auth/reset-password.twig', $data);
	}

	public function postResetPassword($request, $response)
	{
		//validation
		$validation = $this->validator->validate($request, [
			'email_address' => v::noWhitespace()->notEmpty()->email(),
		]);

		if ($validation->failed())
		{
			return $response->withRedirect($this->router->pathFor('auth.reset-password'));
		}
		//end of validation

		$user = User::where('user_email', '=' ,$request->getParam('email_address'))->first();

		if ($user == true) {
			$user = User::find($user->user_id);
			$user->user_token = md5(uniqid(rand(), true));
			$user->save();
			//email confirmation link
//			$this->mailer->send('confirm-reset.twig', ['token' => $user->user_token, 'full_name' => $user->user_full_name] , function($message) use ($user){
//				  $message->to($user->user_email);
//				  $message->subject('Reset Password Token');
//				  $message->from('team@galaspace.com');
//				  $message->fromName('Galaspace UserCompany');
//			});

            $msg = "<h1>Galaspace Reset Password Confirmation</h1>
                    <p>Hey ".$user->user_full_name.", you're about to reset your password. Simply click the link below to continue.</p>
                    <a href=\"https://www.galaspace.com/verify-reset/$user->user_token\">Click Here</a>
                    <p>If you're not aware of this message, simply ignore. Thank you.</p>";
            $sm = new SwiftMailer();
            $sm->sendEmail($user->user_email,'Reset Password Token',$msg);
			//end of email confirmation link
            return redirect()->back();
		} else {
			echo "No email address of that kind.";
		}
	}
	//End of User Reset Password

	//Reset Token
	public function getConfirmReset($request, $response)
	{
		$data = [
			'title' => 'Confirm Reset Password',
		];

		return $this->view->render($response, 'confirm-reset.twig', $data);
	}

	public function getVerifyReset($request, $response, $args)
	{
		$tk = $args['user_token'];
		$message = "";
		$data = [
			'title' => 'New Password',
			'token' => $tk,
			'message' => $message
		];

		$user = User::where('user_token', '=' , $tk)->first();
		if ($user == false) {
			$message = "Sorry, invalid code has been detected!";
			echo $message;
		} else {
			return $this->view->render($response, 'verify-reset.twig', $data);
		}
	}

	public function postVerifyReset($request, $response, $args)
	{
		$token = $args['user_token'];

		//validation
		$validation = $this->validator->validate($request, [
			'password' => v::noWhitespace()->notEmpty()->length(8)->alnum()->mightyPassword(),
			'confirm_password' => v::confirmPassword($request->getParam('password'))->noWhitespace()->notEmpty(),
		]);

		if ($validation->failed())
		{
			return $response->withRedirect($request->getUri()->getBasePath().'/verify-reset/'.$token);
		}
		//end of validation

		$user = User::where('user_token', '=' ,$token)->first();
		if ($user == true) {
			$user = User::find($user->user_id);
			$user->user_token = '0';
			$user->user_password = password_hash($request->getParam('password'), PASSWORD_DEFAULT, ['cost' => 12]);
			$user->save();
			$message = "Your password has been successfully changed!.";
		} else {
			$message = "Sorry, failed to change password";
		}

		$data = [
			'title' => 'New Password',
			'token' => $token,
		];

		return $response->withRedirect($this->router->pathFor('auth.signin'));
	}

}
