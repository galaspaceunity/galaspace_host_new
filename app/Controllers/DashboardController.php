<?php

namespace App\Controllers;

use App\Models\CateringMenu;
use App\Models\EventDetail;
use App\Models\Service;
use App\Models\ServiceCatering;
use App\Models\ServiceDetail;
use App\Models\Space;
use App\Models\SpaceAvailability;
use App\Models\SpaceBooking;
use App\Models\SpaceImage;
use App\Models\UserBooking;
use App\Models\UserFunction;
use App\Models\Message;
//use http\Env\Request;
use Respect\Validation\Exceptions\FileException;
use Slim\Views\Twig as View;
use App\Models\User;
use App\Models\Event;
use Slim\Http\UploadedFile;
use Slim\App;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;

class DashboardController extends Controller
{
	//Host Dashboard
	public function getHostDashboard($request, $response)
	{
        $user = User::where('user_id', $_SESSION['user_id'])->first();
        $user_type = $user->user_type;
        $user_image = $user->user_image;

		$space = UserFunction::where('user_id',$_SESSION['user_id'])
               ->rightJoin('spaces','user_function.space_id','=','spaces.space_id')->get();

		$service = UserFunction::where('user_id',$_SESSION['user_id'])
                ->rightJoin('services','user_function.service_id','=','services.service_id')->get();

		$booking_user_space = UserBooking::where('user_bookings.user_id',$_SESSION['user_id'])->join('space_booking','user_bookings.book_id','=','space_booking.book_id')
                              ->join('spaces','spaces.space_id','=','space_booking.space_id')->get();

        $booking_user_service = UserBooking::where('user_bookings.user_id',$_SESSION['user_id'])->rightJoin('service_booking','user_bookings.book_id','=','service_booking.book_id')
            ->leftJoin('services','services.service_id','=','service_booking.service_id')->get();

		$booking_host_space = UserFunction::where('user_function.user_id',$_SESSION["user_id"])->rightJoin('spaces','spaces.space_id','=','user_function.space_id')
                        ->rightJoin('space_booking','space_booking.space_id','=','spaces.space_id')->rightJoin('user_bookings','user_bookings.book_id','=','space_booking.book_id')->get();

        $booking_host_service = UserFunction::where('user_function.user_id',$_SESSION["user_id"])->rightJoin('services','services.service_id','=','user_function.service_id')
            ->rightJoin('service_booking','service_booking.service_id','=','services.service_id')->rightJoin('user_bookings','user_bookings.book_id','=','service_booking.book_id')->get();

            $messages = Message::where('user_id', $_SESSION['user_id'])->orWhere('receiver_id',$_SESSION["user_id"])->groupBy('user_id')->get();

		if(isset($_SESSION['profile_success'])){
		    $pSuccess = $_SESSION['profile_success'];
		    unset($_SESSION['profile_success']);
        }else{
		    $pSuccess = "n.a.";
        };
        if(isset($_SESSION['security_success'])){
            $sSuccess = $_SESSION['security_success'];
            unset($_SESSION['security_success']);
        }else{
            $sSuccess = "n.a.";
        };

        if(isset($_SESSION['space_del'])){
            $spdUpdate = $_SESSION['space_del'];
            unset($_SESSION['space_del']);
        }else{
            $spdUpdate = "n.a.";
        };

        if(isset($_SESSION['service_del'])){
            $svdUpdate = $_SESSION['service_del'];
            unset($_SESSION['service_del']);
        }else{
            $svdUpdate = "n.a.";
        };

        if(isset($_SESSION['cancel_booking'])){
            $cBooking = $_SESSION['cancel_booking'];
            unset($_SESSION['cancel_booking']);
        }else{
            $cBooking = "n.a.";
        };

		$data = [
			'title' => 'Host Dashboard',
			'user_id' => $_SESSION['user_id'],
            'user_type' => $user_type,
            'user_image' => $user_image,
			'spaces' => $space,
            'services' => $service,
			'full_name' => $user->user_full_name,
			'email_address' => $user->user_email,
			'user_description' => ucfirst($user->user_description),
            'booking_user_space' => $booking_user_space,
            'booking_user_service' => $booking_user_service,
            'booking_hosts_space' => $booking_host_space,
            'booking_hosts_service' => $booking_host_service,
            'hosts_chats' => $this->getUserChats($messages),

            //script pass through
            'profile_success' => $pSuccess,
            'security_success' => $sSuccess,
            'space_delete' => $spdUpdate,
            'service_delete' => $svdUpdate,
            'cancel_booking' => $cBooking,
		];
		return $this->view->render($response, '/dashboard/host-dashboard.twig', $data);
	}

	public function postHostDashboard($request, $response)
	{
		// Input hidden values here to determine which form is ready to update using AJAX
		if ($request->getParam('dashboard_hidden_input') === "profile") {
			$user = User::where('user_id', $_SESSION['user_id'])->first();
			$user_img = $user->user_image;
            $uploadedFiles = $request->getUploadedFiles();
            $uFileArr = array_column($uploadedFiles,'file');
                if($uFileArr[0] == ""){
                    $user->user_image = $user_img;
                }else {
                    if($user_img == "" ){
                        //handle single input with single file upload
                        $uploadedFile = $uploadedFiles['profile_photo'];
                        $targetDir = "Users/" . $user->user_name . "/profilePic/";
                        if (!file_exists($targetDir)) {
                            mkdir($targetDir, 0777, true);
                        }
                        $targetFile = $targetDir . basename($_FILES["profile_photo"]["name"]);
                        $check = getimagesize($_FILES["profile_photo"]["tmp_name"]);
                        $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
                        $uploadOK = 1;
                        // Check file size
                        if ($_FILES["profile_photo"]["size"] > 25000000) {
                            echo "Sorry, your file is too large.";
                            $uploadOK = 0;
                        }
                        //file format condition
                        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                            && $imageFileType != "gif" ) {
                            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                            $uploadOK = 0;
                        }
                        if ($check !== false && $uploadOK != 0) {
                            if ($c = move_uploaded_file($_FILES["profile_photo"]["tmp_name"], $targetFile)) {
                                //for database dir purpose
                                $user->user_image = "/Users/" . $user->user_name . "/profilePic/" . basename($_FILES["profile_photo"]["name"]);
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                                return;
                            }
                        } else {
                            echo "Your image is not uploaded.";
                            return;
                        }
                    }elseif($user_img != "" && $uploadedFiles != null){
                        //delete first the current dir on db and also the file
                        $user->user_image = "";
//                      $delURL = $request->getUri()->getBaseUrl().$user_img;
                        //handle single input with single file upload
                        $uploadedFile = $uploadedFiles['profile_photo'];
                        $targetDir = "Users/" . $user->user_name . "/profilePic/";
                        if (!file_exists($targetDir)) {
                            mkdir($targetDir, 0777, true);
                        }
                        $targetFile = $targetDir . basename($_FILES["profile_photo"]["name"]);
                        $check = getimagesize($_FILES["profile_photo"]["tmp_name"]);
                        $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
                        $uploadOK = 1;
                        // Check file size
                        if ($_FILES["profile_photo"]["size"] > 500000) {
                            echo "Sorry, your file is too large.";
                            $uploadOK = 0;
                        }
                        //file format condition
                        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                            && $imageFileType != "gif" ) {
                            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                            $uploadOK = 0;
                        }
                        if ($check !== false && $uploadOK != 0) {
                            if ($c = move_uploaded_file($_FILES["profile_photo"]["tmp_name"], $targetFile)) {
                                //for database dir purpose
                                $user->user_image = "/Users/" . $user->user_name . "/profilePic/" . basename($_FILES["profile_photo"]["name"]);
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                                return;
                            }
                        } else {
                            echo "Your image is not uploaded";
                            return;
                        }
                    }elseif($user_img != "" && $uploadedFiles == null){
                        $user->user_image = $user_img;
                    }else{

                    }
                }
            //plan to update email but since it's virtue input, we hold first
			$user->user_description = $request->getParam('about_me');
			if($user->save()){
               $_SESSION['profile_success'] = "true";
            }else{
               $_SESSION['profile_success'] = "false";
            }
		} elseif ($request->getParam('dashboard_hidden_input') === "password_security") {
            $user = User::where("user_id", $_SESSION["user_id"])->first();
            $dbPswd = $user->user_password;
            $cPswd = $request->getParam('current_password');
            $nPswd = $request->getParam('new_password');

            $pv = password_verify($cPswd,$dbPswd);
            if($pv == true){
                $user->user_password = (password_hash($nPswd, PASSWORD_DEFAULT, ['cost' => 12]));
                if ($user->save()) {
                    $_SESSION['security_success'] = "true";
                } else {
                    $_SESSION['security_success'] = "false";
                }
            }else{
                (!$user->save());
                $_SESSION['security_success'] = "unmatch";
            }
		} else {
			// last callback exception
		}
        return $response->withRedirect($request->getUri()->getBasePath().'/host-dashboard/'.$_SESSION['user_id']);
	}
	//End of Host Dashboard

    //Space Overview
    public function getSpaceOverview($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
        }

        //get the id from url on host dashboard
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $eID = $url_d[4];

        $space_data = Space::where('space_id', $eID)->get();
        $space_image = SpaceImage::where('space_id',$eID)->get();
        $space_availability = SpaceAvailability::where('space_id',$eID)->first();
        $auth = UserFunction::where('space_id','=',$eID)->where('user_id','=',$user_id)->first();

        if($space_availability == null && $space_data != ""){
            SpaceAvailability::create([
                'space_id' => $eID,
            ]);
        }

        if($auth != ""){
            $user_auth = "true";
        }else{
            $user_auth = "";
        }
        if(isset($_SESSION['overview_update'])){
            $oUpdate = $_SESSION['overview_update'];
            unset($_SESSION['overview_update']);
        }else{
            $oUpdate = "n.a.";
        };

        if(isset($_SESSION['amenities_update'])){
            $aUpdate = $_SESSION['amenities_update'];
            unset($_SESSION['amenities_update']);
        }else{
            $aUpdate = "n.a.";
        };

        if(isset($_SESSION['space_del'])){
            $dUpdate = $_SESSION['space_del'];
            unset($_SESSION['space_del']);
        }else{
            $dUpdate = "n.a.";
        };

        $data = [
            'title' => 'Space Overview',
            'space_id' => $eID,
            'user_id' => $user_id,
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_auth' => $user_auth,
            'space' => $space_data,
            'space_img' => $space_image,
            'space_availability' => $space_availability,

            //script pass through
            'oUpdate' => $oUpdate,
            'aUpdate' => $aUpdate,
            'dUpdate' => $dUpdate,
            ];

        return $this->view->render($response, '/spaces/space-overview.twig', $data);
    }

    public function postSpaceOverview($request, $response)
    {
        $validation = $this->validator->validate($request, [
            'space_name' => v::notEmpty()->alpha(),
            'space_description' =>v::notEmpty(),
            'space_guest' => v::notEmpty()->alnum(),
            'space_size' => v::notEmpty()->alnum(),
            'space_restroom' =>v::notEmpty()->alnum(),
            'space_pricing' => v::notEmpty()->alnum(),

            'username' => v::noWhitespace()->notEmpty()->length(5)->alnum()->usernameTaken(),
            'title_name' => v::notEmpty(),
            'full_name' => v::notEmpty()->alpha(),
            'email_address' => v::noWhitespace()->notEmpty()->email()->emailTaken(),
            'password' => v::noWhitespace()->notEmpty()->length(8)->alnum()->mightyPassword(),
            'confirm_password' => v::confirmPassword($request->getParam('password'))->noWhitespace()->notEmpty(),
            'user_type' => v::notEmpty(),
        ]);

        if ($validation->failed()) {
            //trigger the error
        }

        //get the id from url on host dashboard / not best solution
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $eID = $url_d[4];
        $space = Space::where('space_id', $eID)->first();
        $space_availability = SpaceAvailability::where('space_id',$eID)->first();

        if($request->getParam("spaceoverview_hidden_input") === "spaceoverview") {

            $space->space_name = $request->getParam('space_name');
            $space->space_address = $request->getParam('space_address');
            $space->space_description = $request->getParam('space_description');
            $space->space_category = ucwords($request->getParam('space_category'));
            $space->space_guest = $request->getParam('space_guest');
            $space->space_size = $request->getParam('space_size');
            $space->space_restroom = $request->getParam('space_restroom');
            $space->space_pricing = $request->getParam('space_pricing');
            if($request->getParam('space-price-v') == "Price Hide"){
                $space->space_pricing_visibility = 0;
            }else{
                $space->space_pricing_visibility = 1;
            }
            $space->space_type = $request->getParam('space_type');

//        monday logic begin
            if ($request->getParam('monday_close') == "Monday Closed") {
                $space_availability->mon_available = 0;
                $space_availability->mon_start = "00:00:00";
                $space_availability->mon_end = "00:00:00";
            } else {
                $space_availability->mon_available = 1;
                $space_availability->mon_start = $request->getParam('monday_from');
                $space_availability->mon_end = $request->getParam('monday_to');
            }
//        monday logic ends
//        tuesday logic begin
            if ($request->getParam('tuesday_close') == "Tuesday Closed") {
                $space_availability->tue_available = 0;
                $space_availability->tue_start = "00:00:00";
                $space_availability->tue_end = "00:00:00";
            } else {
                $space_availability->tue_available = 1;
                $space_availability->tue_start = $request->getParam('tuesday_from');
                $space_availability->tue_end = $request->getParam('tuesday_to');
            }
//        tuesday logic ends
//        wednesday logic begin
            if ($request->getParam('wednesday_close') == "Wednesday Closed") {
                $space_availability->wed_available = 0;
                $space_availability->wed_start = "00:00:00";
                $space_availability->wed_end = "00:00:00";
            } else {
                $space_availability->wed_available = 1;
                $space_availability->wed_start = $request->getParam('wednesday_from');
                $space_availability->wed_end = $request->getParam('wednesday_to');
            }
//        wednesday logic ends
//        thursday logic begin
            if ($request->getParam('thursday_close') == "Thursday Closed") {
                $space_availability->thu_available = 0;
                $space_availability->thu_start = "00:00:00";
                $space_availability->thu_end = "00:00:00";
            } else {
                $space_availability->thu_available = 1;
                $space_availability->thu_start = $request->getParam('thursday_from');
                $space_availability->thu_end = $request->getParam('thursday_to');
            }
//        thursday logic ends
//        friday logic begin
            if ($request->getParam('friday_close') == "Friday Closed") {
                $space_availability->fri_available = 0;
                $space_availability->fri_start = "00:00:00";
                $space_availability->fri_end = "00:00:00";
            } else{
                $space_availability->fri_available = 1;
                $space_availability->fri_start = $request->getParam('friday_from');
                $space_availability->fri_end = $request->getParam('friday_to');
            }
//        friday logic ends
//        saturday logic begin
            if ($request->getParam('saturday_close') == "Saturday Closed") {
                $space_availability->sat_available = 0;
                $space_availability->sat_start = "00:00:00";
                $space_availability->sat_end = "00:00:00";
            } else {
                $space_availability->sat_available = 1;
                $space_availability->sat_start = $request->getParam('saturday_from');
                $space_availability->sat_end = $request->getParam('saturday_to');
            }
//        saturday logic ends
//        sunday logic begin
            if ($request->getParam('sunday_close') == "Sunday Closed") {
                $space_availability->sun_available = 0;
                $space_availability->sun_start = "00:00:00";
                $space_availability->sun_end = "00:00:00";
            } else {
                $space_availability->sun_available = 1;
                $space_availability->sun_start = $request->getParam('sunday_from');
                $space_availability->sun_end = $request->getParam('sunday_to');
            }
//        sunday logic ends

            $space->space_guest = $request->getParam('space_guest');

            //space image starts here
            $user = User::where('user_id', $_SESSION["user_id"])->first();
            $spaceImage = SpaceImage::where('space_id',$eID)->get();
            $spaceID = $eID;
            $upfile = $request->getUploadedFiles();
            if($upfile != "") {
                //img1 starts here
                if ($_FILES["space_photo1"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo1"]["name"]);
                    $check = getimagesize($_FILES["space_photo1"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo1"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo1"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo1"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 1)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo1"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 1;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo1"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr1') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',1)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }

                //img2 starts here
                if ($_FILES["space_photo2"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo2"]["name"]);
                    $check = getimagesize($_FILES["space_photo2"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo2"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo2"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo2"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 2)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo2"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 2;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo2"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr2') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',2)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }

                //img3 starts here
                if ($_FILES["space_photo3"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo3"]["name"]);
                    $check = getimagesize($_FILES["space_photo3"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo3"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo3"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo3"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 3)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo3"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 3;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo3"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr3') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',3)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }

                //img4 starts here
                if ($_FILES["space_photo4"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo4"]["name"]);
                    $check = getimagesize($_FILES["space_photo4"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo4"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo4"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo4"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 4)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo4"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 4;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo4"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr4') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',4)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }

                //img5 starts here
                if ($_FILES["space_photo5"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo5"]["name"]);
                    $check = getimagesize($_FILES["space_photo5"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo5"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo5"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo5"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 5)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo5"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 5;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo5"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr5') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',5)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }
                //img6 starts here
                if ($_FILES["space_photo6"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo6"]["name"]);
                    $check = getimagesize($_FILES["space_photo6"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo6"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo6"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo6"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 6)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo6"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 6;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo6"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr6') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',6)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }

                //img7 starts here
                if ($_FILES["space_photo7"]["tmp_name"]) {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo7"]["name"]);
                    $check = getimagesize($_FILES["space_photo7"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo7"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo7"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo7"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 7)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo7"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 7;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo7"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr7') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',7)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }
                //img 8 starts here
                if ($_FILES["space_photo8"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/" . basename($_FILES["space_photo8"]["name"]);
                    $check = getimagesize($_FILES["space_photo8"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size *the size of file that declared is on byte
                    if ($_FILES["space_photo8"]["size"] > 10000000) {
                        echo "Sorry, your picture is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false) {
                        if ($uploadOK != 0) {
                            $upDir = "Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo8"]["name"]);
                            $c = chmod($upDir, 0755);
                            boolval($c);
                            $b = move_uploaded_file($_FILES["space_photo8"]["tmp_name"], $upDir);
                            if ($b == true) {
                                //for database dir purpose
                                $si = SpaceImage::where('space_id', $eID)->where('space_index', 8)->first();
                                if ($si != null) {
                                    unlink($si->space_image_path);
                                    $si->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo8"]["name"]);
                                    $si->save();
                                } else {
                                    $spaceImg = new SpaceImage();
                                    $spaceImg->user_id = $_SESSION["user_id"];
                                    $spaceImg->space_id = $eID;
                                    $spaceImg->space_index = 8;
                                    $spaceImg->space_image_path = "/Users/" . $user->user_name . "/eventPic/" . $spaceID . "/" . basename($_FILES["space_photo8"]["name"]);
                                    $spaceImg->save();
                                }
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        } else {
                            echo "Your image is not uploaded.";
                        }
                    } else {

                    }
                }else{
                    if($request->getParam('img-attr8') == 0 ){
                        $imgAttr = SpaceImage::where('space_id',$eID)->where('space_index',8)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_image_path);
                            $imgAttr->delete();
                        }
                    }
                }

                //space cover photo starts here
                if ($_FILES["space_cover_photo"]["tmp_name"] != "") {
                    $targetDir = "Users/" . $user->user_name . "/eventPic/" . $eID;
                    if (!file_exists($targetDir)) {
                        mkdir($targetDir, 0777, true);
                    }
                    $targetFile = $targetDir . "/C" . basename($_FILES["space_cover_photo"]["name"]);
                    $check = getimagesize($_FILES["space_cover_photo"]["tmp_name"]);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $uploadOK = 1;
                    // Check file size
                    if ($_FILES["space_cover_photo"]["size"] > 5000000) {
                        echo "Sorry, your file is too large.";
                        $uploadOK = 0;
                    }
                    //file format condition
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOK = 0;
                    }
                    if ($check != false && $uploadOK != 0) {
                        if ($c = move_uploaded_file($_FILES["space_cover_photo"]["tmp_name"], $targetFile)) {
                            //for database dir purpose
                            $space = Space::where('space_id', $eID)->first();
                            if ($space->space_main_image != "") {
                                unlink($space->space_main_image);
                                $space->space_main_image = "/" . $targetFile;
                                $space->save();
                            } else {
                                $space->space_main_image = "/" . $targetFile;
                                $space->save();
                            }
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }else{
                    if($request->getParam('img-attrC') == 0 ){
                        $imgAttr = Space::where('space_id', $eID)->first();
                        if($imgAttr != null){
                            unlink($imgAttr->space_main_image);
                            $imgAttr->space_main_image = '';
                            $imgAttr->save();
                        }
                    }
                }
            }

            if ($space->save() && $space_availability->save()) {
                $_SESSION["overview_update"] = "true";
            } else {
                $_SESSION["overview_update"] = "false";
            }
            return $response->withRedirect($request->getUri()->getBasePath().'/space-overview/'.$spaceID);

        }elseif ($request->getParam('spaceamenities_hidden_input') === "amenities") {
            $spaceA = Space::where('space_id', $eID)->first();
            if (!empty($request->getParam('space_amenities'))) {
                $amenities_list = implode(',', $request->getParam('space_amenities'));
                $amenities = json_encode($amenities_list);
                $spaceA->space_amenities = $amenities;
            }
            if (!empty($request->getParam('space_rules'))) {
                $rules_list = implode(',', $request->getParam('space_rules'));
                $rules = json_encode($rules_list);
                $spaceA->space_rule = $rules;
            }
            if (!empty($request->getParam('space_catering'))) {
                $catering_list = implode(',', $request->getParam('space_catering'));
                $catering = json_encode($catering_list);
                $spaceA->space_catering = $catering;
            }
                $spaceA->space_access = $request->getParam('space_access');
                $spaceA->space_cancellation = $request->getParam('cancel_type');

                if($spaceA->save()){
                    $_SESSION["amenities_update"] = "true";
                }else {
                    $_SESSION["amenities_update"] = "false";
                }
            return $response->withRedirect($request->getUri()->getBasePath().'/space-overview/'.$eID);

        }elseif ($request->getParam('space_hidden_input') === "deletespace") {
           $uPswd =  $request->getParam('password');
           $user = User::where('user_id',$_SESSION["user_id"])->first();
            $pv = password_verify($uPswd,$user->user_password);
            if($pv == true) {
                UserFunction::where('space_id',$eID)->delete();
                Space::where('space_id',$eID)->delete();
                SpaceAvailability::where('space_id',$eID)->delete();
                SpaceImage::where('space_id',$eID)->delete();
                $_SESSION["space_del"] = "true";
                return $response->withRedirect($request->getUri()->getBasePath().'/host-dashboard/'.$_SESSION['user_id']);
            }else{
                $_SESSION["space_del"] = "false";
                return $response->withRedirect($request->getUri()->getBasePath().'/space-overview/'.$eID);
            }
        }
    }
    //End of Space Overview

    //Service Overview
    public function getServiceOverview($request, $response)
    {
        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
            $user_full_name = $_SESSION['user_full_name'];
            $user_avatar = $_SESSION['user_avatar'];
        } else {
            $user_id = "";
            $user_full_name = "";
            $user_avatar = "";
        }

        //get the id from url on host dashboard
        $url = $request->getUri();
        $url_d = explode("/", $url);
        $sID = $url_d[4]; //might be consider session sols
        $_SESSION['sID_session'] = $sID;

        $auth = UserFunction::where('service_id','=',$sID)->where('user_id','=',$user_id)->first();
        $sv = Service::where('service_id',$_SESSION['sID_session'])->first();
        $spBook = SpaceBooking::where('user_id',$user_id)->leftJoin('spaces','spaces.space_id','=','space_booking.space_id')->get();


        $catMenu = "";
        $service = "";

        if($sv->service_catering != ""){
            $service = Service::where('services.service_id',$sID)->rightJoin('service_catering','services.service_catering','=','service_catering.catering_id')->groupBy('services.service_id')->get();
            $catMenu = CateringMenu::where('catering_id',$sv->service_catering)->get();
        }elseif($sv->service_detail != ""){
            $service = Service::where('services.service_id','=',$sID)->rightJoin('service_detail','services.service_detail','=','service_detail.detail_id')->groupBy('services.service_id')->get();
            $catMenu = NULL;
        }

        if(isset($_SESSION['service_update'])){
            $sUpdate = $_SESSION['service_update'];
            unset($_SESSION['service_update']);
        }else{
            $sUpdate = "n.a.";
        };

        if(isset($_SESSION['service_del'])){
            $dUpdate = $_SESSION['service_del'];
            unset($_SESSION['service_del']);
        }else{
            $dUpdate = "n.a.";
        };

        if(isset($_SESSION['servicemenu_del'])){
            $mUpdate = $_SESSION['servicemenu_del'];
            unset($_SESSION['servicemenu_del']);
        }else{
            $mUpdate = "n.a.";
        };

        if($auth != ""){
            $user_auth = "true";
        }else{
            $user_auth = "";
        }

        $data = [
            'title' => 'Service Overview',
            'service_id' => $sID,
            'user_id' => $_SESSION['user_id'],
            'user_full_name' => $user_full_name,
            'user_avatar' => $user_avatar,
            'user_auth' => $user_auth,
            'services' => $service,
            'catering_menu' => $catMenu,
            'space_booking' => $spBook,
            'sUpdate' => $sUpdate,
            'dUpdate' => $dUpdate,
            'mUpdate' => $mUpdate,
        ];

        return $this->view->render($response, '/services/service-overview.twig', $data);
    }

    public function postServiceOverview(Request $request, Response $response)
    {
        if ($request->getParam('catering_hidden_input') === "catering") {
            $srv = Service::where('service_id', $_SESSION['sID_session'])->first();
            $srvCat = ServiceCatering::where('catering_id', $srv->service_catering)->first();
            $user = User::where('user_id', $_SESSION["user_id"])->first();

            $srv->service_name = $request->getParam('service_name');
            $srvCat->catering_description = $request->getParam('service_description');
            $srvCat->catering_type = $request->getParam('catering_type');
            $srvCat->catering_halal = $request->getParam('catering_halal');

            //menu_img_cover
            if ($_FILES["detail_cover_photo"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/menuPic/" .$_SESSION['sID_session'];
                $targetFile = $targetDir . "/C" . basename($_FILES["detail_cover_photo"]["name"]);
                $check = getimagesize($_FILES["detail_cover_photo"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["detail_cover_photo"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/servicePic/" . "/C" . basename($_FILES["detail_cover_photo"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["detail_cover_photo"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $srv->service_main_image;
                            unlink($df);
                            //for database dir purpose
                            $srv->service_main_image = "Users/" . $user->user_name . "/servicePic/C" . basename($_FILES["detail_cover_photo"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            }else{
                if($request->getParam('img-attrC') == 0 ){
                    $serv = Service::where('service_id',$_SESSION['sID_session'])->first();
                    unlink($serv->service_main_image);
                    $serv->service_main_image = "";
                    $serv->save();
                }
            }

            if ($srvCat->save() && $srv->save()) {
                $_SESSION['service_update'] = 'true';
                return $response->withRedirect($request->getUri()->getBasePath() . '/service-overview/' . $_SESSION['sID_session']);
            } else {
                $_SESSION['service_update'] = 'false';
            }

        }elseif ($request->getParam('editMenu_hidden_input') === "editMenu"){

            $user = User::where('user_id', $_SESSION["user_id"])->first();
            $mID = $request->getParam('menu_id');
            $catMenu = CateringMenu::where('menu_id', $mID)->first();
            $catMenu->menu_name = $request->getParam('menu_name');
            $catMenu->menu_price = $request->getParam('menu_price');
            $catMenu->menu_description = $request->getParam('menu_description');
            //img starts here
            $menu_img_1 = $catMenu->menu_image_1;
            $menu_img_2 = $catMenu->menu_image_2;
            $menu_img_3 = $catMenu->menu_image_3;
            $menu_img_4 = $catMenu->menu_image_4;

            //menu_img_1
            $request->getUploadedFiles();

            if ($_FILES["menu_photo_1"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/menuPic/" . $mID;
                $targetFile = $targetDir . "/" . basename($_FILES["menu_photo_1"]["name"]);
                $check = getimagesize($_FILES["menu_photo_1"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                if (!file_exists($targetDir)) {
                    mkdir($targetDir, 0777, true);
                }
                // Check file size *the size of file that declared is on byte
                if ($_FILES["menu_photo_1"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_1"]["name"]);
                        chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["menu_photo_1"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $menu_img_1;
                            unlink($df);
                            //for database dir purpose
                            $catMenu->menu_image_1 = "Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_1"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            } else {
                if ($request->getParam('img-attr1e') == 0) {
                    unlink($catMenu->menu_image_1);
                    $catMenu->menu_image_1 = "";
                }
            }

            //menu_img_2
            if ($_FILES["menu_photo_2"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/menuPic/" . $mID;
                $targetFile = $targetDir . "/" . basename($_FILES["menu_photo_2"]["name"]);
                $check = getimagesize($_FILES["menu_photo_2"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                if (!file_exists($targetDir)) {
                    mkdir($targetDir, 0777, true);
                }
                // Check file size *the size of file that declared is on byte
                if ($_FILES["menu_photo_2"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_2"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["menu_photo_2"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $menu_img_2;
                            unlink($df);
                            //for database dir purpose
                            $catMenu->menu_image_2 = "Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_2"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            } else {
                if ($request->getParam('img-attr2e') == 0) {
                    unlink($catMenu->menu_image_2);
                    $catMenu->menu_image_2 = "";
                }
            }

            //menu_img_3
            if ($_FILES["menu_photo_3"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/menuPic/" . $mID;
                $targetFile = $targetDir . "/" . basename($_FILES["menu_photo_3"]["name"]);
                $check = getimagesize($_FILES["menu_photo_3"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                if (!file_exists($targetDir)) {
                    mkdir($targetDir, 0777, true);
                }
                // Check file size *the size of file that declared is on byte
                if ($_FILES["menu_photo_3"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_3"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["menu_photo_3"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $menu_img_3;
                            unlink($df);
                            //for database dir purpose
                            $catMenu->menu_image_3 = "Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_3"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            } else {
                if ($request->getParam('img-attr3e') == 0) {
                    unlink($catMenu->menu_image_3);
                    $catMenu->menu_image_3 = "";
                }
            }

            //menu_img_4
            if ($_FILES["menu_photo_4"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/menuPic/" . $mID;
                $targetFile = $targetDir . "/" . basename($_FILES["menu_photo_4"]["name"]);
                $check = getimagesize($_FILES["menu_photo_4"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                if (!file_exists($targetDir)) {
                    mkdir($targetDir, 0777, true);
                }
                // Check file size *the size of file that declared is on byte
                if ($_FILES["menu_photo_4"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_4"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["menu_photo_4"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $menu_img_4;
                            unlink($df);
                            //for database dir purpose
                            $catMenu->menu_image_4 = "/Users/" . $user->user_name . "/menuPic/" . $mID . "/" . basename($_FILES["menu_photo_4"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            } else {
                if ($request->getParam('img-attr4e') == 0) {
                    unlink($catMenu->menu_image_4);
                    $catMenu->menu_image_4 = "";
                }
            }


            if ($catMenu->save()) {
                $_SESSION['service_update'] = 'true';
                return $response->withRedirect($request->getUri()->getBasePath() . '/service-overview/' . $_SESSION['sID_session']);
            } else {
                $_SESSION['service_update'] = 'false';
            }

        }elseif ($request->getParam('newmenu_hidden_input') === "newmenulist"){
            $svc = Service::where('service_id',$_SESSION['sID_session'])->first();
            $menuID = "M".$svc->service_catering.date('mdYHis');
            CateringMenu::create([
                'menu_id' => $menuID,
                'catering_id' => $svc->service_catering,
                'menu_name' => ucwords($request->getParam('menu_name')),
                'menu_price' => $request->getParam('menu_price'),
                'menu_description' => $request->getParam('menu_description'),
            ]);
            $user = User::where('user_id', $_SESSION["user_id"])->first();
            $uploadedImage = $request->getUploadedFiles();
            $uFcount = count($uploadedImage);
            $feV = "Users/" . $user->user_name . "/menuPic/";
            if (!file_exists($feV)) {
                mkdir($feV, 0777, true);
            }
            for ($xC = 0; $xC < $uFcount; $xC++) {
                $cfp = "Users/" . $user->user_name . "/menuPic/".$menuID;
                if (!file_exists($cfp)) {
                    mkdir($cfp, 0777, true);
                }
                $targetDir = "Users/" . $user->user_name . "/menuPic/".$menuID;
                $targetFile = $targetDir . "/" . basename($_FILES["menu_photo_".($xC+1)]["name"]);
                $check = getimagesize($_FILES["menu_photo_".($xC+1)]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["menu_photo_".($xC+1)]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                        $b = move_uploaded_file($_FILES["menu_photo_".($xC+1)]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //for database dir purpose
                            $menu = CateringMenu::where('menu_id',$menuID)->first();
                            $ac = $xC+1;
                            if($ac == 1){
                                $menu->menu_image_1 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }elseif ($ac == 2){
                                $menu->menu_image_2 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }elseif ($ac == 3){
                                $menu->menu_image_3 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }elseif($ac == 4){
                                $menu->menu_image_4 = "/Users/" . $user->user_name . "/menuPic/".$menuID."/".basename($_FILES["menu_photo_".($xC+1)]["name"]);
                            }
                            $menu->save();
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                } else {

                }
            }
            return $response->withRedirect($request->getUri()->getBasePath().'/service-overview/'.$_SESSION['sID_session']);

        }elseif ($request->getParam('servicedetailoverview_hidden_input') === "servicedetailoverview") {

            $user = User::where('user_id', $_SESSION["user_id"])->first();
            $svc = Service::where('service_id', $_SESSION['sID_session'])->first();
            $svcDetail = ServiceDetail::where('detail_id', $svc->service_detail)->first();

            $svc->service_name = $request->getParam('service_name');
            $svcDetail->detail_description = $request->getParam('service_description');
            $svcDetail->detail_pricing = $request->getParam('detail_pricing');

            //img starts here
            $detail_img_1 = $svcDetail->detail_image_1;
            $detail_img_2 = $svcDetail->detail_image_2;
            $detail_img_3 = $svcDetail->detail_image_3;
            $detail_img_4 = $svcDetail->detail_image_4;


            $request->getUploadedFiles();
            //menu_img_1
            if ($_FILES["detail_photo_1"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id;
                $targetFile = $targetDir . "/" . basename($_FILES["detail_photo_1"]["name"]);
                $check = getimagesize($_FILES["detail_photo_1"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["detail_photo_1"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_1"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["detail_photo_1"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $detail_img_1;
                            unlink($df);
                            //for database dir purpose
                            $svcDetail->detail_image_1 = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_1"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            }else{
                if($request->getParam('img-attr1') == 0 ){
                    unlink($svcDetail->detail_image_1);
                    $svcDetail->detail_image_1 = "";
                }
            }

            //menu_img_2
            if ($_FILES["detail_photo_2"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id;
                $targetFile = $targetDir . "/" . basename($_FILES["detail_photo_2"]["name"]);
                $check = getimagesize($_FILES["detail_photo_2"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["detail_photo_2"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_2"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["detail_photo_2"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $detail_img_2;
                            unlink($df);
                            //for database dir purpose
                            $svcDetail->detail_image_2 = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_2"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            }else{
                if($request->getParam('img-attr2') == 0 ){
                    unlink($svcDetail->detail_image_2);
                    $svcDetail->detail_image_2 = "";
                }
            }

            //menu_img_3
            if ($_FILES["detail_photo_3"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id;
                $targetFile = $targetDir . "/" . basename($_FILES["detail_photo_3"]["name"]);
                $check = getimagesize($_FILES["detail_photo_3"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["detail_photo_3"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_3"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["detail_photo_3"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $detail_img_3;
                            unlink($df);
                            //for database dir purpose
                            $svcDetail->detail_image_3 = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_3"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            }else{
                if($request->getParam('img-attr3') == 0 ){
                    unlink($svcDetail->detail_image_3);
                    $svcDetail->detail_image_3 = "";
                }
            }

            //menu_img_4
            if ($_FILES["detail_photo_4"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id;
                $targetFile = $targetDir . "/" . basename($_FILES["detail_photo_4"]["name"]);
                $check = getimagesize($_FILES["detail_photo_4"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["detail_photo_4"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_4"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["detail_photo_4"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $detail_img_4;
                            unlink($df);
                            //for database dir purpose
                            $svcDetail->detail_image_4 = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/" . basename($_FILES["detail_photo_4"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            }else{
                if($request->getParam('img-attr4') == 0 ){
                    unlink($svcDetail->detail_image_4);
                    $svcDetail->detail_image_4 = "";
                }
            }

            //menu_img_cover
            if ($_FILES["detail_cover_photo"]["tmp_name"] != "") {
                $targetDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id;
                $targetFile = $targetDir . "/C" . basename($_FILES["detail_cover_photo"]["name"]);
                $check = getimagesize($_FILES["detail_cover_photo"]["tmp_name"]); //some img cannot and have to further investigate
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $uploadOK = 1;
                // Check file size *the size of file that declared is on byte
                if ($_FILES["detail_cover_photo"]["size"] > 10000000) {
                    echo "Sorry, your picture is too large.";
                    $uploadOK = 0;
                }
                //Check file exists
                if (file_exists($targetFile)) {
                    echo "Sorry, file already exists.";
                    $uploadOK = 0;
                }
                //file format condition
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOK = 0;
                }
                if ($check != false) {
                    if ($uploadOK != 0) {
                        $upDir = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/C" . basename($_FILES["detail_cover_photo"]["name"]);
                        $a = chmod($upDir, 0777);
                        $b = move_uploaded_file($_FILES["detail_cover_photo"]["tmp_name"], $upDir);
                        if ($b == true) { //if this return false, check dir whether the folder exists
                            //the old img file has to be deleted
                            $df = $svc->service_main_image;
                            unlink($df);
                            //for database dir purpose
                            $svc->service_main_image = "Users/" . $user->user_name . "/detailPic/" . $svcDetail->detail_id . "/C" . basename($_FILES["detail_cover_photo"]["name"]);
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    } else {
                        echo "Your image is not uploaded.";
                    }
                }
            }else{
                if($request->getParam('img-attrC') == 0 ){
                    $serv = Service::where('service_id',$_SESSION['sID_session'])->first();
                    unlink($serv->service_main_image);
                    $serv->service_main_image = "";
                    $serv->save();
                }
            }

            if ($svcDetail->save() && $svc->save()) {
                $_SESSION['service_update'] = "true";
                return $response->withRedirect($request->getUri()->getBasePath() . '/service-overview/' . $_SESSION['sID_session']);
            } else {
                $_SESSION['service_update'] = "false";
                return $response->withRedirect($request->getUri()->getBasePath() . '/service-overview/' . $_SESSION['sID_session']);
            }
        }elseif ($request->getParam('service_hidden_input') === "deleteservice") {
            $uPswd =  $request->getParam('password');
            $user = User::where('user_id',$_SESSION["user_id"])->first();
            $pv = password_verify($uPswd,$user->user_password);
            if($pv == true) {
                UserFunction::where('service_id',$_SESSION['sID_session'])->delete();
                Service::where('service_id',$_SESSION['sID_session'])->delete();
                $sd = ServiceDetail::where('service_id',$_SESSION['sID_session'])->get();
                if($sd != null){
                    ServiceDetail::where('service_id',$_SESSION['sID_session'])->delete();
                }
                $sc = ServiceCatering::where('service_id',$_SESSION['sID_session'])->get();
                if($sc != null){
                    foreach ($sc as $l){
                        CateringMenu::where('catering_id',$l->catering_id)->delete();
                    }
                    ServiceCatering::where('service_id',$_SESSION['sID_session'])->delete();
                }
                $_SESSION["service_del"] = "true";
                return $response->withRedirect($request->getUri()->getBasePath().'/host-dashboard/'.$_SESSION['user_id']);
            }else{
                $_SESSION["service_del"] = "false";
                return $response->withRedirect($request->getUri()->getBasePath().'/space-overview/'.$_SESSION['sID_session']);
            }
        }elseif ($request->getParam('servicemenu_hidden_input') === "deletemenu") {
            $uPswd =  $request->getParam('password');
            $user = User::where('user_id',$_SESSION["user_id"])->first();
            $pv = password_verify($uPswd,$user->user_password);
            if($pv == true) {
                $menuID = $request->getParam('menu_id');
                CateringMenu::where('menu_id',$menuID)->delete();
                unlink('Users/'.$user->user_name.'/menuPic/'.$menuID);

                $_SESSION["servicemenu_del"] = "true";
                return $response->withRedirect($request->getUri()->getBasePath().'/service-overview/'.$_SESSION['sID_session']);
            }else{
                $_SESSION["servicemenu_del"] = "false";
                return $response->withRedirect($request->getUri()->getBasePath().'/service-overview/'.$_SESSION['sID_session']);
            }
        }
    }
    //End of Service Overview

    // Messages

   public function time_elapsed_string($ptime)
   {
     $etime = time() - $ptime;

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
} 

    public function getUserChats($array)
    {
        $message_html = "";
        foreach ($array as $m)
        {
             $r_data = Message::where('user_id', $m->receiver_id);

            $message_html .= '<div class="inbox-profile">
                                        <a data-toggle="tooltip" data-placement="left" title="View Profile" href="#"><img class="img-responsive inbox-profile-img" src="{{base_url()}}/img/'.$r_data->user_name.'.png" alt="Profile Photo"/></a>
                                        <p class="green-text profile-name"><b>'.$r_data->user_name.'</b></p>
                                        <p class="text-right chat-time">'.$this->time_elapsed_string($m->timestamp).'</p>
                                        <p class="chat-content">'.$m->message.'<a class="green-text start_chat" href="#" data-toggle="modal" data-target="#chat-modal" m-id="'.$r_data->user_name.'">Message Now</a></p>
                                        <hr>
                                    </div>';
        }
        
        return $message_html;
    }

    public function getUserMessages($request, $response)
    {
        $message_html = "";
        $reciver = $request->getParam('member');
        $r_id = Message::where('user_name', $reciver);
        $messages = Message::where('user_id', $_SESSION['user_id'])->orWhere('receiver_id', $r_id->user_id);
        

        if (empty($reciver) || empty($r_id))
        {
          return;
        }

        foreach ($messages as $m)
        {
            if ($messages->user_id == $_SESSION['user_id'])
            {
              $message_html .= '<div class="other" style="color:#e8fffd;">
                                 <p><b>You</b> <span class="pull-right">'.$this->time_elapsed_string($m->timestamp).'</span></p>
                                    <p class="chat-message">'.$m->message.'</p>
                               </div>
                               <hr>';
            }
            else
            {
               $message_html .= '<div class="other">
                                 <p><b>'.$m->name.'</b> <span class="pull-right">'.$this->time_elapsed_string($m->timestamp).'</span></p>
                                    <p class="chat-message">'.$m->message.'</p>
                               </div>
                               <hr>'; 
            }
            
        }
        
        return $this->view->render($response, $message_html);
    }

    public function PostUserMessage($request, $response)
    {
        $reciver = $request->getParam('member');
        $message = $request->getParam('message');

        if (strlen($message) > 70 || $message == null || $message == "")
        {
          return;
        }

        $r_id = User::where('user_name', $reciver);
        

        if (empty($r_id))
        {
         return $this->container->flash->addMessage('error', "Unable to send message.");
        }

        $send_message = Message::create([
            'user_id' => $_SESSION['user_id'],
            'name' => $r_id->user_full_name,
            'message' => $message,
            'receiver_id' => $r_id->user_id,
            'seen' => 0
        ]);

         if (empty($send_message))
        {
         return $this->container->flash->addMessage('error', "Unable to send message.");
        }
        
        return;
    }

    public function PostMessageSeen($request, $response)
    {
        $reciver = $request->getParam('member');
         
          $r_id = User::where('user_name', $reciver);
        $chat_exists = Message::where('user_id', $_SESSION['user_id'])->where('reciver_id', $r_id->user_id);
        

        if (empty($chat_exists))
        {
         return;
        }

        $send_message = Message::where('user_id', $_SESSION['user_id'])->where('receiver_id', $r_id->reciver_id)->update([
            'seen' => 1
        ]);
        
        return;
    }

    // End of get messages
    
}
