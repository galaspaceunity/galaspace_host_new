<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBooking extends Model
{
    public $table="user_bookings";
    public $incrementing = false;
    protected $primaryKey = "book_id";
    protected $fillable = [
        'book_id',
        'book_space',
        'book_service',
        'user_id',
        'book_status',
    ];

}
