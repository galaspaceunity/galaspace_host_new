<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceBooking extends Model
{
    public $table = "service_booking";
    public $incrementing = false;
    protected $primaryKey = "svbook_id";
    protected $fillable = [
        'svbook_id',
        'book_id',
        'service_id',
        'space_id',
        'user_id',
        'svbook_date',
        'svbook_description',
        'svbook_time_from',
        'svbook_time_to',
        'svbook_status',
        'svbook_pax',
        'svbook_pricing',
        'service_by',
    ];

}
