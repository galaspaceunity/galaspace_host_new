<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFunction extends Model
{

    public $table = "user_function";
    protected $fillable = [
        'id',
        'user_id',
        'space_id',
        'service_id'
    ];

}
