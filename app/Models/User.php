<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $table="users";
    protected $primaryKey = "user_id";
	protected $fillable = [
		'user_id',
		'user_name',
        'user_title',
		'user_full_name',
		'user_email',
		'user_password',
		'user_country_code',
		'user_contact_number',
		'user_type',
		'user_profile_photo',
		'user_confirm',
        'user_status',
        'user_token',
        'company_id'
	];

}
