<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CateringBooking extends Model
{
    public $table="catering_booking";
    protected $primaryKey = "catBk_id";
    protected $fillable = [
        'svbook_id',
        'menu_id',
        'catering_id',
        'catBk_pax',
    ];

}