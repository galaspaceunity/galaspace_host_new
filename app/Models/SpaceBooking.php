<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpaceBooking extends Model
{
    public $table = "space_booking";
    public $incrementing = false;
    protected $primaryKey = "spbook_id";
    protected $fillable = [
        'spbook_id',
        'space_id',
        'book_id',
        'user_id',
        'spbook_event_type',
        'spbook_date',
        'spbook_time_from',
        'spbook_time_to',
        'spbook_guest',
        'spbook_desc_msg',
        'spbook_vendor_msg',
        'spbook_status',
    ];

    public function getSpaceBookingDate($id,$dt){
        $model = SpaceBooking::where('space_id',$id)->get();
        foreach($model as $loop_dt){
            $a = $loop_dt->spbook_date;
            $fdt = date("d-m-Y", strtotime($a));
            if($dt == $fdt){
                return true;
            }
        }
    }

    public function getSpaceBookingTimeFrom($id){
        $model = SpaceBooking::where('space_id',$id)->first();
        $dt = @$model->spbook_time_from;
        return $dt;
    }

    public function getSpaceBookingTimeTo($id){
        $model = SpaceBooking::where('space_id',$id)->first();
        $dt = @$model->spbook_time_to;
        return $dt;
    }
}
