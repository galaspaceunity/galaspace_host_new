<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    public $table="spaces";
    public $incrementing = false;
    protected $primaryKey = "space_id";
	protected $fillable = [
		'space_id',
        'space_name',
        'space_main_image',
        'space_description',
        'space_known',
        'space_type',
        'space_category',
        'space_approval',
        'space_guest',
        'space_size',
        'space_restroom',
        'space_access',
        'space_place_id',
        'space_address',
        'space_floor',
        'space_latitude',
        'space_longitude',
        'map_location',
        'space_area',
        'space_amenities',
        'space_availability',
        'space_rule',
        'space_catering',
        'space_cancellation',
        'space_currency',
        'space_pricing',
        'space_pricing_visibility',
        'space_min_usage',
        'space_full_rate',
        'space_add_rate',
        'space_status',
        'space_by'
	];

    protected $casts = [
        'space_amenities' => 'array',
        'space_rule' => 'array'
    ];

}
