<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpaceAvailability extends Model
{
	public $table = "space_availability";
    protected $primaryKey = "id";
	protected $fillable = [
	    'id',
        'space_id',
        'mon_available',
        'mon_start',
        'mon_end',
        'tue_available',
        'tue_start',
        'tue_end',
        'wed_available',
        'wed_start',
        'wed_end',
        'thu_available',
        'thu_start',
        'thu_end',
        'fri_available',
        'fri_start',
        'fri_end',
        'sat_available',
        'sat_start',
        'sat_end',
        'sun_available',
        'sun_start',
        'sun_end'
	];

}
