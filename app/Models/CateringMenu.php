<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CateringMenu extends Model
{
    public $table="catering_menu";
    protected $primaryKey = "id";
    protected $fillable = [
        'id',
        'menu_id',
        'catering_id',
        'menu_name',
        'menu_description',
        'menu_price',
        'menu_image_1',
        'menu_image_2',
        'menu_image_3',
        'menu_image_4'
    ];

}