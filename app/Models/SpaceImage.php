<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpaceImage extends Model
{
    public $table="space_image";
    protected $primaryKey = "id";
    protected $fillable = [
        'id',
        'user_id',
        'space_id',
        'space_index',
        'space_image_name',
        'space_image_description',
        'space_image_path'
    ];

}
