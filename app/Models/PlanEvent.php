<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanEvent extends Model
{
    public $table = "plan_event";
    protected $primaryKey = "event_id";
    protected $fillable = [
        'event_name',
        'event_email',
        'event_contact',
        'event_plan',
        'event_budget',
        'event_location',
        'event_dt',
        'event_guest',
        'event_services',
        'event_request',
    ];

}
