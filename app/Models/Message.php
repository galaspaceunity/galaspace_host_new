<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $table="messages";
    protected $primaryKey = "message_id";
	protected $fillable = [
		'user_id',
		'name',
        'message',
		'receiver_id',
		'timestamp',
		'seen'
	];

}
