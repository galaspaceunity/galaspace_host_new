<?php
/**
 * Created by IntelliJ IDEA.
 * User: Aiman Bin Idris
 * Date: 25/5/2018
 * Time: 10:56 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDetail extends Model
{
    public $table="service_detail";
    protected $primaryKey = "id";
    protected $fillable = [
        'id',
        'detail_id',
        'service_id',
        'detail_description',
        'detail_pricing',
        'detail_pricing_type',
        'detail_image_1',
        'detail_image_2',
        'detail_image_3',
        'detail_image_4'
    ];

}