<?php
/**
 * Created by IntelliJ IDEA.
 * User: Aiman Bin Idris
 * Date: 16/7/2018
 * Time: 9:19 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $table="contacts";
    protected $primaryKey = "contact_id";
    protected $fillable = [
        'contact_id',
        'contact_full_name',
        'contact_email',
        'contact_message'
    ];

}