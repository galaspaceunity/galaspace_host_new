<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    public $table = "user_company";
    protected $primaryKey = "company_id";
    protected $fillable = [
        'company_id',
        'company_name',
        'company_description',
        'company_location',
        'company_radius'
    ];
}

