<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $table = "services";
    public $incrementing = false;
    protected $primaryKey = "service_id";
    protected $fillable = [
        'service_id',
        'service_name',
        'service_type',
        'service_catering',
        'service_detail',
        'service_main_image',
        'service_currency',
        'service_pricing',
        'service_location',
        'service_status',
        'service_by'
    ];

}
