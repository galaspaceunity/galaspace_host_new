<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 26/6/2018
 * Time: 12:18 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserInterest extends Model
{
    public $table="user_interest";
    protected $primaryKey = "id";
    protected $fillable = [
        'id',
        'email_address'
    ];
}
