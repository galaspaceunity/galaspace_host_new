<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCatering extends Model
{
    protected $primaryKey = "id";
    public $table = "service_catering";
    protected $fillable = [
        'id',
        'catering_id',
        'service_id',
        'catering_description',
        'catering_type',
        'catering_halal',
        'menu_id'
    ];

}
