<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $table="payments";
    protected $primaryKey = "payment_id";
    public $incrementing = false;
    protected $fillable = [
        'payment_id',
        'book_id',
        'payment_amount',
        'payment_currency',
        'payment_generated',
        'payment_due',
        'payment_status',
        'user_id'
    ];

}