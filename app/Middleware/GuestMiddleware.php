<?php

namespace App\Middleware;

class GuestMiddleware extends Middleware
{
	public function __invoke($request, $response, $next)
	{

		if ($this->container->auth->check()) {
			$this->container->flash->addMessage('warning', "You are accessing the page which is unavailable!");
			return $response->withRedirect($request->getUri()->getBasePath());
		}

		$response = $next($request, $response);
		return $response;
	}
}
