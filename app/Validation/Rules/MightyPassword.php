<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

class MightyPassword extends AbstractRule
{
	public function validate($input)
	{
		if (preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])/', $input)) {
			return $input;
		}
	}

}
