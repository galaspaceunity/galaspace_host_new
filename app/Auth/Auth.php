<?php

namespace App\Auth;

use App\Models\User;

class Auth
{
	public function user()
	{
		if (isset($_SESSION['user_id'])) {
			return User::find($_SESSION['user_id']);
		}
	}

	public function check()
	{
		return isset($_SESSION['user_id']);
	}

	public function attempt($username, $password)
	{
			$user = User::where('user_name', $username)->first();

			if (!$user) {
				return false;
			}

			if (password_verify($password, $user->user_password)) {
                $_SESSION['user_id'] = $user->user_id;
                $_SESSION['user_full_name'] = $user->user_full_name;
                $_SESSION['user_avatar'] = $user->user_image;
                $_SESSION['user_name'] = $user->user_name;
				if($user->user_type === "administrator"){
                    $_SESSION['user_admin'] = "true";
                }else{
                    $_SESSION['user_admin'] = "";
                }
                return true;
			}

			return false;
	}

	public function logout()
	{
		unset($_SESSION['user_id']);
	}


}
