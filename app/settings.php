<?php
//PHP Error Reporting
// error_reporting(0);

// Environment Variable Setup
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

// Timezone
setTimezone(getenv('TIMEZONE'));

    function setTimezone($timezone)
    {
	return date_default_timezone_set($timezone);
}

// General Settings
$config = [
	'settings' => [
		'displayErrorDetails' => true,
	],

//	//live db
		'db' => [
			'driver' 	=> 'mysql',
			'host' 		=> 'www.galaspace.com',
			'database' 	=> 'galaspac_dev',
			'username' 	=> 'galaspac_db2',
			'password' 	=> 'galaspaceunity',
			'charset' 	=> 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix' 	=> '',
		]

	//local db
//		 'db' => [
//		 	'driver' 	=> 'mysql',
//		 	'host' 		=> '127.0.0.2',
//		 	'database' 	=> 'galaspace_db',
//		 	'username' 	=> 'root',
//		 	'password' 	=> '',
//		 	'charset' 	=> 'utf8',
//		 	'collation' => 'utf8_unicode_ci',
//		 	'prefix' 	=> '',
//		 ]
];
