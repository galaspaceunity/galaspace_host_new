<?php

use Respect\Validation\Validator as v;

$container = $app->getContainer();

//Auth Dependency Injection Container
$container['auth'] = function($container) {
	return new \App\Auth\Auth;
};

//View Dependency Injection Container
$container['view'] = function($container) {
	$view =  new \Slim\Views\Twig(__DIR__ . '/../resources/Views', [
		'cache' => false,
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$container->router,
		$container->request->getUri()
	));

	$view->getEnvironment()->addGlobal('auth', [
		'check' => $container->auth->check(),
		'user' => $container->auth->user(),
	]);

	$view->getEnvironment()->addGlobal('flash', $container->flash);

	return $view;
};

//Controller Dependency Injection Container
$container['HomeController'] = function($container) {
	return new \App\Controllers\HomeController($container);
};

$container['AdministratorController'] = function($container) {
    return new \App\Controllers\AdministratorController($container);
};

$container['AuthController'] = function($container) {
	return new \App\Controllers\AuthController($container);
};

$container['SpaceRegistrationController'] = function($container) {
	return new \App\Controllers\SpaceRegistrationController($container);
};

$container['ServiceRegistrationController'] = function($container) {
    return new \App\Controllers\ServiceRegistrationController($container);
};

$container['UserRegistrationController'] = function($container) {
	return new \App\Controllers\UserRegistrationController($container);
};

$container['DashboardController'] = function($container) {
	return new \App\Controllers\DashboardController($container);
};

$container['BookingController'] = function($container) {
    return new \App\Controllers\BookingController($container);
};

$container['ReportController'] = function($container) {
    return new \App\Controllers\ReportController($container);
};
//Model Dependency Injection Container
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function($container) use ($capsule) {
	return $capsule;
};

//MiddleWare Dependency Injection Container
$app->add(new \App\Middleware\CsrfMiddleware($container));

$container['csrf'] = function($container) {
	$guard = new \Slim\Csrf\Guard();
	$guard->setPersistentTokenMode(true);
	return $guard;
	// return new \Slim\Csrf\Guard;
};
$app->add($container->csrf);

//Validation Dependency Injection Container
$container['validator'] = function($container) {
	return new App\Validation\Validator;
};

$app->add(new \App\Middleware\ValidationMiddleware($container));
$app->add(new \App\Middleware\PreviousInputMiddleware($container));

v::with('App\\Validation\\Rules\\');

//Flash Dependency Injection
$container['flash'] = function($container) {
	return new \Slim\Flash\Messages;
};

//Not Found Dependency Injection Container
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['view']->render($response->withStatus(404), '404.twig');
    };
};

//Nothing
// $container['nothing'] = function($container) {
// 	return new \App\Nothing;
// };
