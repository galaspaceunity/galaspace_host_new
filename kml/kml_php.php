<?php

$username = 'aimanidr_GSkml';
$password = 'galaspaceKML';
$database = 'aimanidr_galaspaceDev';
$server = 'www.aimanidris.my';

// Opens a connection to a MySQL server.
$conn = mysqli_connect($server, $username, $password, $database);
if (!$conn)
{
    die('Not connected : ' . mysqli_connect_error());
}

// Sets the active MySQL database.
//$db_selected = mysqli_select_db($database, $conn);
//if (!$db_selected)
//{
//    die ('Can\'t use db : ' . mysqli_error());
//}

// Selects all the rows in the markers table.
$result = mysqli_query($conn,"select * from spaces where space_status='PENDING'");

// Creates an array of strings to hold the lines of the KML file.
$kml = array('<?xml version="1.0" encoding="UTF-8"?>');
$kml[] = '<kml xmlns="http://earth.google.com/kml/2.1">';
$kml[] = ' <Document>';
// Iterates through the rows, printing a node for each row.
if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $kml[] = '<Folder>';
        $kml[] = '<Placemark id="placemark' . $row['space_id'] . '">';
        $kml[] = ' <name>' . htmlentities($row['space_name']) . '</name>';
        $kml[] = ' <description><![CDATA[ '. htmlentities($row['map_location']) .']]></description>';
        $kml[] = ' <Point>';
        $kml[] = ' <coordinates>' . $row['space_longitude'] . ',' . $row['space_latitude'] . '</coordinates>';
        $kml[] = ' </Point>';
        $kml[] = ' </Placemark>';
        $kml[] = '</Folder>';
    }
}else{
    echo "Not found";
}

mysqli_close($conn);

// End XML file
$kml[] = ' </Document>';
$kml[] = '</kml>';
$kmlOutput = join("\n", $kml);
header('Content-type: application/vnd.google-earth.kml+xml');
echo $kmlOutput;
?>