var base_url = window.location;
var base_url_host = window.location.hostname;
var base_url_protocol = window.location.protocol;

var geocoder;
var map;
var infowindow;
var exact_location;
var placeID;

// Google Autocomplete Search
var inputSrc = document.getElementById("event_location");
var autocomplete = new google.maps.places.Autocomplete(inputSrc);

// Set initial restrict to the greater list of countries.
autocomplete.setComponentRestrictions({'country': ['sg']});

// // Attr to get lat and lng
// autocomplete.addListener('place_changed', function () {
//     var lat = autocomplete.getPlace().geometry.location.lat();
//     var lng = autocomplete.getPlace().geometry.location.lng();
//     var placeID = autocomplete.getPlace().place_id;
//
//     if (placeID != "") {
//         location.href = "/event-spaces?" + placeID;
//     }
// });

// function initMap() {
    geocoder = new google.maps.Geocoder();
    var myLatlng = new google.maps.LatLng(1.3285349, 103.8238249);
    var mapOptions = {
        zoom: 12,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var georssLayer = new google.maps.KmlLayer({
        url: "http://galaspace.com/kml/kml_php.php"
    });
    // var layer = new google.maps.FusionTablesLayer({
    //     query: {
    //         select: 'location',
    //         from: '{{ place_id }}',
    //     }
    // });
    // layer.setMap(map);
    georssLayer.setMap(map);
// }
