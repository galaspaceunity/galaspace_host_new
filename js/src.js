var base_url = window.location.origin;

$('.dtLoad').hide();
    $('form#frmSrc').submit(function (e) {
        // prevent the page from submitting like normal
        e.preventDefault();
        $('.dtAvailable').hide();
        $('.dtLoad').show();
        $.ajax({
            url: '/event-spaces',
            type: 'get',
            data: $(this).serialize(),
            success: function (dt) {
                $('.spLoop').empty();
                for(var i in dt) {
                    var dtImg = dt[i].space_main_image;
                    if (dtImg === null || dtImg === "") {
                        var imgAppnd = base_url + "/img/bg-img/event_organizer.jpg";
                    } else {
                        var imgAppnd = base_url + "" + dtImg;
                    }
                    $('.spLoop').append(
                        '<a href="'+base_url+'/space-overview/'+dt[i].space_id+'" style="color: black;"><div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 25px;">' +
                        '<div class="col-lg-11 event-space-image">' +
                        '<img src="'+imgAppnd.toString()+'" class="img-responsive"/><br>' +
                        '<div class="event-space-info">' +
                        '<p class=""><b>'+dt[i].space_currency+dt[i].space_pricing+' '+dt[i].space_name+'</b><br>'+dt[i].space_area+'</p>' +
                        '<div class="event-space-det"><span class="event-stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span> 43 Reviews <span class="event-capacity">'+dt[i].space_guest+' <i class="fa fa-users"></i></span></div>' +
                        '</div>' +
                        '</div>' +
                        '</div></a>');
                }
                $('.dtAvailable').show();
                $('.spLoop').show();
                $('.dtLoad').hide();
                console.log('Data Loaded');
            },
            error: function (ee) {
                console.log("----------------");
                console.log(ee);
                console.log("----------------");
                console.log('Data Load Failed!');
            }
        });
    });