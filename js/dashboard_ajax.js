var base_url = window.location.origin;
var user_id = $('.hidden-class').attr('data-userid');
var space_id = $('.hidden-class').attr('data-spaceid');
var service_id = $('.hidden-class').attr('data-serviceid');
var catering_id = $('.hidden-class').attr('data-cateringid');
var menu_id = $('.hidden-class').attr('data-menuid');

$(".history").hide();
$(".mylistings").hide();
$(".inbox").hide();
$(".settings").hide();
$(".mybookings").hide();
$(".security").hide();
// My Profile Form
// $('input#profile-submit').submit(function(event){
// 	event.preventDefault();
// 	// form validation
// 	// $(this).validate({
// 	// 	rules: {
// 	// 		email: {
// 	// 			required: true,
// 	// 			email: true
// 	// 		}
// 	// 	}
// 	// });
// 	// end of form validation
//
// 	$.ajax({
// 		type: 'POST',
//         // mimeType: 'multipart/form-data',
// 		data: $('form.myprofile-form').serialize(),
//         // data: fd,
//         dataType: 'json',
//         contentType: false,
//         cache:false,
//         processData: false
// 	});
// 	// return false;
// });
// End of My Profile Form

// Security Form
$('input#password_security-submit').submit(function(e) {
    e.preventDefault();
    // form validation
    // $(this).validate({
    // 	rules: {
    // 		email: {
    // 			required: true,
    // 			email: true
    // 		}
    // 	}
    // });
    // end of form validation

    $.ajax({
        url: base_url+'/host-dashboard/'+user_id,
        type: "POST",
        data: $('form.password_security-form').serialize(),
        // dataType: 'json',
        // contentType: false,
        // cache:false,
        // processData: false,
        // beforeSend: function() {
        //     $('#password_security-submit').html('Updating <i class="fa fa-spinner fa-spin"></i>');
        // },
        success: function() {

            alert("HELLO");
            // swal({
            //     type: 'success',
            //     title: 'Security Password Updated',
            //     text: data,
            //     confirmButtonColor: "#00a99d",
            // });
            // $('#password_security-submit').html('Update');
        },
        error: function(response) {
            swal({
                type: 'error',
                title: 'Security Password Error',
                text: response,
                confirmButtonColor: "#00a99d",
            });
            $('#password_security-submit').html('Update');
        }
    });
});
// End of Security Form

//Space overview ajax starts here

// Space Delete
$('.deleteSpace-form').submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'http://localhost/space-overview/'+space_id,
        type: "POST",
        contentType: 'multipart/form-data',
        data: new FormData(this),
        dataType: "JSON",
        processData: false,
        contentType: false,
    });
});
//End of Space Delete

// Service Delete
$('.deleteSpace-form').submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'http://localhost/service-overview/'+service_id,
        type: "POST",
        contentType: 'multipart/form-data',
        data: new FormData(this),
        dataType: "JSON",
        processData: false,
        contentType: false,
    });
});
//End of Service Delete

// Service Menu Delete
$('.deleteMenu-form').submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'http://localhost/service-overview/'+service_id,
        type: "POST",
        contentType: 'multipart/form-data',
        data: new FormData(this),
        dataType: "JSON",
        processData: false,
        contentType: false,
    });
});
//End of Service Menu Delete

// Catering Menu Form
$('#menu-submit').submit(function(e) {
    e.preventDefault();

    var form = $('.menu-form')[0];
    var data = new FormData(form);
    $.ajax({
        url: 'http://localhost/service-menu/'+catering_id,
        type: "POST",
        enctype: 'multipart/form-data',
        // data: $(this).serialize(),
        data: data,
        dataType: json,
        contentType: false,
        cache:false,
        processData: false,
    });
});

// New Catering Menu Overview Form
$('#newmenu-submit').submit(function(e) {
    e.preventDefault();
    // form validation
    // $(this).validate({
    // 	rules: {
    // 		email: {
    // 			required: true,
    // 			email: true
    // 		}
    // 	}
    // });
    // end of form validation

    var form = $('.newmenuoverview-form')[0];
    var data = new FormData(form);
    $.ajax({
        url: 'http://localhost/service-menu/'+catering_id,
        type: "POST",
        enctype: 'multipart/form-data',
        // data: $(this).serialize(),
        data: data,
        dataType: json,
        contentType: false,
        cache:false,
        processData: false,
        // beforeSend: function() {
        // 	$('#myprofile-submit').html('Updating <i class="fa fa-spinner fa-spin"></i>');
        // },
        // success: function(response) {
        // 	swal({
        // 		type: 'success',
        // 		title: 'My Profile Updated',
        // 		text: response,
        // 		confirmButtonColor: "#00a99d",
        // 	});
        // 	$('#myprofile-submit').html('Update');
        // },
        // error: function(e) {
        // 	swal({
        // 		type: 'error',
        // 		title: 'My Profile Error',
        // 		text: e,
        // 		confirmButtonColor: "#00a99d",
        // 	});
        // 	$('#myprofile-submit').html('Update');
        // }
    });
});
//End of New Catering Menu form

// Catering Overview Form
$('.editMenu-form').submit(function(e) {
    e.preventDefault();
    // form validation
    // $(this).validate({
    // 	rules: {
    // 		email: {
    // 			required: true,
    // 			email: true
    // 		}
    // 	}
    // });
    // end of form validation

    $.ajax({
        url: 'http://localhost/service-overview/'+service_id,
        type: "POST",
        enctype: 'multipart/form-data',
        data: $(this).serialize(),
        data: new FormData(this),
        dataType: "JSON",
        // contentType: false,
        // cache:false,
        // processData: false,
    });
});
//End of catering overview form

// Catering Menu Overview Form
$('.servicemenuoverview-form').submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'http://localhost/service-overview/'+service_id,
        type: "POST",
        contentType: 'multipart/form-data',
        data: new FormData(this),
        dataType: "JSON",
        processData: false,
        contentType: false,
    });
});
//End of catering menu overview form

// Service Detail Overview Form
$('.servicedetailoverview-form').submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'http://localhost/service-overview/'+service_id,
        type: "POST",
        contentType: 'multipart/form-data',
        data: new FormData(this),
        dataType: "JSON",
        processData: false,
        contentType: false,
    });
});
//End of service detail overview form
// $(document).ready(function() {
//     if ($('#bookingToggle').is(':checked')) {
//         $('#booking_user').show();
//         $('#booking_hosts').hide();
//     } else {
//         $('#booking_user').hide();
//         $('#booking_hosts').show();
//     }
// });

$('#booking_user').show();
$('#booking_hosts').hide();
$('#bookingToggle').change(function() {
    if($('#bookingToggle').prop('checked')){
        $('#booking_user').show();
        $('#booking_hosts').hide();
    }else{
        $('#booking_user').hide();
        $('#booking_hosts').show();
    }
});




