"use strict";

$(document).ready(function() {
    $('form#frmBooking').submit(function (e) {
        // prevent the page from submitting like normal
        e.preventDefault();
        swal({
            title: 'Confirmation',
            text: 'Are you sure you want to book this event?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Book Now!'
        }).then(function(){
            $.ajax({
                url: "/space-booking",
                type: "POST",
                data: $('#frmBooking').serialize(),
                dataType: 'json',
                success: function (dt) {
                    console.log(dt);
                    if(dt.includes("Validation Failed")){
                        return location.reload();
                    }
                    if(dt.includes("success")){
                        return window.location = '/service-booking/'+dt[1];
                    }
                    var text = "";
                    var i;
                    for (i = 0; i < dt.length; i++) {
                        text += dt[i] + "<br/>";
                    }
                    swal({
                        type: 'error',
                        title: 'Data Error',
                        html: "<p style='text-align: left'>" + text + "</p><hr>"
                    });
                },
                error: function(dt) {
                    console.log(dt);
                    console.log("error. please mail to aiman@galaspace.com")
                },
            });
        })
    });

    $("#frmsvBooking").submit(function(e){
        // prevent the page from submitting like normal
        e.preventDefault();
        swal({
            title: 'Confirmation',
            text: 'Are you sure you want to book this service for your event?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Book Now!'
        }).then(function(){
            $.ajax({
                url: "/service-booking",
                type: "POST",
                data: $('#frmsvBooking').serialize(),
                dataType: 'json',
                success: function (dt) {
                    console.log(dt);
                    if(dt.includes("Validation Failed")){
                        return location.reload();
                    }
                    if(dt.includes("success")){
                        return window.location = '/service-booking/'+dt[1];
                    }
                    var text = "";
                    var i;
                    for (i = 0; i < dt.length; i++) {
                        text += dt[i] + "<br/>";
                    }
                    swal({
                        type: 'error',
                        title: 'Data Error',
                        html: "<p style='text-align: left'>" + text + "</p><hr>"
                    });
                },
                error: function(dt) {
                    console.log(dt);
                    console.log("error. please mail to aiman@galaspace.com")
                },
            });
        })
    });

    //booking overview
    $('#booking-overview').show();
    $('#booking-overview-edit').hide();
    $('#booking-edit-btn').show();
    $('#booking-update-btn').hide();

    $('#booking-edit-btn').click(function () {
        $('#booking-edit-btn').hide();
        $('#booking-update-btn').show();
        $('#booking-overview').hide();
        $('#booking-overview-edit').show();
    });

    $('#booking-update-btn').click(function () {
        var book_id = $('#book_id');
        swal({
            title: 'Confirmation',
            text: 'Are you sure you want to update this booking?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No, Do not update',
            confirmButtonText: 'Yes, Update Now!',
        }).then(function(){
            $.ajax({
                url: "/booking-overview/"+book_id,
                type: "POST",
                data: $('#booking-edit').serialize(),
                dataType: 'json',
                success: function (dt) {
                    console.log(dt);
                    if(dt.includes("success")){
                        return window.location = '/booking-overview/'+dt[1];
                    }
                },
                error: function(dt) {
                    console.log(dt);
                    console.log("error. please mail to aiman@galaspace.com")
                },
            });
        });
    });

});

$(".spLoopCatering").hide();
$(".spLoopCatering").slice(0,3).show();
$(".spLoopMusic").hide();
$(".spLoopMusic").slice(0,3).show();
$(".spLoopPV").hide();
$(".spLoopPV").slice(0,3).show();

$("#loadMoreCatering").on('click', function (e) {
    e.preventDefault();
    var a = document.getElementById('loadMoreCatering').innerText;
    console.log(a);
    if(a == "Load More"){
        $(".spLoopCatering:hidden").slice().slideDown();
        document.getElementById('loadMoreCatering').innerText = "Load Less";
    }else{
        $(".spLoopCatering").hide();
        $(".spLoopCatering").slice(0,3).show();
        document.getElementById('loadMoreCatering').innerText = "Load More";
    }
});
$("#loadMoreMusic").on('click', function (e) {
    e.preventDefault();
    var a = document.getElementById('loadMoreMusic').innerText;
    console.log(a);
    if(a == "Load More"){
        $(".spLoopMusic:hidden").slice().slideDown();
        document.getElementById('loadMoreMusic').innerText = "Load Less";
    }else{
        $(".spLoopMusic").hide();
        $(".spLoopMusic").slice(0,3).show();
        document.getElementById('loadMoreMusic').innerText = "Load More";
    }
});
$("#loadMorePV").on('click', function (e) {
    e.preventDefault();
    var a = document.getElementById('loadMorePV').innerText;
    console.log(a);
    if(a == "Load More"){
        $(".spLoopPV:hidden").slice().slideDown();
        document.getElementById('loadMorePV').innerText = "Load Less";
    }else{
        $(".spLoopPV").hide();
        $(".spLoopPV").slice(0,3).show();
        document.getElementById('loadMorePV').innerText = "Load More";
    }
});
var spbook_id = $('.spbookID').attr('value');

function modalSvcCancel(a,b) {
    var svbook_id = a;
    var spbook_id = b;
    var form = $(this).parents('form');
    swal({
        title: 'Confirmation',
        text: 'Are you sure you want to cancel this service booking?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'No, Do not cancel',
        confirmButtonText: 'Yes, Cancel Now!',
    }).then(function(){
        location.href = '/delete-svBooking/'+svbook_id+'/'+spbook_id;
    });
};

function spbook_Confirm_Vendor(a) {
    swal({
        title: 'Confirmation',
        text: 'Are you sure you want to confirm this space booking?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'No, Do not submit',
        confirmButtonText: 'Yes, Submit Now!',
    }).then(function () {
        $('#frmVC').submit();
    });
};

function dtValidateTime(spaceId){
    var dt = $('.available-date').val();
    var tClone = $('.booking-timeH2').clone();
    $('.booking-timeH').empty();
    $('.booking-timeH').append(tClone.clone());
    var disableTime = [];
    $.ajax({
        url: "/getSpaceBookingAvailability/"+spaceId+"/"+dt,
        type: "GET",
        dataType: 'json',
        success: function (dt) {
            var fromJSON = [];
            var toJSON = [];
            var fromSplitH = [];
            var fromSplitM = [];
            var toSplitH = [];
            var toSplitM = [];
            var i;
            for(i=0; i<dt.length;i++){
                fromJSON[i] = dt[i][0];
                toJSON[i] = dt[i][1];
                var fromSplit = fromJSON[i].split(":");
                var toSplit = toJSON[i].split(":");
                if(fromSplit[0] == "00"){
                    fromSplitH[i] = "0";
                }else{
                    fromSplitH[i] = fromSplit[0];
                }
                if(fromSplit[1] == "00"){
                    fromSplitM[i] = "0";
                }else{
                    fromSplitM[i] = fromSplit[1];
                }
                if(toSplit[0] == "00"){
                    toSplitH[i] = "0";
                }else{
                    toSplitH[i] = toSplit[0];
                }
                if(toSplit[1] == "00"){
                    toSplitM[i] = "0";
                }else{
                    toSplitM[i] = toSplit[1];
                }
                disableTime.push('{ "from" : ['+ parseInt(fromSplitH[i]) + ',' + parseInt(fromSplitM[i]) + '], "to" : [' + parseInt(toSplitH[i]) + ',' + parseInt(toSplitM[i]) + '] }' );
            }
            var e = eval('['+disableTime+']');
            $('.booking-time').pickatime({
                disable: e
            });
        },
        error: function(dt) {
            console.log("error. please mail to aiman@galaspace.com")
        },
    });
}

function dtError(){
    swal({
        title: "Error",
        text: "Please choose date!",
        type: 'error',
    });
}
