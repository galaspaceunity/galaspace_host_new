$(document).ready(activateJS());
var base_url = window.location;
var base_url_host = window.location.hostname;
var base_url_protocol = window.location.protocol;
$('.dtLoad').hide();
//login js
// $(document).ready(function(){
$('#signin-btn').click(function(){
	var uname = $('#username').val();
	var pswd = $('#password').val();
	if(uname != "" && pswd != ""){
		$('.dtLoad').show();
		$.ajax({
			url: "/signin",
			type: "POST",
			data: $('#loginFrm').serialize(),
			success: function (dt) {
				location.reload();
			},
			error: function(dt) {
				console.log(dt);
				$('.dtLoad').hide();
				swal({
					type: 'error',
					title: 'Invalid Username or Password',
				});
			},
		});
	}else{
		swal({
			type: 'warning',
			title: 'Please enter your username and password',
		});
	}
});
// });

function activateJS() {
	$('.space-overview-carousel').slick({
		centerMode: true,
		slidesToShow: 3,
		infinite: true,
		dots: true,
		arrows: true,
		autoplay: true,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.service-overview-carousel').slick({
		centerMode: true,
		slidesToShow: 3,
		infinite: true,
		dots: true,
		arrows: true,
		autoplay: true,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.current_year').text(new Date().getFullYear());
}

var readURL = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('.profile-img').attr('src', e.target.result);
			$('.menu-img').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$(".profile-photo").on('change', function(){
	readURL(this);
});

$(".profile-upload-btn").on('click', function() {
	$(".profile-photo").click();
});

$('[data-toggle="tooltip"]').tooltip();

// Dashboard Tabs
$(".profile").show();
$("#profile").click(function() {
	$('.profile').show();
	$('.history').hide();
	$('.mylistings').hide();
	$('.inbox').hide();
	$('.settings').hide();
	$('.mybookings').hide();
	$('.security').hide();
});
$("#user").click(function() {
	$('.profile').hide();
	$('.history').hide();
	$('.mylistings').hide();
	$('.inbox').hide();
	$('.settings').hide();
	$('.mybookings').hide();
	$('.security').hide();
});
$("#history").click(function() {
	$('.profile').hide();
	$('.history').show();
	$('.mylistings').hide();
	$('.inbox').hide();
	$('.settings').hide();
	$('.mybookings').hide();
	$('.security').hide();
});
$("#mylistings").click(function() {
	$('.profile').hide();
	$('.history').hide();
	$('.mylistings').show();
	$('.inbox').hide();
	$('.settings').hide();
	$('.mybookings').hide();
	$('.security').hide();
});
$("#inbox").click(function() {
	$('.profile').hide();
	$('.history').hide();
	$('.mylistings').hide();
	$('.inbox').show();
	$('.settings').hide();
	$('.mybookings').hide();
	$('.security').hide();
});
$("#settings").click(function() {
	$('.profile').hide();
	$('.history').hide();
	$('.mylistings').hide();
	$('.inbox').hide();
	$('.settings').show();
	$('.mybookings').hide();
	$('.security').hide();
});
$("#mybookings").click(function() {
	$('.profile').hide();
	$('.history').hide();
	$('.mylistings').hide();
	$('.inbox').hide();
	$('.settings').hide();
	$('.mybookings').show();
	$('.security').hide();
});
$("#security").click(function() {
	$('.profile').hide();
	$('.history').hide();
	$('.mylistings').hide();
	$('.inbox').hide();
	$('.settings').hide();
	$('.mybookings').hide();
	$('.security').show();
});

// Space Overview Tabs
$('.edit-overview-btn').show();
$('.edit-amenities-btn').hide();

$(".overview").show();
$("#overview").css({'border-bottom':'3px solid #00a99d', 'padding-bottom':'15px'});
$("#overview").click(function() {
	$(this).css({'border-bottom':'3px solid #00a99d', 'padding-bottom':'15px'});
	$('#amenities').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#review').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#location').css({'border-bottom':'none', 'padding-bottom':'5px'});

	$('.edit-overview-btn').show();
	$('.edit-amenities-btn').hide();

	$('.overview').show();
	$('.amenities').hide();
	$('.edit-amenities').hide();
	$('.review').hide();
	$('.location').hide();

});
$("#amenities").click(function() {
	$(this).css({'border-bottom':'3px solid #00a99d', 'padding-bottom':'15px'});
	$('#overview').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#review').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#location').css({'border-bottom':'none', 'padding-bottom':'5px'});

	$('.edit-overview-btn').hide();
	$('.edit-amenities-btn').show();

	$('.amenities').show();
	$('.overview').hide();
	$('.edit-overview').hide();
	$('.review').hide();
	$('.location').hide();
});
$("#review").click(function() {
	$(this).css({'border-bottom':'3px solid #00a99d', 'padding-bottom':'15px'});
	$('#amenities').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#overview').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#location').css({'border-bottom':'none', 'padding-bottom':'5px'});

	$('.overview').hide();
	$('.amenities').hide();
	$('.review').show();
	$('.location').hide();
});
$("#location").click(function() {
	$(this).css({'border-bottom':'3px solid #00a99d', 'padding-bottom':'15px'});
	$('#amenities').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#review').css({'border-bottom':'none', 'padding-bottom':'5px'});
	$('#overview').css({'border-bottom':'none', 'padding-bottom':'5px'});

	$('.overview').hide();
	$('.amenities').hide();
	$('.review').hide();
	$('.location').show();
});


// Settings Tab
$('.payout-methods-container').hide();
$('.transaction-history-container').hide();
$('.account-settings-container').hide();

$('.payment-methods').click(function() {
	$(this).addClass('active');
	$('.setting-lists>li:not(.payment-methods)').removeClass('active');
	$('.payment-methods-container').fadeIn();
	$('.payout-methods-container').hide();
	$('.transaction-history-container').hide()
	$('.account-settings-container').hide()
});

$('.payout-methods').click(function() {
	$(this).addClass('active');
	$('.setting-lists>li:not(.payout-methods)').removeClass('active');
	$('.payment-methods-container').hide();
	$('.payout-methods-container').fadeIn();
	$('.transaction-history-container').hide()
	$('.account-settings-container').hide()
});

$('.transaction-history').click(function() {
	$(this).addClass('active');
	$('.setting-lists>li:not(.transaction-history)').removeClass('active');
	$('.payment-methods-container').hide();
	$('.payout-methods-container').hide();
	$('.transaction-history-container').fadeIn();
	$('.account-settings-container').hide()
});

$('.account-settings').click(function() {
	$(this).addClass('active');
	$('.setting-lists>li:not(.account-settings)').removeClass('active');
	$('.payment-methods-container').hide();
	$('.payout-methods-container').hide();
	$('.transaction-history-container').hide()
	$('.account-settings-container').fadeIn()
});
// End of Settings Tab

// My Bookings Tab
$('.contact-organizer-container').hide();
$('.contact-subhosts-container').hide();
$('.contact-galaspace-container').hide();
$('.deposit-payment-container').hide();
$('.cancel-booking-container').hide();

$('.event-details').click(function() {
	$(this).addClass('active');
	$('.mybooking-lists>li:not(.event-details)').removeClass('active');
	$('.event-details-container').fadeIn();
	$('.contact-organizer-container').hide();
	$('.contact-subhosts-container').hide();
	$('.contact-galaspace-container').hide();
	$('.deposit-payment-container').hide();
	$('.cancel-booking-container').hide();
});

$('.contact-organizer').click(function() {
	$(this).addClass('active');
	$('.mybooking-lists>li:not(.contact-organizer)').removeClass('active');
	$('.event-details-container').hide();
	$('.contact-organizer-container').fadeIn();
	$('.contact-subhosts-container').hide();
	$('.contact-galaspace-container').hide();
	$('.deposit-payment-container').hide();
	$('.cancel-booking-container').hide();
});

$('.contact-subhosts').click(function() {
	$(this).addClass('active');
	$('.mybooking-lists>li:not(.contact-subhosts)').removeClass('active');
	$('.event-details-container').hide();
	$('.contact-organizer-container').hide();
	$('.contact-subhosts-container').fadeIn();
	$('.contact-galaspace-container').hide();
	$('.deposit-payment-container').hide();
	$('.cancel-booking-container').hide();
});

$('.contact-galaspace').click(function() {
	$(this).addClass('active');
	$('.mybooking-lists>li:not(.contact-galaspace)').removeClass('active');
	$('.event-details-container').hide();
	$('.contact-organizer-container').hide();
	$('.contact-subhosts-container').hide();
	$('.contact-galaspace-container').fadeIn();
	$('.deposit-payment-container').hide();
	$('.cancel-booking-container').hide();
});

$('.deposit-payment').click(function() {
	$(this).addClass('active');
	$('.mybooking-lists>li:not(.deposit-payment)').removeClass('active');
	$('.event-details-container').hide();
	$('.contact-organizer-container').hide();
	$('.contact-subhosts-container').hide();
	$('.contact-galaspace-container').hide();
	$('.deposit-payment-container').fadeIn();
	$('.cancel-booking-container').hide();
});

$('.cancel-booking').click(function() {
	$(this).addClass('active');
	$('.mybooking-lists>li:not(.cancel-booking)').removeClass('active');
	$('.event-details-container').hide();
	$('.contact-organizer-container').hide();
	$('.contact-subhosts-container').hide();
	$('.contact-galaspace-container').hide();
	$('.deposit-payment-container').hide();
	$('.cancel-booking-container').fadeIn();
});
// End of My Bookings Tab

// FAQ Question Selector

$("#faq-option li").on("click", function(){
	var faq = $(this).attr('faq-answer');
	var ques = $(this).html();

	$('.fq-ques').html(ques);
	$('.fq-ans').html(faq);

});

// End Of FAQ Selector

// Job Role Selector

$(".career-bd-item").on("click", function(){
	console.log('test');
	var faq = $(this).attr('role-bd');
	$('.role-info').html(faq);

});

// End Of FAQ Selector

//Service overview JS
//left side
$('.edit-overview').hide();
$('.edit-overview-details').hide();
$('.edit-overview-content').hide();
$('.update-overview-btn').hide();
$('.edit-cimg-btn').hide();
$('.review').hide();
$('.delete-service-btn').hide();
$('.edit-overview-btn').click(function() {
	$('.edit-overview').show();
	$('.overview').hide();
	$('.edit-overview-btn').hide();
	$('.update-overview-btn').show();
	$('.edit-cimg-btn').show();
	$('#review').hide();
	$('.delete-service-btn').show();
});
$('#review').click(function() {
	$('#review').show();
	$('.edit-overview-btn').hide();
	$('.edit-cimg-btn').hide();
	$('.delete-service-btn').hide();
});
$('#overview').click(function() {
	$('#review').show();
	$('.overview-content').show();
	$('.edit-overview').hide();
	$('.edit-overview-details').hide();
	$('.edit-overview-content').hide();
	$('.update-overview-btn').hide();
	$('.edit-cimg-btn').hide();
	$('.review').hide();
	$('.delete-service-btn').hide();
});
//end of service overview JS

var date = new Date();
var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
$('.available-dt').bootstrapMaterialDatePicker( { format: 'DD-MM-YYYY hh:mm:ss' } );
$('.available-date').bootstrapMaterialDatePicker( { format: 'DD-MM-YYYY', time:false, minDate:new Date() } );

$('.menu-photos').slick({
	dots: true,
	infinite: false,
	speed: 300,
	slidesToShow: 5,
	slidesToScroll: 5,
	responsive: [
		{
			breakpoint: 1366,
			settings: {
				slidesToShow: 5,
				slidesToScroll: 5,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 5,
				slidesToScroll: 5
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	]
});

//Space Images
// Space Image 1
var space_image1 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img1').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo1").on('change', function(){
	space_image1(this);
});

$("#space-upload-btn1").on('click', function() {
	$("#space-photo1").click();
});

$("#space-cancel-btn1").on('click', function() {
	document.getElementById('space-img1').removeAttribute('src');
	document.getElementById('space-img1').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn1').style.visibility = 'hidden';
	document.getElementById('img-attr1').setAttribute('value','0');
});

if($("#space-img1").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn1').style.visibility = 'hidden';
}

$("#space-img1").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn1').style.visibility = 'visible';
				document.getElementById('img-attr1').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 1

// Space Image 2
var space_image2 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img2').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo2").on('change', function(){
	space_image2(this);
});

$("#space-upload-btn2").on('click', function() {
	$("#space-photo2").click();
});

$("#space-cancel-btn2").on('click', function() {
	document.getElementById('space-img2').removeAttribute('src');
	document.getElementById('space-img2').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn2').style.visibility = 'hidden';
	document.getElementById('img-attr2').setAttribute('value','0');
});

if($("#space-img2").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn2').style.visibility = 'hidden';
}

$("#space-img2").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn2').style.visibility = 'visible';
				document.getElementById('img-attr2').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 2

// Space Image 3
var space_image3 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img3').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo3").on('change', function(){
	space_image3(this);
});

$("#space-upload-btn3").on('click', function() {
	$("#space-photo3").click();
});

$("#space-cancel-btn3").on('click', function() {
	document.getElementById('space-img3').removeAttribute('src');
	document.getElementById('space-img3').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn3').style.visibility = 'hidden';
	document.getElementById('img-attr3').setAttribute('value','0');
});

if($("#space-img3").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn3').style.visibility = 'hidden';
}

$("#space-img3").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn3').style.visibility = 'visible';
				document.getElementById('img-attr3').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 3

// Space Image 4
var space_image4 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img4').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo4").on('change', function(){
	space_image4(this);
});

$("#space-upload-btn4").on('click', function() {
	$("#space-photo4").click();
});

$("#space-cancel-btn4").on('click', function() {
	document.getElementById('space-img4').removeAttribute('src');
	document.getElementById('space-img4').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn4').style.visibility = 'hidden';
	document.getElementById('img-attr4').setAttribute('value','0');
});

if($("#space-img4").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn4').style.visibility = 'hidden';
}

$("#space-img4").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn4').style.visibility = 'visible';
				document.getElementById('img-attr4').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 4

// Space Image 5
var space_image5 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img5').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo5").on('change', function(){
	space_image5(this);
});

$("#space-upload-btn5").on('click', function() {
	$("#space-photo5").click();
});

$("#space-cancel-btn5").on('click', function() {
	document.getElementById('space-img5').removeAttribute('src');
	document.getElementById('space-img5').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn5').style.visibility = 'hidden';
	document.getElementById('img-attr5').setAttribute('value','0');
});

if($("#space-img5").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn5').style.visibility = 'hidden';
}

$("#space-img5").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn5').style.visibility = 'visible';
				document.getElementById('img-attr5').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 5

// Space Image 6
var space_image6 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img6').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo6").on('change', function(){
	space_image6(this);
});

$("#space-upload-btn5").on('click', function() {
	$("#space-photo5").click();
});

$("#space-cancel-btn6").on('click', function() {
	document.getElementById('space-img6').removeAttribute('src');
	document.getElementById('space-img6').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn6').style.visibility = 'hidden';
	document.getElementById('img-attr6').setAttribute('value','0');
});

if($("#space-img6").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn6').style.visibility = 'hidden';
}

$("#space-img6").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn6').style.visibility = 'visible';
				document.getElementById('img-attr6').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 6

// Space Image 7
var space_image7 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img7').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo7").on('change', function(){
	space_image7(this);
});

$("#space-upload-btn7").on('click', function() {
	$("#space-photo7").click();
});

$("#space-cancel-btn7").on('click', function() {
	document.getElementById('space-img7').removeAttribute('src');
	document.getElementById('space-img7').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn7').style.visibility = 'hidden';
	document.getElementById('img-attr7').setAttribute('value','0');
});

if($("#space-img7").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn7').style.visibility = 'hidden';
}

$("#space-img7").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn7').style.visibility = 'visible';
				document.getElementById('img-attr7').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 7

// Space Image 8
var space_image8 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#space-img8').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-photo8").on('change', function(){
	space_image8(this);
});

$("#space-upload-btn8").on('click', function() {
	$("#space-photo8").click();
});

$("#space-cancel-btn8").on('click', function() {
	document.getElementById('space-img8').removeAttribute('src');
	document.getElementById('space-img8').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btn8').style.visibility = 'hidden';
	document.getElementById('img-attr8').setAttribute('value','0');
});

if($("#space-img8").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btn8').style.visibility = 'hidden';
}

$("#space-img8").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btn8').style.visibility = 'visible';
				document.getElementById('img-attr8').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Image 8

// Space Cover Image
var space_imageC = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e){
			$('#space-imgC').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#space-upload-btnC").unbind('click');

$("#space-photoC").on('change', function(e){
	space_imageC(this);
});

$("#space-upload-btnC").on('click', function(e) {
	e.stopPropagation();
	$("#space-photoC").click();
});

if($("#space-imgC").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('space-cancel-btnC').style.visibility = 'hidden';
}

$("#space-cancel-btnC").on('click', function() {
	document.getElementById('space-imgC').removeAttribute('src');
	document.getElementById('space-imgC').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('space-cancel-btnC').style.visibility = 'hidden';
	document.getElementById('img-attrC').setAttribute('value','0');
});

$("#space-imgC").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('space-cancel-btnC').style.visibility = 'visible';
				document.getElementById('img-attrC').setAttribute('value', '1');
			}
		}
	}
});
// End of Space Cover Image

//End of Space Images

//Service Images
// Service Image 1
var service_image1 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#service-img1').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#service-photo1").on('change', function(){
	service_image1(this);
});

$("#service-upload-btn1").on('click', function() {
	$("#service-photo1").click();
});

$("#service-cancel-btn1").on('click', function() {
	document.getElementById('service-img1').removeAttribute('src');
	document.getElementById('service-img1').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('service-cancel-btn1').style.visibility = 'hidden';
	document.getElementById('img-attr1').setAttribute('value','0');
});

if($("#service-img1").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('service-cancel-btn1').style.visibility = 'hidden';
}

$("#service-img1").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('service-cancel-btn1').style.visibility = 'visible';
				document.getElementById('img-attr1').setAttribute('value', '1');
			}
		}
	}
});

// End of Service Image 1

// Service Image 2
var service_image2 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#service-img2').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#service-photo2").on('change', function(){
	service_image2(this);
});

$("#service-upload-btn2").on('click', function() {
	$("#service-photo2").click();
});

$("#service-cancel-btn2").on('click', function() {
	document.getElementById('service-img2').removeAttribute('src');
	document.getElementById('service-img2').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('service-cancel-btn2').style.visibility = 'hidden';
	document.getElementById('img-attr2').setAttribute('value','0');
});

if($("#service-img2").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('service-cancel-btn2').style.visibility = 'hidden';
}

$("#service-img2").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('service-cancel-btn2').style.visibility = 'visible';
				document.getElementById('img-attr2').setAttribute('value', '1');
			}
		}
	}
});
// End of Service Image 2

// Service Image 3
var service_image3 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#service-img3').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#service-photo3").on('change', function(){
	service_image3(this);
});

$("#service-upload-btn3").on('click', function() {
	$("#service-photo3").click();
});

$("#service-cancel-btn3").on('click', function() {
	document.getElementById('service-img3').removeAttribute('src');
	document.getElementById('service-img3').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('service-cancel-btn3').style.visibility = 'hidden';
	document.getElementById('img-attr3').setAttribute('value','0');
});

if($("#service-img3").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('service-cancel-btn3').style.visibility = 'hidden';
}

$("#service-img3").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('service-cancel-btn3').style.visibility = 'visible';
				document.getElementById('img-attr2').setAttribute('value', '1');
			}
		}
	}
});
// End of Service Image 3

// Service Image 4
var service_image4 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#service-img4').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#service-photo4").on('change', function(){
	service_image4(this);
});

$("#service-upload-btn4").on('click', function() {
	$("#service-photo4").click();
});

$("#service-cancel-btn4").on('click', function() {
	document.getElementById('service-img4').removeAttribute('src');
	document.getElementById('service-img4').setAttribute('src', base_url_protocol+"//"+base_url_host+'/img/placeholder.png');
	document.getElementById('service-cancel-btn4').style.visibility = 'hidden';
	document.getElementById('img-attr4').setAttribute('value','0');
});

if($("#service-img4").attr('src') == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
	document.getElementById('service-cancel-btn4').style.visibility = 'hidden';
}

$("#service-img4").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+"//"+base_url_host+"/img/placeholder.png")) {
				document.getElementById('service-cancel-btn4').style.visibility = 'visible';
				document.getElementById('img-attr4').setAttribute('value', '1');
			}
		}
	}
});
// End of Service Image 4
//End of Service Images

//Menu New Images
// Menu New Image 1
var menuN_image1 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menuN-img1').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menuN-photo1").on('change', function(){
	menuN_image1(this);
});

$("#menuN-upload-btn1").on('click', function() {
	$("#menuN-photo1").click();
});

$("#menuN-cancel-btn1").on('click', function() {
	document.getElementById('menuN-img1').removeAttribute('src');
	document.getElementById('menuN-img1').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menuN-cancel-btn1').style.visibility = 'hidden';
	document.getElementById('img-attr1').setAttribute('value','0');
});

if($("#menuN-img1").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menuN-cancel-btn1').style.visibility = 'hidden';
}

$("#menuN-img1").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")) {
				document.getElementById('menuN-cancel-btn1').style.visibility = 'visible';
				document.getElementById('img-attr1').setAttribute('value', '1');
			}
		}
	}
});

// End of Menu New Image 1

// Menu New Image 2
var menuN_image2 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menuN-img2').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menuN-photo2").on('change', function(){
	menuN_image2(this);
});

$("#menuN-upload-btn2").on('click', function() {
	$("#menuN-photo2").click();
});

$("#menuN-cancel-btn2").on('click', function() {
	document.getElementById('menuN-img2').removeAttribute('src');
	document.getElementById('menuN-img2').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menuN-cancel-btn2').style.visibility = 'hidden';
	document.getElementById('img-attr2').setAttribute('value','0');
});

if($("#menuN-img2").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menuN-cancel-btn2').style.visibility = 'hidden';
}

$("#menuN-img2").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")) {
				document.getElementById('menuN-cancel-btn2').style.visibility = 'visible';
				document.getElementById('img-attr2').setAttribute('value', '1');
			}
		}
	}
});
// End of Menu New Image 2


// Menu New Image 3
var menuN_image3 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menuN-img3').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menuN-photo3").on('change', function(){
	menuN_image3(this);
});

$("#menuN-upload-btn3").on('click', function() {
	$("#menuN-photo3").click();
});

$("#menuN-cancel-btn3").on('click', function() {
	document.getElementById('menuN-img3').removeAttribute('src');
	document.getElementById('menuN-img3').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menuN-cancel-btn3').style.visibility = 'hidden';
	document.getElementById('img-attr3').setAttribute('value','0');
});

if($("#menuN-img3").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menuN-cancel-btn3').style.visibility = 'hidden';
}

$("#menuN-img3").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")) {
				document.getElementById('menuN-cancel-btn3').style.visibility = 'visible';
				document.getElementById('img-attr3').setAttribute('value', '1');
			}
		}
	}
});
// End of Menu New Image 3

// Menu New Image 4
var menuN_image4 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menuN-img4').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menuN-photo4").on('change', function(){
	menuN_image4(this);
});

$("#menuN-upload-btn4").on('click', function() {
	$("#menuN-photo4").click();
});

$("#menuN-cancel-btn4").on('click', function() {
	document.getElementById('menuN-img4').removeAttribute('src');
	document.getElementById('menuN-img4').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menuN-cancel-btn4').style.visibility = 'hidden';
	document.getElementById('img-attr4').setAttribute('value','0');
});

if($("#menuN-img4").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menuN-cancel-btn4').style.visibility = 'hidden';
}

$("#menuN-img4").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
				document.getElementById('menuN-cancel-btn4').style.visibility = 'visible';
				document.getElementById('img-attr4').setAttribute('value', '1');
			}
		}
	}
});
// End of Menu New Image 4

// Menu Image 1
var menu_image1 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menu-img1').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menu-photo1").on('change', function(){
	menu_image1(this);
});

$("#menu-upload-btn1").on('click', function() {
	$("#menu-photo1").click();
});

$("#menu-cancel-btn1").on('click', function() {
	document.getElementById('menu-img1').removeAttribute('src');
	document.getElementById('menu-img1').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menu-cancel-btn1').style.visibility = 'hidden';
	document.getElementById('img-attr1').setAttribute('value','0');
});

if($("#menu-img1").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menu-cancel-btn1').style.visibility = 'hidden';
}

$("#menu-img1").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")) {
				document.getElementById('menu-cancel-btn1').style.visibility = 'visible';
				document.getElementById('img-attr1').setAttribute('value', '1');
			}
		}
	}
});

// End of Menu Image 1

// Menu Image 2
var menu_image2 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menu-img2').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menu-photo2").on('change', function(){
	menu_image2(this);
});

$("#menu-upload-btn2").on('click', function() {
	$("#menu-photo2").click();
});

$("#menu-cancel-btn2").on('click', function() {
	document.getElementById('menu-img2').removeAttribute('src');
	document.getElementById('menu-img2').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menu-cancel-btn2').style.visibility = 'hidden';
	document.getElementById('img-attr2').setAttribute('value','0');
});

if($("#menu-img2").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menu-cancel-btn2').style.visibility = 'hidden';
}

$("#menu-img2").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")) {
				document.getElementById('menu-cancel-btn2').style.visibility = 'visible';
				document.getElementById('img-attr2').setAttribute('value', '1');
			}
		}
	}
});
// End of Menu Image 2


// Menu Image 3
var menu_image3 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menu-img3').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menu-photo3").on('change', function(){
	menu_image3(this);
});

$("#menu-upload-btn3").on('click', function() {
	$("#menu-photo3").click();
});

$("#menu-cancel-btn3").on('click', function() {
	document.getElementById('menu-img3').removeAttribute('src');
	document.getElementById('menu-img3').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menu-cancel-btn3').style.visibility = 'hidden';
	document.getElementById('img-attr3').setAttribute('value','0');
});

if($("#menu-img3").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menu-cancel-btn3').style.visibility = 'hidden';
}

$("#menu-img3").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")) {
				document.getElementById('menu-cancel-btn3').style.visibility = 'visible';
				document.getElementById('img-attr3').setAttribute('value', '1');
			}
		}
	}
});
// End of Menu Image 3

// Menu Image 4
var menu_image4 = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#menu-img4').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menu-photo4").on('change', function(){
	menu_image4(this);
});

$("#menu-upload-btn4").on('click', function() {
	$("#menu-photo4").click();
});

$("#menu-cancel-btn4").on('click', function() {
	document.getElementById('menu-img4').removeAttribute('src');
	document.getElementById('menu-img4').setAttribute('src', base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	document.getElementById('menu-cancel-btn4').style.visibility = 'hidden';
	document.getElementById('img-attr4').setAttribute('value','0');
});

if($("#menu-img4").attr('src') == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
	document.getElementById('menu-cancel-btn4').style.visibility = 'hidden';
}

$("#menu-img4").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
			if(e.newValue != (base_url_protocol+'//'+base_url_host+"/img/placeholder.png")){
				document.getElementById('menu-cancel-btn4').style.visibility = 'visible';
				document.getElementById('img-attr4').setAttribute('value', '1');
			}
		}
	}
});
// End of Menu Image 4

//service menu edit JS
function divModalEdit(a) {

	var nMID = a.querySelector('#menuID').value.replace('#','');
	$('.modalID').attr('id','menu'+nMID);
	$('#menu_id').attr('value', a.querySelector('#menuID').value);
	$('.menu_name').attr('value', a.querySelector('#menuName').value);
	$('.menu_price').attr('value', a.querySelector('#menuPrice').value);
	$('.menu_description').val(a.querySelector('#menuDescription').value);
	var img1 = a.querySelector('#menuImg1').value;
	var img2 = a.querySelector('#menuImg2').value;
	var img3 = a.querySelector('#menuImg3').value;
	var img4 = a.querySelector('#menuImg4').value;

	var imgEmpty = (base_url_protocol+'//'+base_url_host+'/img/placeholder.png');
	if (img1 == "") {
		document.getElementById("menu-img1").src = imgEmpty.toString();
		document.getElementById('menu-cancel-btn1').style.visibility = 'hidden';
		document.getElementById('img-attr1').setAttribute('value','0');
	} else {
		document.getElementById("menu-img1").src = (base_url_protocol+'//'+base_url_host+'/'+img1.toString());
		document.getElementById('menu-cancel-btn1').style.visibility = 'visible';
		document.getElementById('img-attr1').setAttribute('value','1');
	}
	if (img2 == "") {
		document.getElementById("menu-img2").src = imgEmpty;
		document.getElementById('menu-cancel-btn2').style.visibility = 'hidden';
		document.getElementById('img-attr2').setAttribute('value','0');
	} else {
		document.getElementById("menu-img2").src = (base_url_protocol+'//'+base_url_host+'/'+img2.toString());
		document.getElementById('menu-cancel-btn2').style.visibility = 'visible';
		document.getElementById('img-attr2').setAttribute('value','1');
	}
	if (img3 == "") {
		document.getElementById("menu-img3").src = imgEmpty;
		document.getElementById('menu-cancel-btn3').style.visibility = 'hidden';
		document.getElementById('img-attr3').setAttribute('value','0');
	} else {
		document.getElementById("menu-img3").src = (base_url_protocol+'//'+base_url_host+'/'+img3.toString());
		document.getElementById('menu-cancel-btn3').style.visibility = 'visible';
		document.getElementById('img-attr3').setAttribute('value','1');
	}
	if (img4 == "") {
		document.getElementById("menu-img4").src = imgEmpty;
		document.getElementById('menu-cancel-btn4').style.visibility = 'hidden';
		document.getElementById('img-attr4').setAttribute('value','0');
	} else {
		document.getElementById("menu-img4").src = (base_url_protocol+'//'+base_url_host+'/'+img4.toString());
		document.getElementById('menu-cancel-btn4').style.visibility = 'visible';
		document.getElementById('img-attr4').setAttribute('value','1');
	}

};

//service menu view JS
function divModalView(a) {
	console.log(a);
	var nMID = a.querySelector('#menuID').value.replace('#','');
	$('.modalView').attr('id','menuView'+nMID);
	$('#menu_id').attr('value', a.querySelector('#menuID').value);
	$('#menu_name').text(a.querySelector('#menuName').value);
	$('.menu_price').attr('value', a.querySelector('#menuPrice').value);
	$('#menu_description').text(a.querySelector('#menuDescription').value);
	var img1 = a.querySelector('#menuImg1').value;
	var img2 = a.querySelector('#menuImg2').value;
	var img3 = a.querySelector('#menuImg3').value;
	var img4 = a.querySelector('#menuImg4').value;

	if (img1 == "") {
		$('#menu-img1v').remove();
	} else {
		document.getElementById("menu-img1v").src = (base_url_protocol+'//'+base_url_host+'/'+img1.toString());
	}
	if (img2 == "") {
		$('#menu-img2v').remove();
	} else {
		document.getElementById("menu-img2v").src = (base_url_protocol+'//'+base_url_host+'/'+img2.toString());
	}
	if (img3 == "") {
		$('#menu-img3v').remove();
	} else {
		document.getElementById("menu-img3v").src = (base_url_protocol+'//'+base_url_host+'/'+img3.toString());
	}
	if (img4 == "") {
		$('#menu-img4v').remove();
	} else {
		document.getElementById("menu-img4v").src = (base_url_protocol+'//'+base_url_host+'/'+img4.toString());
	}

	//put back to modal

};

// Menu Cover Image
var menu_imageC = function(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e){
			$('#menu-imgC').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#menu-upload-btnC").unbind('click');

$("#menu-photoC").on('change', function(e){
	menu_imageC(this);
});

$("#menu-upload-btnC").on('click', function(e) {
	e.stopPropagation();
	$("#menu-photoC").click();
});

if($("#menu-imgC").attr('src') == "/img/placeholder.png"){
	document.getElementById('menu-cancel-btnC').style.visibility = 'hidden';
}

$("#menu-cancel-btnC").on('click', function() {
	document.getElementById('menu-imgC').removeAttribute('src');
	document.getElementById('menu-imgC').setAttribute('src','/img/placeholder.png');
	document.getElementById('menu-cancel-btnC').style.visibility = 'hidden';
	document.getElementById('img-attrC').setAttribute('value','0');
});

$("#menu-imgC").attrchange({
	trackValues: true,
	callback: function (e) {
		if(e.oldValue == "/img/placeholder.png"){
			if(e.newValue != "/img/placeholder.png") {
				document.getElementById('menu-cancel-btnC').style.visibility = 'visible';
				document.getElementById('img-attrC').setAttribute('value', '1');
			}
		}
	}
});
// End of Menu Cover Image
//End of Menu Images

//viewer js
if(document.getElementById('picture')){
	var picture = document.getElementById('picture');
	var viewer = new Viewer(picture, {
		url: 'data-original',
		title: false,
		list: false,
		toolbar: {
			zoomIn: true,
			zoomOut: true,
		},
	});
}
//Google Analytics
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-124591193-1');
//End of Google Analytics